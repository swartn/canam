!> !> !! !! !
subroutine init_snow_rt_lut()

  !
  !     * feb 10/2015 - j.cole. new version for gcm18:
  !                             - nbc increased from 12 to 20.
  !     * jan 24/2013 - j.cole. previous version for gcm17:
  !                    - read in snow albedo and transmissivity look up table
  !                      required for new albedo parameterizations.

  implicit none

  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  ! this subroutine reads in a single cccma formatted file that contains
  ! lookup tables for each band in the cccma solar radiative transfer model
  ! for snow albedo and tranmissivity.  for both variables there are two tables
  ! one for incident direct beam radiation and diffuse radiation.  the
  ! tables are a function of the cosine of solar zenith angle, underlying
  ! surface albedo, snow water equivalent, snow grain size and black carbon
  ! concentration.  the data in the file has been unrolled into a 1xn vector
  ! of values which needs to be reshaped into five dimensional arrays.
  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  integer :: iu
  integer :: isbuf
  integer :: isdat
  integer :: i
  integer :: ivar
  integer :: newunit
  integer :: nc4to8

  integer :: ismu
  integer :: isalb
  integer :: ibc
  integer :: isgs
  integer :: iswe
  integer :: ipnt
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< !           !! !           !! !==================================================================

  !--- table dimensions
  ! nsmu ... number of cosine of the solar zenith angle
  ! nsfa ... number of surface albedos
  ! nbc  ... number of black carbon concentrations
  ! nsgs ... number of snow grain sizes
  ! nswe ... number of snow water equivalents
  ! nbnd ... number of solar radiation bands

  integer, parameter :: nsmu = 10
  integer, parameter :: nsfa = 11
  integer, parameter :: nbc  = 20
  integer, parameter :: nsgs = 10
  integer, parameter :: nswe = 11
  integer, parameter :: nbnd = 4
  integer, parameter :: irec = nsmu * nsfa * nbc * nsgs * nswe
  integer, parameter :: lrec = irec * 2

  real, dimension(irec) :: datat

  logical :: ok

  common /iscom/ isbuf(8), isdat(lrec)

  integer :: machine
  integer :: intsize
  common /machtyp/ machine, intsize

  integer*4 :: mynode
  common /mpinfo/ mynode

  integer :: snow_alb_lut_init
  integer :: snow_tran_lut_init

  character(len = 4) :: cnam

  ! need four look up tables.
  real, dimension(nbc,nswe,nsgs,nsmu,nsfa,nbnd) :: albdif
  real, dimension(nbc,nswe,nsgs,nsmu,nsfa,nbnd) :: albdir
  real, dimension(nbc,nswe,nsgs,nsmu,nsfa,nbnd) :: trandif
  real, dimension(nbc,nswe,nsgs,nsmu,nsfa,nbnd) :: trandir

  common /snowalblut/ albdif,albdir,snow_alb_lut_init
  common /snowtranlut/ trandif,trandir,snow_tran_lut_init

  snow_alb_lut_init  = 0
  snow_tran_lut_init = 0

  !.........get an available unit number
  iu = newunit(80)

  !.........open the file containing the lookup tables
  inquire(file = "SNOWRTLUT",exist = ok)
  if (.not.ok) then
    write(6, * )'INIT_SNOW_RT_LUT: ', &
                   'Error accessing SNOW_ALB_TRAN_LUT'
    call xit('INIT_SNOW_RT_LUT', - 1)
  end if
  open(iu,file = 'SNOWRTLUT',form = 'UNFORMATTED')

  ivar = 1

  !.........read the albedo lookup tables

  do i = 1,nbnd
    write(cnam,1001) "ADF",i
    call getfld2(iu,datat, &
                 nc4to8("GRID"),ivar,nc4to8(cnam),1,isbuf,lrec,ok)
    if (.not.ok) call xit('INIT_SNOW_RT_LUT', - 2)
    ipnt = 1
    do isalb = 1, nsfa
      do ismu = 1, nsmu
        do isgs = 1, nsgs
          do iswe = 1, nswe
            do ibc = 1, nbc
              albdif (ibc,iswe,isgs,ismu,isalb,i) = datat(ipnt)
              ipnt = ipnt + 1
            end do ! ibc
          end do ! iswe
        end do ! isgs
      end do ! ismu
    end do ! isalb
  end do

  do i = 1,nbnd
    write(cnam,1001) "ADR",i
    call getfld2(iu,datat, &
                 nc4to8("GRID"),ivar,nc4to8(cnam),1,isbuf,lrec,ok)
    if (.not.ok) call xit('INIT_SNOW_RT_LUT', - 3)
    ipnt = 1
    do isalb = 1, nsfa
      do ismu = 1, nsmu
        do isgs = 1, nsgs
          do iswe = 1, nswe
            do ibc = 1, nbc
              albdir(ibc,iswe,isgs,ismu,isalb,i) = datat(ipnt)
              ipnt = ipnt + 1
            end do ! ibc
          end do ! iswe
        end do ! isgs
      end do ! ismu
    end do ! isalb
  end do

  !.........read the transmissivity lookup tables
  do i = 1,nbnd
    write(cnam,1001) "TDF",i
    call getfld2(iu,datat, &
                 nc4to8("GRID"),ivar,nc4to8(cnam),1,isbuf,lrec,ok)
    if (.not.ok) call xit('INIT_SNOW_RT_LUT', - 4)
    ipnt = 1
    do isalb = 1, nsfa
      do ismu = 1, nsmu
        do isgs = 1, nsgs
          do iswe = 1, nswe
            do ibc = 1, nbc
              trandif (ibc,iswe,isgs,ismu,isalb,i) = datat(ipnt)
              ipnt = ipnt + 1
            end do ! ibc
          end do ! iswe
        end do ! isgs
      end do ! ismu
    end do ! isalb
  end do

  do i = 1,nbnd
    write(cnam,1001) "TDR",i
    call getfld2(iu,datat, &
                 nc4to8("GRID"),ivar,nc4to8(cnam),1,isbuf,lrec,ok)
    if (.not.ok) call xit('INIT_SNOW_RT_LUT', - 5)
    ipnt = 1
    do isalb = 1, nsfa
      do ismu = 1, nsmu
        do isgs = 1, nsgs
          do iswe = 1, nswe
            do ibc = 1, nbc
              trandir(ibc,iswe,isgs,ismu,isalb,i) = datat(ipnt)
              ipnt = ipnt + 1
            end do ! ibc
          end do ! iswe
        end do ! isgs
      end do ! ismu
    end do ! isalb
  end do

  snow_alb_lut_init  = 1
  snow_tran_lut_init = 1

  close(iu)

  return
1001 format(a,i1)

end subroutine init_snow_rt_lut 
!> !> !! !! ! !! ! !! ! !! ! !! ! !! !
