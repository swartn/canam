!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine phystepinit(phys_arrays, ni, nk, trnch)
  use psizes_19
  use clip_boundary_values
  use times_mod, only: kstart, kount
  use agcm_types_mod, only : phys_arrays_type
  use agcm_types_mod, only : options => phys_options
  use agcm_types_mod, only : diag_options => phys_diag
  use tileinfo_mod, only : iulak, irlwt, irlic, iowat, iosic
  use phys_consts, only : grav, tfrez, gtfsw, sicmin
  use phybus

  use phy_getmeta_mod, only: phy_getmeta
  use phy_options, only: debug_mem_L
  use phy_typedef, only: phymeta
  use phygetmetaplus_mod,   only: phymetaplus, phygetmetaplus
  use, intrinsic :: iso_fortran_env, only: INT64
  use debug_mod, only: assert_not_naninf

  implicit none

  integer,                intent(in)    :: ni     !< Number of grid point (along x)
  integer,                intent(in)    :: nk     !< Number of model vertical levels
  integer,                intent(in)    :: trnch  !< Latitude slice index
  type(phys_arrays_type), intent(inout) :: phys_arrays

  type(phymetaplus) :: pmeta
  integer :: i !<
  integer :: j !<
  integer :: k !<
  integer :: l !<

  real :: fveg !<
  real :: fwat !<
  real :: sandx !<
  real :: depthx !<
  integer :: ier !<
  integer :: indx !<
  integer :: n !<
  integer :: nt !<
  !
  real, parameter :: rhow = 1.0e3    !< Water density, kg/m3
  real, parameter :: rlim = 0.025    !<
  real, parameter :: tzeroc = 273.16 !< Zero-point temperature, K
  !
  integer :: m !<

  real, dimension(ni) :: gtano !<
  real, pointer, dimension(:) :: gtrot_wat
  !
#include <rmnlib_basics.hf>
  include "phyinput.inc"

  logical,parameter :: SHORTMATCH_L = .true.
  integer,parameter :: MYMAX = 2048

  character(len=1)  :: bus_S
  integer           :: ivar, nvars, istat

  type(phymetaplus) :: meta_m
  type(phymeta), pointer :: metalist(:)
  real, pointer, dimension(:,:) :: tmp1, tmp2

  IF_DEBUG: if (debug_mem_L) then

    !# Init diag level of dyn bus (copy down) if var not read
    !# this is needed in debug mode since the bus is init with NaN
    !# It needs to be done at kount==0 and at every restart
    bus_S = 'D'
    nullify(metalist)
    nvars = phy_getmeta(metalist, '', F_npath='V', F_bpath=bus_S, &
         F_maxmeta=MYMAX, F_shortmatch=SHORTMATCH_L)
    do ivar = 1, nvars
       istat = phygetmetaplus(meta_m, metalist(ivar)%vname, F_npath='V', &
            F_bpath=bus_S, F_quiet=.true., F_shortmatch=.false.)
       if (.not.any(meta_m%meta%vname == phyinread_list_s(1:phyinread_n))) then

          if (meta_m%meta%nk >= nk) then
             !#TODO: adapt for 4D vars (ni,nk,fmul,nj)
             tmp1(1:ni,1:nk) => meta_m%vptr(:,trnch)
             if (.not.RMN_IS_OK(assert_not_naninf(tmp1(:,nk)))) then
                if (any(metalist(ivar)%vname == (/"pw_gz:m", "pw_wz:p"/)).or.&
                     metalist(ivar)%vname(1:3) == "tr/") then
                   tmp1(:,nk) = 0
                else
                   tmp1(:,nk) = tmp1(:,nk-1)
                endif
             endif
          endif
       endif
    enddo
    deallocate(metalist, stat=istat)

    !# Reset Vol bus var to zero if var not read
    !# this is needed in debug mode since the bus is init with NaN
    bus_S = 'V'
    nullify(metalist)
    nvars = phy_getmeta(metalist, '', F_npath='V', F_bpath=bus_S, &
         F_maxmeta=MYMAX, F_shortmatch=SHORTMATCH_L)
    do ivar = 1, nvars
       istat = phygetmetaplus(meta_m, metalist(ivar)%vname, F_npath='V', &
            F_bpath=bus_S, F_quiet=.true., F_shortmatch=.false.)
       if (.not.any(meta_m%meta%vname == phyinread_list_s(1:phyinread_n))) &
            meta_m%vptr(:,trnch) = 0.
    enddo
    deallocate(metalist, stat=istat)

    !#TODO: check that memgap is still filled with NANs on all buses
  endif IF_DEBUG

  ! Check that the (stratospheric aerosols) fields have been read into the entry bus before assigning them
  if (options%explvol .and. any(phyinread_list_S == 'swe1_row')) then
     phys_arrays%sw_ext_sa_rol(:, :, 1) = read_ent_bus_2d('SWE1_ROW', ni, levsa, trnch)
     phys_arrays%sw_ext_sa_rol(:, :, 2) = read_ent_bus_2d('SWE2_ROW', ni, levsa, trnch)
     phys_arrays%sw_ext_sa_rol(:, :, 3) = read_ent_bus_2d('SWE3_ROW', ni, levsa, trnch)
     phys_arrays%sw_ext_sa_rol(:, :, 4) = read_ent_bus_2d('SWE4_ROW', ni, levsa, trnch)

     phys_arrays%sw_ssa_sa_rol(:, :, 1) = read_ent_bus_2d('SWS1_ROW', ni, levsa, trnch)
     phys_arrays%sw_ssa_sa_rol(:, :, 2) = read_ent_bus_2d('SWS2_ROW', ni, levsa, trnch)
     phys_arrays%sw_ssa_sa_rol(:, :, 3) = read_ent_bus_2d('SWS3_ROW', ni, levsa, trnch)
     phys_arrays%sw_ssa_sa_rol(:, :, 4) = read_ent_bus_2d('SWS4_ROW', ni, levsa, trnch)

     phys_arrays%sw_g_sa_rol(:, :, 1) = read_ent_bus_2d('SWG1_ROW', ni, levsa, trnch)
     phys_arrays%sw_g_sa_rol(:, :, 2) = read_ent_bus_2d('SWG2_ROW', ni, levsa, trnch)
     phys_arrays%sw_g_sa_rol(:, :, 3) = read_ent_bus_2d('SWG3_ROW', ni, levsa, trnch)
     phys_arrays%sw_g_sa_rol(:, :, 4) = read_ent_bus_2d('SWG4_ROW', ni, levsa, trnch)

     phys_arrays%lw_ext_sa_rol(:, :, 1) = read_ent_bus_2d('LWE1_ROW', ni, levsa, trnch)
     phys_arrays%lw_ext_sa_rol(:, :, 2) = read_ent_bus_2d('LWE2_ROW', ni, levsa, trnch)
     phys_arrays%lw_ext_sa_rol(:, :, 3) = read_ent_bus_2d('LWE3_ROW', ni, levsa, trnch)
     phys_arrays%lw_ext_sa_rol(:, :, 4) = read_ent_bus_2d('LWE4_ROW', ni, levsa, trnch)
     phys_arrays%lw_ext_sa_rol(:, :, 5) = read_ent_bus_2d('LWE5_ROW', ni, levsa, trnch)
     phys_arrays%lw_ext_sa_rol(:, :, 6) = read_ent_bus_2d('LWE6_ROW', ni, levsa, trnch)
     phys_arrays%lw_ext_sa_rol(:, :, 7) = read_ent_bus_2d('LWE7_ROW', ni, levsa, trnch)
     phys_arrays%lw_ext_sa_rol(:, :, 8) = read_ent_bus_2d('LWE8_ROW', ni, levsa, trnch)
     phys_arrays%lw_ext_sa_rol(:, :, 9) = read_ent_bus_2d('LWE9_ROW', ni, levsa, trnch)

     phys_arrays%lw_ssa_sa_rol(:, :, 1) = read_ent_bus_2d('LWS1_ROW', ni, levsa, trnch)
     phys_arrays%lw_ssa_sa_rol(:, :, 2) = read_ent_bus_2d('LWS2_ROW', ni, levsa, trnch)
     phys_arrays%lw_ssa_sa_rol(:, :, 3) = read_ent_bus_2d('LWS3_ROW', ni, levsa, trnch)
     phys_arrays%lw_ssa_sa_rol(:, :, 4) = read_ent_bus_2d('LWS4_ROW', ni, levsa, trnch)
     phys_arrays%lw_ssa_sa_rol(:, :, 5) = read_ent_bus_2d('LWS5_ROW', ni, levsa, trnch)
     phys_arrays%lw_ssa_sa_rol(:, :, 6) = read_ent_bus_2d('LWS6_ROW', ni, levsa, trnch)
     phys_arrays%lw_ssa_sa_rol(:, :, 7) = read_ent_bus_2d('LWS7_ROW', ni, levsa, trnch)
     phys_arrays%lw_ssa_sa_rol(:, :, 8) = read_ent_bus_2d('LWS8_ROW', ni, levsa, trnch)
     phys_arrays%lw_ssa_sa_rol(:, :, 9) = read_ent_bus_2d('LWS9_ROW', ni, levsa, trnch)
  end if

  if (kount /= 0) then
     ! Load the SST from the entry bus after initial time (for non-coupled runs)
     ier = phygetmetaplus(pmeta, 'WRK_SST', F_npath='V', F_bpath='P', &
                          F_quiet=.true., F_shortmatch=.false.)
     do i = 1, ni
        if (phys_arrays%farerot(i, iowat) > 0.0) then
           phys_arrays%gtrot(i, iowat) = pmeta%vptr(i, trnch)
        end if
     end do
  end if

  if (kount == 0) then
     ! Load sicrow and sicnrow from the entry bus at initial time
     ier = phygetmetaplus(pmeta, 'SIC_ENT', F_npath='V', F_bpath='E', &
                          F_quiet=.true., F_shortmatch=.false.)
     phys_arrays%sicrow(1:ni) = pmeta%vptr(1:ni, trnch)

     ier = phygetmetaplus(pmeta, 'SICN_ENT', F_npath='V', F_bpath='E', &
                          F_quiet=.true., F_shortmatch=.false.)
     phys_arrays%sicnrow(1:ni) = pmeta%vptr(1:ni, trnch)

     do k = 1, levox
        do i = 1, ni
           phys_arrays%h2o2row(i, k) = phys_arrays%h2o2rol(i, k)
           phys_arrays%hno3row(i, k) = phys_arrays%hno3rol(i, k)
           phys_arrays%nh3row(i, k) = phys_arrays%nh3rol(i, k)
           phys_arrays%nh4row(i, k) = phys_arrays%nh4rol(i, k)
           phys_arrays%no3row(i, k) = phys_arrays%no3rol(i, k)
           phys_arrays%o3row(i, k) = phys_arrays%o3rol(i, k)
           phys_arrays%ohrow(i, k) = phys_arrays%ohrol(i, k)
        end do
     end do

     do k = 1, levoz
        do i = 1, ni
           phys_arrays%ozrow(i,k) = phys_arrays%ozrol(i, k)
        end do
     end do

     do k = 1, levozc
        do i = 1, ni
           phys_arrays%o3crow(i, k) = phys_arrays%o3crol(i, k)
        end do
     end do

     do i = 1, ni
        phys_arrays%dmsorow(i) = phys_arrays%dmsorol(i)
        phys_arrays%edmsrow(i) = phys_arrays%edmsrol(i)
        phys_arrays%eostrow(i) = phys_arrays%eostrol(i)
     end do

     if (options%transient_aerosol_emissions) then
        do k = 1, levwf
           do i = 1, ni
              phys_arrays%fbbcrow(i, k) = phys_arrays%fbbcrol(i, k)
           end do
        end do

        do k = 1,levair
           do i = 1, ni
              phys_arrays%fairrow(i, k) = phys_arrays%fairrol(i, k)
           end do
        end do
        if (options%emists) then
           do i = 1, ni
              phys_arrays%sairrow(i) = phys_arrays%sairrol(i)
              phys_arrays%ssfcrow(i) = phys_arrays%ssfcrol(i)
              phys_arrays%sbiorow(i) = phys_arrays%sbiorol(i)
              phys_arrays%sshirow(i) = phys_arrays%sshirol(i)
              phys_arrays%sstkrow(i) = phys_arrays%sstkrol(i)
              phys_arrays%sfirrow(i) = phys_arrays%sfirrol(i)
              phys_arrays%oairrow(i) = phys_arrays%oairrol(i)
              phys_arrays%osfcrow(i) = phys_arrays%osfcrol(i)
              phys_arrays%obiorow(i) = phys_arrays%obiorol(i)
              phys_arrays%oshirow(i) = phys_arrays%oshirol(i)
              phys_arrays%ostkrow(i) = phys_arrays%ostkrol(i)
              phys_arrays%ofirrow(i) = phys_arrays%ofirrol(i)
              phys_arrays%bairrow(i) = phys_arrays%bairrol(i)
              phys_arrays%bsfcrow(i) = phys_arrays%bsfcrol(i)
              phys_arrays%bbiorow(i) = phys_arrays%bbiorol(i)
              phys_arrays%bshirow(i) = phys_arrays%bshirol(i)
              phys_arrays%bstkrow(i) = phys_arrays%bstkrol(i)
              phys_arrays%bfirrow(i) = phys_arrays%bfirrol(i)
           end do
        end if
     end if

     if (options%explvol) then
        do i = 1, ni
           phys_arrays%vtaurow(i) = phys_arrays%vtaurol(i)
           phys_arrays%troprow(i) = phys_arrays%troprol(i)
        end do

        phys_arrays%sw_ext_sa_row(:, :, :) = phys_arrays%sw_ext_sa_rol(:, :, :)
        phys_arrays%sw_ssa_sa_row(:, :, :) = phys_arrays%sw_ssa_sa_rol(:, :, :)
        phys_arrays%sw_g_sa_row(:, :, :) = phys_arrays%sw_g_sa_rol(:, :, :)

        phys_arrays%lw_ext_sa_row(:, :, :) = phys_arrays%lw_ext_sa_rol(:, :, :)
        phys_arrays%lw_ssa_sa_row(:, :, :) = phys_arrays%lw_ssa_sa_rol(:, :, :)

        do k = 1,levsa
           do i = 1, ni
              phys_arrays%w055_ext_sa_row(i, k) = phys_arrays%w055_ext_sa_rol(i, k)
              phys_arrays%w110_ext_sa_row(i, k) = phys_arrays%w110_ext_sa_rol(i, k)
              phys_arrays%pressure_sa_row(i, k) = phys_arrays%pressure_sa_rol(i, k)
           end do
        end do
     end if

     do i = 1, ni
        phys_arrays%pdsfrow(i) = phys_arrays%pdsfrol(i)
        phys_arrays%suz0row(i) = phys_arrays%suz0rol(i)
     end do
!
     do n = 1,ntld
        do i = 1, ni
           phys_arrays%farerot(i, n) = phys_arrays%flndrow(i)
        end do
     end do

     if (iulak > 0) then
        do i = 1, ni
           phys_arrays%farerot(i, iulak) = phys_arrays%flkurow(i)
        end do
     end if
     if (irlwt > 0) then
        do i = 1, ni
           phys_arrays%farerot(i, irlwt) = (1. - phys_arrays%lrinrow(i)) * phys_arrays%flkrrow(i)
        end do
     end if
     if (irlic > 0) then
        do i = 1, ni
           phys_arrays%farerot(i, irlic) = phys_arrays%lrinrow(i) * phys_arrays%flkrrow(i)
        end do
     end if
     if ((iulak + irlwt + irlic) <= 0) then
        call xit('PHYSTEPINIT', -1)
     end if

     do i = 1, ni
        if (phys_arrays%flkurow(i) /= 0.0) then
           phys_arrays%lzicrow(i) = phys_arrays%luimrow(i) / 913.
           if (options%cslm) then
              phys_arrays%nlklrow(i) = real(nint(phys_arrays%hlakrow(i) / 0.50))    ! delzlk=0.5
              phys_arrays%delurow(i) = 0.
              phys_arrays%t0lkrow(i) = max(phys_arrays%gtrow(i), 277.16)
              phys_arrays%tkelrow(i) = 1.0e-12                ! tkemin=1.0e-12
              phys_arrays%ldmxrow(i) = 0.5*nint(phys_arrays%nlklrow(i)) - 1
              phys_arrays%dtmprow(i) = 0.

              if (options%relax_lake > 0) then
                 phys_arrays%ldmxrow(i) = 0.5
              end if
!
!             * initial mixed layer temp (celsius), density, expansivity,
!             * and reduced gravity.
!
              phys_arrays%rhomrow(i) = 999.975* &
                       (1. - 8.2545e-6 * ((phys_arrays%t0lkrow(i) - tfrez) - 3.9816) * &
                       ((phys_arrays%t0lkrow(i) - tfrez) - 3.9816))
              phys_arrays%expwrow(i) = -1.0 * (-2. * 999.975 * 8.2545e-6 * &
                       ((phys_arrays%t0lkrow(i) - tfrez) - 3.9816)) / phys_arrays%rhomrow(i)
              phys_arrays%gredrow(i) = grav*abs(rhow-phys_arrays%rhomrow(i)) / rhow
!
!             * ABORT CONDITIONS FOR CSLM.
!
              if (phys_arrays%nlklrow(i) > real(nlklm)) then
                 call xit('PHYSTEPINIT', -2)
              end if
!
              if (phys_arrays%gredrow(i) <= 0.) then
                 call xit('PHYSTEPINIT', -3)
              end if
!
              do l = 1, phys_arrays%nlklrow(i)
                 phys_arrays%tlakrow(i, l) = max(phys_arrays%gtrow(i), 277.16)
              end do
           end if
        end if
     end do

     if (iowat > 0) then
        do i = 1, ni
           phys_arrays%farerot(i, iowat) = (1.0 - phys_arrays%sicnrow(i)) * &
               (1.0 - phys_arrays%flndrow(i) - phys_arrays%flkrrow(i) - phys_arrays%flkurow(i))
        end do
     end if
     if (iosic > 0) then
        do i = 1, ni
           phys_arrays%farerot(i, iosic) = phys_arrays%sicnrow(i) * &
             (1. - phys_arrays%flndrow(i) - phys_arrays%flkrrow(i) - phys_arrays%flkurow(i))
        end do
     end if
     if ((iowat + iosic) <= 0) then
        call xit('PHYSTEPINIT', -4)
     end if

     do i = 1, ni
        if (phys_arrays%sandrot(i, 1, 1) == -4.0 .and. &
            phys_arrays%snorow(i) > 60.0) phys_arrays%snorow(i)=60.
     end do

     do i = 1, ni
        if (phys_arrays%snorow(i) > 1.0e-3) then
           phys_arrays%rhonrow(i) = 300.
           phys_arrays%anrow  (i) = 0.84
           phys_arrays%fnrow  (i) = 1.
           phys_arrays%refrow (i) = 5.45256270447e-05
           phys_arrays%bcsnrow(i) = 0.
        else
           phys_arrays%snorow (i) = 0.
           phys_arrays%rhonrow(i) = 0.
           phys_arrays%anrow  (i) = 0.
           phys_arrays%fnrow  (i) = 0.
           phys_arrays%refrow (i) = 0.
           phys_arrays%bcsnrow(i) = 0.
        end if
     end do

     do i = 1, ni
        phys_arrays%emisrow(i) = 1.
     end do

     do i = 1, ni
        phys_arrays%tnrow(i) = min(phys_arrays%gtrow(i), tzeroc)
     end do

     do i = 1, ni
        if (phys_arrays%flndrow(i) > 0.) then
           if (phys_arrays%gtrow(i) > tzeroc) then
              do n = 1, ntld
                phys_arrays%ttrot(i, n) = 1.0
              end do
           else
              do n = 1, ntld
                 phys_arrays%ttrot(i, n) = 0.0
              end do
           end if
           do n = 1, ntld
              phys_arrays%tvrot(i, n) = phys_arrays%gtrow(i)
           end do
        else
           do n = 1, ntld
              phys_arrays%ttrot(i, n) = 2.0
              phys_arrays%tvrot(i, n) = 0.
           end do
        end if

        do n = 1, ntld
           fveg=0.
           do l = 1, icanp1
              fveg=fveg+phys_arrays%fcanrot(i, n, l)
           end do
           if (fveg > 1.0) then
              do l = 1, icanp1
                 phys_arrays%fcanrot(i, n, l) = phys_arrays%fcanrot(i, n , l) / fveg
              end do
           end if
        end do

        do n = 1, ntld
           do l = 1, ignd
              sandx = phys_arrays%sandrot(i, n, l)
              depthx = zbot(l) - delz(l) + rlim
              if (phys_arrays%sandrot(i, n, 1) == -4) then
                 phys_arrays%sandrot(i, n, l) = -4.
                 phys_arrays%thlqrot(i, n, l) = 0.
                 phys_arrays%thicrot(i, n, l) = 1.
                 phys_arrays%tgrot  (i, n, l) = min(phys_arrays%tgrot(i, n, l),tzeroc)
              else if(sandx /= -3. .and. phys_arrays%dpthrot(i, n) < depthx) then
                 phys_arrays%sandrot(i, n, l) = -3.
                 phys_arrays%thlqrot(i, n, l) = 0.
                 phys_arrays%thicrot(i, n, l) = 0.
              end if
           end do
        end do
     end do
!
!      * initialization of grid-average (untiled) albedoes.
!
     do i = 1, ni
        if (phys_arrays%sicnrow(i) >= 0.15 .or. phys_arrays%snorow(i) > 0.) then
           phys_arrays%csalrol(i, 1) = 0.778
           phys_arrays%salbrol(i, 1) = 0.778
           phys_arrays%csalrol(i, 2) = 0.443
           phys_arrays%salbrol(i, 2) = 0.443
           phys_arrays%csalrol(i, 3) = 0.055
           phys_arrays%salbrol(i, 3) = 0.055
           phys_arrays%csalrol(i, 4) = 0.036
           phys_arrays%salbrol(i, 4) = 0.036
        else
           phys_arrays%csalrol(i, 1) = 0.15
           phys_arrays%salbrol(i, 1) = 0.15
           phys_arrays%csalrol(i, 2) = 0.15
           phys_arrays%salbrol(i, 2) = 0.15
           phys_arrays%csalrol(i, 3) = 0.15
           phys_arrays%salbrol(i, 3) = 0.15
           phys_arrays%csalrol(i, 4) = 0.15
           phys_arrays%salbrol(i, 4) = 0.15
        endif
     end do
!
!      * other tiled fields which exist over all tiles.
!
!      * first, general initialization for all tiles to start.
!
     do i = 1, ni
        do m = 1, im
           phys_arrays%emisrot(i, m) = 0.
           phys_arrays%csalrot(i, m, 1) = 0.
           phys_arrays%salbrot(i, m, 1) = 0.
           phys_arrays%csalrot(i, m, 2) = 0.
           phys_arrays%salbrot(i, m, 2) = 0.
           phys_arrays%csalrot(i, m, 3) = 0.
           phys_arrays%salbrot(i, m, 3) = 0.
           phys_arrays%csalrot(i, m, 4) = 0.
           phys_arrays%salbrot(i, m, 4) = 0.
!
           phys_arrays%gtrot  (i, m) = phys_arrays%gtrow(i)
           phys_arrays%snorot (i, m) = 0.
           phys_arrays%anrot  (i, m) = 0.
           phys_arrays%fnrot  (i, m) = 0.
           phys_arrays%tnrot  (i, m) = 0.
           phys_arrays%rhonrot(i, m) = 0.
           phys_arrays%refrot (i, m) = 0.
           phys_arrays%bcsnrot(i, m) = 0.
        end do
!
!        * land tiles.
!
        do m = 1, ntld
           if (phys_arrays%flndrow(i) > 0.) then
!
!            * set rot fields from initialized read-in row fields (getgg14)
!            * and above.
!
              phys_arrays%emisrot(i, m) = 1.
              phys_arrays%csalrot(i, m, 1) = 0.25
              phys_arrays%salbrot(i, m, 1) = 0.25
              phys_arrays%csalrot(i, m, 2) = 0.25
              phys_arrays%salbrot(i, m, 2) = 0.25
              phys_arrays%csalrot(i, m, 3) = 0.25
              phys_arrays%salbrot(i, m, 3) = 0.25
              phys_arrays%csalrot(i, m, 4) = 0.25
              phys_arrays%salbrot(i, m, 4) = 0.25
!
              if (phys_arrays%snorow(i) > 0.) then
                 phys_arrays%snorot (i, m) = phys_arrays%snorow (i)
                 phys_arrays%anrot  (i, m) = phys_arrays%anrow  (i)
                 phys_arrays%fnrot  (i, m) = phys_arrays%fnrow  (i)
                 phys_arrays%tnrot  (i, m) = phys_arrays%tnrow  (i)
                 phys_arrays%rhonrot(i, m) = phys_arrays%rhonrow(i)
                 phys_arrays%refrot (i, m) = phys_arrays%refrow (i)
                 phys_arrays%bcsnrot(i, m) = phys_arrays%bcsnrow(i)
              else
                 phys_arrays%snorot (i, m) = 0.
                 phys_arrays%anrot  (i, m) = 0.
                 phys_arrays%fnrot  (i, m) = 0.
                 phys_arrays%tnrot  (i, m) = 0.
                 phys_arrays%rhonrot(i, m) = 0.
                 phys_arrays%refrot (i, m) = 0.
                 phys_arrays%bcsnrot(i, m) = 0.
              end if
           end if
        end do
!
!        * unresolved lakes.
!
        if (phys_arrays%flkurow(i) > 0.) then
           if (phys_arrays%luinrow(i) > 0.) then
              phys_arrays%gtrot  (i, iulak) = 273.15
              phys_arrays%emisrot(i, iulak) = 1.
              phys_arrays%csalrot(i, iulak, 1) = 0.778
              phys_arrays%salbrot(i, iulak, 1) = 0.778
              phys_arrays%csalrot(i, iulak, 2) = 0.443
              phys_arrays%salbrot(i, iulak, 2) = 0.443
              phys_arrays%csalrot(i, iulak, 3) = 0.055
              phys_arrays%salbrot(i, iulak, 3) = 0.055
              phys_arrays%csalrot(i, iulak, 4) = 0.036
              phys_arrays%salbrot(i, iulak, 4) = 0.036
           else
              phys_arrays%gtrot  (i, iulak) = phys_arrays%gtrow(i)
              phys_arrays%emisrot(i, iulak) = 0.97   !emsw
              phys_arrays%csalrot(i, iulak, 1) = 0.05
              phys_arrays%salbrot(i, iulak, 1) = 0.05
              phys_arrays%csalrot(i, iulak, 2) = 0.05
              phys_arrays%salbrot(i, iulak, 2) = 0.05
              phys_arrays%csalrot(i, iulak, 3) = 0.05
              phys_arrays%salbrot(i, iulak, 3) = 0.05
              phys_arrays%csalrot(i, iulak, 4) = 0.05
              phys_arrays%salbrot(i, iulak, 4) = 0.05
           end if
        end if
!
!        * resolved lakes (assumed no initial snow and all ice-covered).
!
        if (phys_arrays%flkrrow(i) > 0.) then
           if (phys_arrays%lrinrow(i) > 0.) then
               phys_arrays%gtrot  (i, irlic) = 273.15
               phys_arrays%emisrot(i, irlic) = 1.
               phys_arrays%csalrot(i, irlic, 1) = 0.778
               phys_arrays%salbrot(i, irlic, 1) = 0.778
               phys_arrays%csalrot(i, irlic, 2) = 0.443
               phys_arrays%salbrot(i, irlic, 2) = 0.443
               phys_arrays%csalrot(i, irlic, 3) = 0.055
               phys_arrays%salbrot(i, irlic, 3) = 0.055
               phys_arrays%csalrot(i, irlic, 4) = 0.036
               phys_arrays%salbrot(i, irlic, 4) = 0.036
           end if
           if (phys_arrays%lrinrow(i) < 1.) then
              phys_arrays%gtrot  (i, irlwt) = phys_arrays%gtrow(i)
              phys_arrays%emisrot(i, irlwt) = 0.97   !emsw
              phys_arrays%csalrot(i, irlwt, 1) = 0.05
              phys_arrays%salbrot(i, irlwt, 1) = 0.05
              phys_arrays%csalrot(i, irlwt, 2) = 0.05
              phys_arrays%salbrot(i, irlwt, 2) = 0.05
              phys_arrays%csalrot(i, irlwt, 3) = 0.05
              phys_arrays%salbrot(i, irlwt, 3) = 0.05
              phys_arrays%csalrot(i, irlwt, 4) = 0.05
              phys_arrays%salbrot(i, irlwt, 4) = 0.05
           end if
        end if
!
!        * ocean tiles (assume no snow and ice-covered).
!
        fwat=1. - phys_arrays%flndrow(i) - phys_arrays%flkurow(i) - phys_arrays%flkrrow(i)
        if (fwat > 0.) then
           if (phys_arrays%sicnrow(i) > 0.) then
              phys_arrays%gtrot  (i, iosic) = 273.15
              phys_arrays%emisrot(i, iosic) = 1.
              phys_arrays%csalrot(i, iosic, 1) = 0.778
              phys_arrays%salbrot(i, iosic, 1) = 0.778
              phys_arrays%csalrot(i, iosic, 2) = 0.443
              phys_arrays%salbrot(i, iosic, 2) = 0.443
              phys_arrays%csalrot(i, iosic, 3) = 0.055
              phys_arrays%salbrot(i, iosic, 3) = 0.055
              phys_arrays%csalrot(i, iosic, 4) = 0.036
              phys_arrays%salbrot(i, iosic, 4) = 0.036
           end if
           if (phys_arrays%sicnrow(i) < 1.) then
              phys_arrays%gtrot  (i, iowat) = phys_arrays%gtrow(i)
              phys_arrays%emisrot(i, iowat) = 0.97   !emsw
              phys_arrays%csalrot(i, iowat, 1) = 0.05
              phys_arrays%salbrot(i, iowat, 1) = 0.05
              phys_arrays%csalrot(i, iowat, 2) = 0.05
              phys_arrays%salbrot(i, iowat, 2) = 0.05
              phys_arrays%csalrot(i, iowat, 3) = 0.05
              phys_arrays%salbrot(i, iowat, 3) = 0.05
              phys_arrays%csalrot(i, iowat, 4) = 0.05
              phys_arrays%salbrot(i, iowat, 4) = 0.05
           end if
        end if
     end do

     do i = 1, ni
        do n = 1, ntld
           if (phys_arrays%sandrot(i, n, 1) == -4.) &
              phys_arrays%ttrot(i, n) = -2.
        end do
     end do

     do i = 1, ni
        do n = 1, ntld
           phys_arrays%tbasrot(i, n) = phys_arrays%tgrot(i, n, ignd)
           phys_arrays%tavrot (i, n) = phys_arrays%tvrot(i, n)
           phys_arrays%qavrot (i, n) = 0.5e-4
           phys_arrays%mvrot  (i, n) = 0.
     end do
        end do

     do i = 1, ni
        do n=1,ntld
           phys_arrays%tsfsrot(i, n, 1) = tzeroc
           phys_arrays%tsfsrot(i, n, 2) = tzeroc
           phys_arrays%tsfsrot(i, n, 3) = phys_arrays%tgrot(i, n, 1)
           phys_arrays%tsfsrot(i, n, 4) = phys_arrays%tgrot(i, n, 1)
        end do
     end do

     do i = 1, ni
        phys_arrays%pbltrow(i) = real(ilev)
        phys_arrays%tcvrow (i) = real(ilev)
        phys_arrays%gtarow (i) = phys_arrays%gtrow(i)
        phys_arrays%stmxrow(i) = phys_arrays%gtrow(i)
        phys_arrays%stmnrow(i) = phys_arrays%gtrow(i)
     enddo

     do n = 1, ntrac
        do k = 1, nk - 1
           do i = 1, ni
              phys_arrays%sfrcrol(i, k, n) = 1.0
           end do
        end do
     end do
  !
  !     * initialize angle to due e-w for gwd launching to zero for agcm.
  !     * this will **not** be true for rcm wrapper !
  !
     do i = 1, ni
        phys_arrays%phierow(i) = 0.0
     end do
  !
     if (diag_options%radforce) then
  !     * initialize radiative forcing tropopause index
  !
        phys_arrays%kthrol(:) = diag_options%radforce_trop_idx
     end if

  end if !kount

  gtano = 0.
  gtrot_wat(1:ni) => phys_arrays%gtrot(1:ni, iowat)
  call apply_clipping_gt_sic_sicn(gtrot_wat, gtano, phys_arrays%sicrow, phys_arrays%sicnrow, &
                                  phys_arrays%flndrow, ni, 1, ni, sicmin, gtfsw)

  if (kstart == kount) then
!
!ctem - initialize intercellular co2 concentrations to zero
!
     do n = 1, ntld
        do i = 1, ni
           phys_arrays%fsnowrtl(i, n) = 0.
           phys_arrays%tcanortl(i, n) = 0.
           phys_arrays%tcansrtl(i, n) = 0.
              phys_arrays%tartl(i, n) = 0.
        end do
     end do

     do k = 1, ictem
        do n = 1, ntld
           do i = 1, ni
               phys_arrays%ancgrtl(i, n, k) = 0.
               phys_arrays%ancsrtl(i, n, k) = 0.
              phys_arrays%rmlcgrtl(i, n, k) = 0.
              phys_arrays%rmlcsrtl(i, n, k) = 0.
           end do
        end do
     end do

     do k = 1, ignd
        do n = 1, ntld
           do i = 1, ni
                phys_arrays%tbarrtl(i, n, k) = 0.
               phys_arrays%tbarcrtl(i, n, k) = 0.
              phys_arrays%tbarcsrtl(i, n, k) = 0.
               phys_arrays%tbargrtl(i, n, k) = 0.
              phys_arrays%tbargsrtl(i, n, k) = 0.
              phys_arrays%thliqcrtl(i, n, k) = 0.
              phys_arrays%thliqgrtl(i, n, k) = 0.
              phys_arrays%thicecrtl(i, n, k) = 0.
           end do
        end do
     end do
  end if

 return

 contains

 function read_ent_bus_2d(fieldname, ni, nlev, jlat) result(array2d)
    implicit none

    integer,          intent(in) :: ni
    integer,          intent(in) :: nlev
    integer,          intent(in) :: jlat
    character(len=*), intent(in) :: fieldname

    real, dimension(ni, nlev) :: array2d
    integer :: icnt, ier, ii, kk

    ier = phygetmetaplus(pmeta, trim(fieldname), F_npath='V', F_bpath='E', &
                         F_quiet=.true., F_shortmatch=.false.)
    if (ier < 0) then
       call xit('READ_ENT_BUS_2D', -1)
    end if

    icnt = 0
    do kk = 1, nlev
       do ii = 1, ni
          icnt = icnt + 1
          array2d(ii, kk) = pmeta%vptr(icnt, jlat)
       end do
    end do

 end function read_ent_bus_2d

end subroutine phystepinit
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
