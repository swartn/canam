#!/bin/bash
#

#-----------------------------------------
set -x
#monthly diagnostics job

export PATH=$PATH:${TASK_BIN}

runyear=$(echo ${SEQ_SHORT_DATE} | cut -c1-4)

# year_offset used in daily and hourly decks
year_offset=$((runyear-1))

# Retrieve the needed CanDIAG settings (set in canesm.cfg and/or CanDIAG.cfg)
delt=${CanAM_delt%.*}

# get issp (steps of output frequency of dynamic variables)
issp=$((issp/delt))

# get israd (steps of output frequency of accumulated variables)
israd=$((israd/delt))

# get ishp
ishp=$((ishp/delt))

# get isgg and isbeg (unit in steps)
isgg=$((isgg/delt))
isbeg=$((isbeg/delt))

# get vertical levels and plid
ilev=${ilev} #
#ilevte=$(cat ${TASK_INPUT}/cfg_settings | sed -n '/hyb/,/=/p'|sed -e '$d' -e 's/, /, \n/g' -e '/^$/d' -e 's/hyb.*=//' -e 's/ //g'|wc -l)

g1=0.001080000 #
#g1te=$(cat ${TASK_INPUT}/cfg_settings | sed -n '/hyb/,/=/p'|sed -e '$d' -e 's/, /, \n/g' -e '/^$/d' -e 's/hyb.*=//' -e 's/,//g'|awk 'FNR==1')
g2=0.001440000 #
#g2te=$(cat ${TASK_INPUT}/cfg_settings | sed -n '/hyb/,/=/p'|sed -e '$d' -e 's/, /, \n/g' -e '/^$/d' -e 's/hyb.*=//' -e 's/,//g'|awk 'FNR==2')
plid=$(echo $g1 $g2|awk '{printf "%10.3f",$1*sqrt($1/$2)*100000.0}')

uxxx="ma"

agcm_diag_parallel=1
# The chunk_size in months (must correspond to an integer number of years)
if [ $agcm_diag_parallel -eq 1 ] ; then
  chunk_size=`echo ${runyear} ${RUNCYCLE_start_month} ${runyear} ${RUNCYCLE_stop_month} | awk '{printf "%d",($3-$1)*12+($4-$2)+1}'`
  if [ $chunk_size -gt 12 ] ; then
    chunk_size=12
  fi

  # Limit the chuck size to maximum of 3 months for the RCM because of large memory requirements.
  if [[ "${CANESM_mode}" = "RCM" && $chunk_size -gt 3 ]] ; then
     chunk_size=3
  fi
else
  chunk_size=1
fi
echo chunk_size=$chunk_size

year=${runyear}
diag_uxxx=da
diagpath=${DATAPATH}
datatype=${CANESM_datatype}
plid="$(printf "%10.2f" $plid)" # 10-character plid

debug=${CanDIAG_debug}

username=${CANESM_user}; user=${CANESM_lastname};
CCRNTMP_agcm_diag="$CCRNTMP"; TMPFS_agcm_diag="";
CCRNTMP="${CCRNTMP_agcm_diag}"; TMPFS="${TMPFS_agcm_diag}";

oldiag="diag4";

#
mask=land_mask_${resol};
#

trac=${CanDIAG_trac}

lon="$(printf "%5d" ${CanDIAG_lon})"
lat="$(printf "%5d" ${CanDIAG_lat})"

t1="        1"; t2="999999999"; t3="   1"; s3="   1";
r1="        1"; r2="999999999"; r3="   1";
g1=$g1 ; g2=$g2 ; g3="   1";
a1="        1"; a2="999999999"; a3="   1";
lml="  995";  d="B";
lay="    2";   coord=" ET15"; topsig="-1.00";
plv="   44"; p01="-0100"; p02="-0150"; p03="-0200"; p04="-0300"; p05="-0500";
             p06="-0700"; p07="   10"; p08="   15"; p09="   20"; p10="   30";
             p11="   50"; p12="   70"; p13="   80"; p14="   90"; p15="  100";
             p16="  115"; p17="  125"; p18="  130"; p19="  150"; p20="  170";
             p21="  175"; p22="  200"; p23="  225"; p24="  250"; p25="  300";
             p26="  350"; p27="  400"; p28="  450"; p29="  500"; p30="  550";
             p31="  600"; p32="  650"; p33="  700"; p34="  750"; p35="  775";
             p36="  800"; p37="  825"; p38="  850"; p39="  875"; p40="  900";
             p41="  925"; p42="  950"; p43="  975"; p44=" 1000"; p45="     ";
             p46="     "; p47="     "; p48="     "; p49="     "; p50="     ";
kax="    1"; kin="    1"; lxp="    0"; map="    0"; b="  +";

lrt="   63"; lmt="   63"; typ="    2";
npg="    2"; ncx="    2"; ncy="    2";

 # daily data tiers (default tier is "1")
 # 0: exit, i.e., not output daily files
 # 1: all 2-D variables requested for CMIP6, saved in _dd file
 # 2: all 3-D/zonal mean variables on pressure levels, saved in _dp and _dx files
 daily_cmip6_tiers="1 2"

# Options for epflux_cmip6.dk
 nbands="0";  # to process all zonal wavenumbers (k)

# nbands="1";            # to process a single wavenumber band, in addition to all k
# mfirst_band1="   1";   # starting k of first band
# mlast_band1="   3";    # ending k of first band
# plotlab_band1="M=1-3"  # label for plots
# maxf="  64";           # maxf=lon/2  (needed for llafrb & used only if nbands > 0)

# Options for gwdiag_cmip6.dk
# number of saves per day (4 => 6hr data)
 nsavesgw_per_day="   4" ### NB FOR RUNS DONE PRIOR TO ~FEB 18, 2019 this parameter should be set to 1


if [[ "${CANESM_mode}" = "RCM" ]] ; then
  ${TASK_BIN}/merge_diag_scripts -f -l ${CANDIAG_REF_PATH}/diag4 -o ${TASK_BIN}/merged_diag_deck.sh delmodinfo.sh gpintstat2.sh gsstats7.sh xstats5.sh daily_cordex.sh hourly_cordex.sh cleanall.sh
else
  ${TASK_BIN}/merge_diag_scripts -f -l ${CANDIAG_REF_PATH}/diag4 -o ${TASK_BIN}/merged_diag_deck.sh delmodinfo.sh gpintstat2.sh gsstats7.sh xstats5.sh cleanall.sh
fi
chmod +x ${TASK_BIN}/merged_diag_deck.sh
###  ${TASK_BIN}/merge_diag_scripts -o ${TASK_BIN}/merged_diag_deck.sh delmodinfo.sh gpintstat2.sh gsstats7.sh xstats5.sh xtrachem.sh xtraconv2.sh effrdsrbc1.sh aodpth_volc1.sh lakestats.sh sfctile.sh xtradust_bulk.sh tmstats.sh daily_cmip6.sh epflux_cmip6.sh gwdiag_cmip6.sh cosp4_monthly.sh radprof_radforce_cmip6.sh cldmic2.sh rhs_cmip6.sh cleanall.sh
#
mon=${RUNCYCLE_stop_month}
while [ "$mon" -gt 0 ] ; do
#for segments in $(seq ${RUNCYCLE_start_month} ${chunk_size} ${RUNCYCLE_stop_month}) ; do
  source ${TASK_BIN}/merged_diag_decks_parallel # >mg_diagjob_${runyear}.log 2>&1
  mon=$(( mon - chunk_size ))
done
