#!/bin/bash
# Upload data files to backend

_checkpoint 0 "GEM_module StageIN.tsk"

scp_cmd_opt1="$(echo ' '${MOD_GEM_scp_opt} | sed 's/ -/ =-/g')"
nthreads=$(nodeinfo -n ${SEQ_NODE} | grep node.cpu= | cut -d '=' -f 2)
nthreads=${nthreads##*x}
if [[ ${nthreads} -lt 1 ]] ; then nthreads=1 ; fi

abort_prefix=UploadTSK_abort_

nb_cfg=$(ls -1d ${TASK_INPUT}/cfg_* | wc -l)
if [[ ${nb_cfg} -lt 1 ]] ; then nb_cfg=1 ; fi
sub_par=$((nthreads/nb_cfg))
if [[ ${sub_par} -lt 1 ]] ; then sub_par=1 ; fi

# Treat ${TASK_INPUT}/shared
file=${TASK_INPUT}/shared
bname=$(basename ${file})
listing=StageIN_${bname}.lis
_checkpoint "StageIN.tsk"
${TASK_BIN}/Um_upload.sh -inrep ${file} -nthreads ${sub_par} \
           -rsync_cmd ${TASK_BIN}/rsync_cmd \
           -abortf ${abort_prefix}${bname} 1> ${listing} 2>&1
_checkpoint "StageIN.tsk"

# Loop to treat multi-domains

count=0
for directory in $(ls -1d ${TASK_INPUT}/cfg_*) ${TASK_INPUT}/prep_task ; do
   count=$(( count + 1 ))
   bname=$(basename ${directory})
   listing=StageIN_${bname}.lis
   ${TASK_BIN}/Um_upload.sh -inrep ${directory} -nthreads ${sub_par} \
              -rsync_cmd ${TASK_BIN}/rsync_cmd \
              -abortf ${abort_prefix}upload_${bname} 1> ${listing} 2>&1 &
   if [[ ${count} -eq ${nthreads} ]]; then
      _checkpoint "StageIN.tsk" ; wait ; _checkpoint "StageIN.tsk"
      count=0
   fi
done

${TASK_BIN}/Upload_binaries.sh -bindir ${MOD_GEM_bindir} -destination ${TASK_OUTPUT}/shared/ATM_MOD -scp_cmd ${TASK_BIN}/scp_cmd -scp_opt "${scp_cmd_opt1}"


_checkpoint "StageIN.tsk" ; wait ; _checkpoint "StageIN.tsk"
abort_prefix=UploadTSK_abort_

# Check for aborted functions
if [[ $(find . -name "${abort_prefix}*" | wc -l) -gt 0 ]] ; then
   echo "ERROR: One or more function calls aborted ... see work/Um_upload_*.lis listings for details"
   exit 1
fi

#if [[ ${MOD_GEM_clean} -gt 0 && ${SAME_MACH} -lt 1 ]] ; then
#  /bin/rm -rf ${SEQ_WORKBASE}/${SEQ_CONTAINER}/Prep${SEQ_CONTAINER_LOOP_EXT}/output/cfg_* &
#fi

_checkpoint "StageIN.tsk"
cd ${TASK_OUTPUT}/prep_task
for directory in $(ls -1d cfg_*) ; do
   if [[ -L ${directory} ]] ; then
      for subd in $(ls -1d $(readlink ${directory})/*) ; do
         ln -s ${subd} ../${directory}
      done
   else
      mv ${directory}/* ../${directory}
   fi
done
cd ${TASK_WORK}
_checkpoint "StageIN.tsk"

launch_model_task() {
   set -x
   # Launch main model task
   if [[ ${SEQ_XFER} != "stop" ]] ; then
      first_index='000'
      if [[ -n "${SEQ_CONTAINER_LOOP_ARGS}" ]]; then
         ${SEQ_BIN}/maestro -n ${SEQ_CONTAINER}/Runmod -s submit ${SEQ_CONTAINER_LOOP_ARGS},Runmod=${first_index} -f ${SEQ_XFER}
      else
         ${SEQ_BIN}/maestro -n ${SEQ_CONTAINER}/Runmod -s submit -l Runmod=${first_index} -f ${SEQ_XFER}
      fi
   fi
}

# Launch main model task (when NOT coupled)
if [[ -z "${MOD_GEM_cpl_expname}" ]] ; then launch_model_task ; fi

wait

_checkpoint 1 "GEM_module StageIN.tsk"
