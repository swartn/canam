# CanAM5 GEM

**WARNING** : this document and code is in alpha. There are no guarantees whatsoever!

These are initial instructions to attempt to compile CanAM5-GEM off the `develop_canesm` branch.

1. **Basic profile setup.**
    See One-time setup in https://gitlab.science.gc.ca/CanESM/cccma-u2-migration/wikis/u2-guidance/Maestro-one-time-setup

2. **Clone the CanESM repo.**
    Typically we put it in /home/ords/crd/ccrn/$USER:

    ```bash
    # note that we clone down the super repo for dependencies in the CanDIAG and CanCPL repos
    git clone --recursive git@gitlab.science.gc.ca:CanESM/CanESM5.git mytest

    # checkout the CanESM5 development branch
    cd mytest
    git scheckout develop_canesm     # note the 's'
    ```
    This pulls down the CanESM5 super-repo from the central repo. and checks out the `develop_canesm` branch.
    You might use your fork instead in future.

3. **Setup for compilation**

    ```bash
    # Create a staging directory for GEM compilation on HOME/ORDS.
    mkdir -p /home/ords/crd/ccrn/${USER}/storage_model

    cd CanAM
    . r.load.dot eccc/mrd/rpn/MIG/GEM/5.1.2 # set GEM environment

    # Where the build is done. Change as appropriate for you. Gets large!
    export storage_model=/home/ords/crd/ccrn/${USER}/storage_model/
    ```

4. **Compile**

    ```bash
    cd gem
    ./compile_canam_gem.sh   # Run the compile script to setup and build GEM executable.
                             # Note: this builds a gem_* and interface_* directories in $storage_model.
    ```


5. **Setup the maestro suite and lunch with maestro**

    ```bash
    mkdir -p ~/.suites/gem/5.1.2
    rsync -az --exclude=listings --exclude=hub --exclude=sequencing --exclude=logs maestrofiles/ ~/.suites/gem/5.1.2/mytest
    cd ~/.suites/gem/5.1.2/mytest
    export SEQ_EXP_HOME=$(pwd)
    mkdir -p hub listings sequencing logs
    makelinks -f

    # Lunch the run with maestro
    bin/start_exp $PWD

    # If you want to launch xflow at a later time after the initial launch
    xflow -exp ~/.suites/gem/5.1.2/mytest &
    ```

### Development Notes

As of June 2020, `gem` integration into `CanAM5` and `CanESM5` is _currently ongoing_, with the main development happening
on the `canam_gem` branch of the [central CanESM5 repository](https://gitlab.science.gc.ca/CanESM/CanESM5), with most of the
development occuring in the [CanAM submodule](https://gitlab.science.gc.ca/CanESM/CanAM).

During this time, you may be working on this branch while _another_ developer is also working on it - if this is the case,
you may get the following error when trying to push your changes:

#### Developing in the CanESM framework
As this work is happening within the `CanESM5` super-repo, it requires developers to work with multiple "sub-repos".
For interested parties, they can read up on sub-repos and submodules [here](https://git-scm.com/book/en/v2/Git-Tools-Submodules),
[here](https://www.vogella.com/tutorials/GitSubmodules/article.html), and many other locations on the internet. However,
to aid developers in working with submodules, we have prepared many `git-s*` commands that can be used to easily interact
with submodules, and using them in the context of `CanESM5` are documented
[here](https://gitlab.science.gc.ca/CanESM/CanESM5/blob/develop_canesm/Developing_readme.md#modifying-the-code).
In specific relation to the `CanAM` `GEM` development, since this work is occuring on an existing branch of the main repo,
developers only need to worry about directions to "**Add and commit the changes**" and "**Push changes back to gitlab**",
but it should be noted that using the `git sadd`, `git scommit`, and `git spush` **requires users to be at the top level**
**of the repo** when they do (i.e. the level that contains the `CanAM` directory).

#### Syncing Changes with Other Developers

```
>> git spush origin canam_gem
...
To gitlab.science.gc.ca:CanESM/CanAM.git
 ! [rejected]        canam_gem -> canam_gem (fetch first)
nt: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
...
```
If this happens, this means that another developer has pushed changed to this branch. Fortunately, all is not lost! You
simply need to pull down the changes and merge them with yours before you can push up the new updates. To do this, run

```bash
git sfetch origin canam_gem
git pull
```
then deal with any conflicts that come up (if any) and then try pushing the branch again.
