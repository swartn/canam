!>\file
!>\brief Holds the arrays used by the radiative transfer calculations.
!!
!! @author Jason Cole
!

module arrays_mod

  ! Define here allocatable arrays needed for the radiative transfer calculations.
  ! Included also are a few scalars that will be read from the input.
  
  ! Note that this includes arrays that must be called to run the stochastic cloud 
  ! generator to produce the necessary fields.
  
  ! Scalars that should be set when the input file is read.
  
  implicit none

  integer :: jlat
  integer :: kount
  
  ! Real arrays of rank 3 used for raddriv call
  ! These are dimensioned (ilg,ilev,idimr)
  real, allocatable, dimension(:,:,:) :: aerin

  ! These are dimensioned (ilgz,levsa,nbs)
  real, allocatable, dimension(:,:,:) :: sw_ext_sa
  real, allocatable, dimension(:,:,:) :: sw_ssa_sa
  real, allocatable, dimension(:,:,:) :: sw_g_sa
  
  ! These are dimensioned (ilgz,levsa,nbl)
  real, allocatable, dimension(:,:,:) :: lw_abs_sa
  
  ! These are dimensioned (ilg, ilev,nxloc)
  real, allocatable, dimension(:,:,:) :: rel_sub
  real, allocatable, dimension(:,:,:) :: rei_sub
  real, allocatable, dimension(:,:,:) :: clw_sub
  real, allocatable, dimension(:,:,:) :: cic_sub
  
  ! These are dimensioned (ilg, im, nbs)
  real, allocatable, dimension(:,:,:) :: salbrot
  real, allocatable, dimension(:,:,:) :: csalrot

  ! These are dimensioned (ilg, im)
  real, allocatable, dimension(:,:) :: emisrot
  

  ! Real arrays of rank 2 used for raddriv call
  
  ! These are dimensioned (ilg,ilev)
  real, allocatable, dimension(:,:) :: hrsrow
  real, allocatable, dimension(:,:) :: hrlrow
  real, allocatable, dimension(:,:) :: hrscrow
  real, allocatable, dimension(:,:) :: hrlcrow
  real, allocatable, dimension(:,:) :: hrsco2row
  real, allocatable, dimension(:,:) :: hrsch4row
  real, allocatable, dimension(:,:) :: hrlo3row
  real, allocatable, dimension(:,:) :: hrlch4row
  real, allocatable, dimension(:,:) :: hrsn2orow
  real, allocatable, dimension(:,:) :: hrln2orow
  ! These are 3d but put them here since they are connected to the 2d arrays above
  real, allocatable, dimension(:,:,:) :: hrsh2orow
  real, allocatable, dimension(:,:,:) :: hrso3row
  real, allocatable, dimension(:,:,:) :: hrso2row
  real, allocatable, dimension(:,:,:) :: hrlh2orow
  real, allocatable, dimension(:,:,:) :: hrlco2row  
  
  real, allocatable, dimension(:,:) :: tbndrol
  real, allocatable, dimension(:,:) :: ea55rol
  real, allocatable, dimension(:,:) :: shj
  real, allocatable, dimension(:,:) :: dshj
  real, allocatable, dimension(:,:) :: dz
  real, allocatable, dimension(:,:) :: unotnd
  real, allocatable, dimension(:,:) :: ozphs ! in physici, this is (ilg,ilev+1) but only a section (ilg,ilev) is sent to raddriv
  real, allocatable, dimension(:,:) :: qc
  real, allocatable, dimension(:,:) :: co2rox
  real, allocatable, dimension(:,:) :: ch4rox
  real, allocatable, dimension(:,:) :: n2orox
  real, allocatable, dimension(:,:) :: f11rox
  real, allocatable, dimension(:,:) :: f12rox
  real, allocatable, dimension(:,:) :: f113rox
  real, allocatable, dimension(:,:) :: f114rox
  real, allocatable, dimension(:,:) :: ccld
  real, allocatable, dimension(:,:) :: rhc
  real, allocatable, dimension(:,:) :: anu
  real, allocatable, dimension(:,:) :: eta
  real, allocatable, dimension(:,:) :: wcdw
  
  real, allocatable, dimension(:,:) :: cldrow
  real, allocatable, dimension(:,:) :: clwrow
  real, allocatable, dimension(:,:) :: cicrow
  real, allocatable, dimension(:,:) :: cldcdd
  real, allocatable, dimension(:,:) :: radeqvw
  real, allocatable, dimension(:,:) :: radeqvi
  real, allocatable, dimension(:,:) :: rlc_cf
  real, allocatable, dimension(:,:) :: rlc_cw
  
  ! These are dimensioned (ilg,lev)
  real, allocatable, dimension(:,:) :: shtj
  
  ! These are dimensioned (ilg,ilev+1)
  real, allocatable, dimension(:,:) :: tfrow
  
  ! These are dimensioned (ilg,ilev+2)
  real, allocatable, dimension(:,:) :: fsaurol
  real, allocatable, dimension(:,:) :: fsadrol
  real, allocatable, dimension(:,:) :: flaurol
  real, allocatable, dimension(:,:) :: fladrol
  real, allocatable, dimension(:,:) :: fscurol
  real, allocatable, dimension(:,:) :: fscdrol
  real, allocatable, dimension(:,:) :: flcurol
  real, allocatable, dimension(:,:) :: flcdrol
  
  ! These are dimensioned (ilg,nbs)
  real, allocatable, dimension(:,:) :: fsdbrol
  real, allocatable, dimension(:,:) :: fsfbrol
  real, allocatable, dimension(:,:) :: csdbrol
  real, allocatable, dimension(:,:) :: csfbrol
  real, allocatable, dimension(:,:) :: fssbrol
  real, allocatable, dimension(:,:) :: fsscbrol
  real, allocatable, dimension(:,:) :: wrkarol
  real, allocatable, dimension(:,:) :: wrkbrol
  real, allocatable, dimension(:,:) :: salbrol
  real, allocatable, dimension(:,:) :: csalrol
  
  ! These are dimensioned (ilg,im)
  real, allocatable, dimension(:,:) :: fsgrot
  real, allocatable, dimension(:,:) :: fsdrot
  real, allocatable, dimension(:,:) :: fsfrot
  real, allocatable, dimension(:,:) :: fsvrot
  real, allocatable, dimension(:,:) :: fsirot
  real, allocatable, dimension(:,:) :: fdlrot
  real, allocatable, dimension(:,:) :: flgrot
  real, allocatable, dimension(:,:) :: fdlcrot
  real, allocatable, dimension(:,:) :: csbrot
  real, allocatable, dimension(:,:) :: clbrot
  real, allocatable, dimension(:,:) :: parrot
  real, allocatable, dimension(:,:) :: csdrot
  real, allocatable, dimension(:,:) :: csfrot
  real, allocatable, dimension(:,:) :: gtrot
  real, allocatable, dimension(:,:) :: farerot

  ! These are dimensioned (ilg,im,nbs).
  real, allocatable, dimension(:,:,:) :: fsdbrot
  real, allocatable, dimension(:,:,:) :: fsfbrot
  real, allocatable, dimension(:,:,:) :: csdbrot
  real, allocatable, dimension(:,:,:) :: csfbrot
  real, allocatable, dimension(:,:,:) :: fsscbrot
  real, allocatable, dimension(:,:,:) :: fssbrot
  
  ! These are dimensioned (ilg,max_sam).  Very important note:  they should be integer not real!!
  real, allocatable, dimension(:,:) :: colnum_sw
  real, allocatable, dimension(:,:) :: colnum_lw
  real, allocatable, dimension(:,:) :: colnum

  ! Real arrays of rank 1 used for raddriv call
  
  real, allocatable, dimension(:) :: fsgrol
  real, allocatable, dimension(:) :: fsdrol
  real, allocatable, dimension(:) :: fsfrol
  real, allocatable, dimension(:) :: fsvrol
  real, allocatable, dimension(:) :: fsirol
  real, allocatable, dimension(:) :: fdlrol
  real, allocatable, dimension(:) :: flgrol
  real, allocatable, dimension(:) :: fdlcrol
  real, allocatable, dimension(:) :: csbrol
  real, allocatable, dimension(:) :: clbrol
  real, allocatable, dimension(:) :: fsrrol
  real, allocatable, dimension(:) :: fsrcrol
  real, allocatable, dimension(:) :: olrrol
  real, allocatable, dimension(:) :: olrcrol
  real, allocatable, dimension(:) :: parrol
  real, allocatable, dimension(:) :: csdrol
  real, allocatable, dimension(:) :: csfrol
  real, allocatable, dimension(:) :: fslorol
  real, allocatable, dimension(:) :: fsamrol
  real, allocatable, dimension(:) :: flamrol
  real, allocatable, dimension(:) :: fsorol
  real, allocatable, dimension(:) :: exb1rol
  real, allocatable, dimension(:) :: exb2rol
  real, allocatable, dimension(:) :: exb3rol
  real, allocatable, dimension(:) :: exb4rol
  real, allocatable, dimension(:) :: exb5rol
  real, allocatable, dimension(:) :: exbtrol
  real, allocatable, dimension(:) :: odb1rol
  real, allocatable, dimension(:) :: odb2rol
  real, allocatable, dimension(:) :: odb3rol
  real, allocatable, dimension(:) :: odb4rol
  real, allocatable, dimension(:) :: odb5rol
  real, allocatable, dimension(:) :: odbtrol
  real, allocatable, dimension(:) :: odbvrol
  real, allocatable, dimension(:) :: ofb1rol
  real, allocatable, dimension(:) :: ofb2rol
  real, allocatable, dimension(:) :: ofb3rol
  real, allocatable, dimension(:) :: ofb4rol
  real, allocatable, dimension(:) :: ofb5rol
  real, allocatable, dimension(:) :: ofbtrol
  real, allocatable, dimension(:) :: abb1rol
  real, allocatable, dimension(:) :: abb2rol
  real, allocatable, dimension(:) :: abb3rol
  real, allocatable, dimension(:) :: abb4rol
  real, allocatable, dimension(:) :: abb5rol
  real, allocatable, dimension(:) :: abbtrol
  real, allocatable, dimension(:) :: exs1rol
  real, allocatable, dimension(:) :: exs2rol
  real, allocatable, dimension(:) :: exs3rol
  real, allocatable, dimension(:) :: exs4rol
  real, allocatable, dimension(:) :: exs5rol
  real, allocatable, dimension(:) :: exstrol
  real, allocatable, dimension(:) :: ods1rol
  real, allocatable, dimension(:) :: ods2rol
  real, allocatable, dimension(:) :: ods3rol
  real, allocatable, dimension(:) :: ods4rol
  real, allocatable, dimension(:) :: ods5rol
  real, allocatable, dimension(:) :: odstrol
  real, allocatable, dimension(:) :: odsvrol
  real, allocatable, dimension(:) :: ofs1rol
  real, allocatable, dimension(:) :: ofs2rol
  real, allocatable, dimension(:) :: ofs3rol
  real, allocatable, dimension(:) :: ofs4rol
  real, allocatable, dimension(:) :: ofs5rol
  real, allocatable, dimension(:) :: ofstrol
  real, allocatable, dimension(:) :: abs1rol
  real, allocatable, dimension(:) :: abs2rol
  real, allocatable, dimension(:) :: abs3rol
  real, allocatable, dimension(:) :: abs4rol
  real, allocatable, dimension(:) :: abs5rol
  real, allocatable, dimension(:) :: abstrol
  real, allocatable, dimension(:) :: pressg
  real, allocatable, dimension(:) :: gtrow
  real, allocatable, dimension(:) :: oztop
  real, allocatable, dimension(:) :: cszrow
  real, allocatable, dimension(:) :: vtaurow
  real, allocatable, dimension(:) :: troprow
  real, allocatable, dimension(:) :: emisrow
  real, allocatable, dimension(:) :: cldtrol
  real, allocatable, dimension(:) :: radj
  real, allocatable, dimension(:) :: tsi !< Total solar irradiance
  real, allocatable, dimension(:) :: solv
  
  ! Integer of rank 2
  ! These are sized (ilg,4)
  integer, allocatable, dimension(:,:) :: iseedrow
  
  ! Integer of rank 1
  ! These are sized (ilg)
  integer, allocatable, dimension(:) :: ncldy
  
end module arrays_mod

!> \file
!> Holds the arrays used by the radiative transfer calculations.
