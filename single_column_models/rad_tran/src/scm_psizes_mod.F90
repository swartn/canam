!>\file
!>\brief Holds additional Physics parameters/constants used in single column radiative transfer model
!! calculations.
!!
!! @author Deji Akingunola
!
module psizes_19
  use parameters_mod, only : levsa, &
                             nbs,   &
                             nbl
  implicit none
! define the pdf to use for the horizontal variability of cloud condensate
! idist = 1 use beta distribution
! idist = 2 use gamma distribution
      integer, parameter :: idist = 2

end module psizes_19
!> \file
!> Additional model Physics parameters.
