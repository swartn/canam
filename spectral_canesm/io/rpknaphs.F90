!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine rpknaphs(nf,g,nlev,ibuf,lon,nlat,ilat,lev, &
                    itrlvs,itrlvf,lh,gg,ok)

  !     *                        new routine, based on rpkphs4, for na tracers
  !
  implicit none
  integer, intent(in) :: ilat
  integer, intent(in) :: itrlvf
  integer, intent(in) :: itrlvs
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: lon
  integer, intent(in) :: nf
  integer, intent(in) :: nlat
  integer, intent(in) :: nlev

  integer, intent(inout), dimension(8) :: ibuf !< Variable description\f$[units]\f$
  integer, intent(in), dimension(nlev) :: lh !< Variable description\f$[units]\f$
  real, intent(inout) :: g (lon * ilat + 1, nlev) !< Variable description\f$[units]\f$
  real, intent(in) :: gg( (lon + 1) * nlat) !< Variable description\f$[units]\f$
  logical, intent(inout) :: ok !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer :: i
  integer :: l
  integer :: lon1
  integer, dimension(8) :: kbuf

  !-----------------------------------------------------------------------
  ok = .false.
  lon1 = lon + 1
  !
  do i = 1,8
    kbuf(i) = ibuf(i)
  end do ! loop 30
  !
  do l = itrlvs,itrlvf
    kbuf(4) = lh(l)
    call getggbx(g(1,l),kbuf(3),nf,lon1,nlat,kbuf(2),kbuf(4),gg)
  end do ! loop 100
  !
  do i = 1,8
    ibuf(i) = kbuf(i)
  end do ! loop 300
  !
  ok = .true.
  return
  !--------------------------------------------------------------------
end subroutine rpknaphs
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
