!>\file
!>\brief
!!
!! @author David Plummer
!
module read_def_arrays_mod

! Read and define arrays coming from external files.
!
! The subroutines in this module are called from read_chem_forcings.F90 when
! the logical phys_options%gas_chem=TRUE.
! The purpose of the subroutines contained here is to read the input fields
! for gas-phase chemistry from external files.
!

implicit none ; private

public :: getacem, getssad, getsfsa, getemis

contains

! -----------------------------------------------------------------------------

  subroutine getssad (apak, apal, lsad, kount, nlat, lon, &
                      ilat, ijpak, levssad, nfext)
! ------------------------------------------------------------------------------
! Based on getzon9
!     * Jul 23, 2021 - B.Winter  : adapt to CanAM5 and F90 syntax
!     * Nov 23, 2015 - D.Plummer : modify to read transient fields
!     * Jun 04, 2008 - J.Scinocca: modify for CCMVAL sulphate aerosols
!
! Incoming fields are zonal means on 64 latitudes and 61 pressure levels.
! Called by /lsmod/read_chem_forcings.F90
! ------------------------------------------------------------------------------

  use chem_params_mod, only : idyovr
  implicit none

  integer, intent(in) :: ijpak   !< ijpak = lonsl*ilat+1 (128*(64/32)+1)
  integer, intent(in) :: ilat, kount, lon, nlat
  integer, intent(in) :: levssad  !< no. levels of specified stratospheric aerosol surface area density
  integer, intent(in) :: nfext  !< unit number of external file: nussad=32
  !
  ! * Arrays for cross-sections read in from external file
  integer, dimension(levssad), intent(in) :: lsad
  !
  ! * Output 3-D pak fields (current and target).
  real, dimension(ijpak,levssad), intent(out) :: apak, apal
  !
  integer :: iyear, imdh, myrssti, isavdts
  common /keeptim/ iyear, imdh, myrssti, isavdts
  !
  integer*4 :: mynode
  integer :: ibuf, idat, machine, intsize
  common /icom/ ibuf(8),idat(1)
  common /machtyp/ machine, intsize
  common /mpinfo/ mynode
  !
  integer :: i, iiday, imo, imot, inday, j, jend, jne, jnp, jse, jsp, jstart
  integer :: l, maxx, nc4to8, ni, nl, nlatq, nlath
  logical :: ok
  real, dimension(nlat,levssad) :: ai, am

! ------------------------------------------------------------------------------

  if (levssad /= 61) then
    write(6,*)'getssad: the number of levels in SSAD data to read does not'
    write(6,*)'         match the expected number:', levssad
    call xit('getssad',-10)
  endif
  !
  imo = imdh / 10000
  iiday = (iyear*100) + imo
  if (imo /= 12) then
    imot = imo + 1
    inday = 100*iyear + imot
  else
    imot = 1
    inday = (iyear+1)*100 + imot
  endif

  ! Dave's workaround for CMIP6. Force using 1850 for any year before 1850.
  if (iyear < 1850) then
    iiday = 185000 + imo
    inday = 185000 + imot
  endif

    ! When this is true, IDYOVR gives the year to use for emissions.
  if (idyovr > 0) then
    iiday = 100*idyovr + imo
    inday = 100*idyovr + imot
  endif

  if (kount == 0) inday = iiday
  !
  ! * Find and read loss rate cross-sections.
  !
  maxx = (nlat+8) * machine
  rewind nfext
  call find (nfext, nc4to8("ZONL"), iiday, nc4to8("SSAD"), -1, ok)
  if (.not. ok) call xit('getssad',-1)
  !
  do l = 1, levssad
    call getfld2 (nfext, ai(1,l), nc4to8("ZONL"), iiday, nc4to8("SSAD"), &
                  lsad(l), ibuf, maxx, ok)

    if (.not. ok) then
      print *, 'getssad: problem with namtm; l, levssad = ', l, levssad
      call xit('getssad',-2)
    endif
  enddo
  !
  rewind nfext
  call find (nfext, nc4to8("ZONL"), inday, nc4to8("SSAD"), -1, ok)
  if (.not. ok) call xit('getssad',-3)
  !
  do l = 1, levssad
    call getfld2 (nfext, am(1,l), nc4to8("ZONL"), inday, nc4to8("SSAD"), &
                  lsad(l), ibuf, maxx, ok)
    if (.not. ok) then
      print *, 'getssad: problem with namtm; l,levssad = ', l, levssad
      call xit('getssad',-4)
    endif
  enddo
  !
  ! * MPI hook.
  jstart = mynode*ilat + 1
  jend   = mynode*ilat + ilat
  !
  ! * Process the levels one at a time.
  !
  if (mod(nlat,4) /= 0) call xit('getssad',-9)
  nlath = nlat / 2
  nlatq = nlat / 4

  do l = 1, levssad
    ni = 0
    nl = 0
    !
    ! * Create 3-D packed fields.
    !
    do j = 1, nlatq

      jsp = j
      nl = nl+1
      if (nl >= jstart .and. nl <= jend) then
        do i = 1, lon
          ni = ni + 1
          apak(ni,l) = ai(jsp,l)
          apal(ni,l) = am(jsp,l)
        enddo
      endif
      !
      jnp = nlat-j+1
      nl = nl+1
      if (nl >= jstart .and. nl <= jend) then
        do i = 1, lon
          ni = ni + 1
          apak(ni,l) = ai(jnp,l)
          apal(ni,l) = am(jnp,l)
        enddo
      endif
      !
      jse = nlath-j+1
      nl = nl + 1
      if (nl >= jstart .and. nl <= jend) then
        do i = 1, lon
          ni = ni + 1
          apak(ni,l) = ai(jse,l)
          apal(ni,l) = am(jse,l)
        enddo
      endif
      !
      jne = nlath + j
      nl=nl + 1
      if(nl >= jstart .and. nl <= jend) then
        do i = 1, lon
          ni = ni + 1
          apak(ni,l) = ai(jne,l)
          apal(ni,l) = am(jne,l)
        enddo
      endif
    enddo  ! loop 500
  enddo    ! loop 600
  !
  end subroutine getssad

! -----------------------------------------------------------------------------

  subroutine getsfsa (sfsapak, sfsapal, nlon, nlat, ijpak, iday, kount, nf)
  ! ----------------------------------------------------------------------------
  ! * Aug 19/2021 - B. Winter: modify to interpolate incoming SO4 horizontally
  ! * Sep 28/2004 - D. Plummer
  !               - modified GETZON9 for use with sulphate surface area fields
  !
  ! Incoming fields are on a 129x64 horizontal grid, with 17 pressure levels.
  ! Time and vertical interpolation are called from physici.F90.
  !
  ! Reference from msizes: size of gg array, whatever it is
  ! ngg = max(ilgsld * nlatd,((ilev + 2) ** 2) * nlatd
  !     = max((192+2)* 96   ,((  49 + 2) ** 2) * 96
  !
  ! Called by /lsmod/read_chem_forcings.F90
  ! ----------------------------------------------------------------------------

  use chem_params_mod, only : nlso4, pso4, mmd
  implicit none

  integer, intent(in) :: nf      !< unit number of file to open: nuso4=33
  integer, intent(in) :: ijpak   !< ijpak = lonsl*ilat+1 = (128*(64/32)+1)
  integer, intent(in) :: iday, kount, nlat, nlon
  real, dimension(ijpak,nlso4), intent(out) :: sfsapak, sfsapal

  real, dimension(nlon, nlat) :: gg

  integer*4 :: mynode
  common /mpinfo/ mynode

  integer :: l, m, mdayr, n, nc4to8

! ------------------------------------------------------------------------------

  m = 0
  do n = 1, 12
    if (iday >= mmd(n)) m = n + 1
  enddo
  if (m == 0 .or. m == 13) m = 1

  mdayr = mmd(m)
  !if (mynode == 0) then
  !  write(6,*) "getsfsa: Reading SO4 fields from unit =", nf
  !  write(6,*) "         iday, mdayr, nlso4", iday, mdayr, nlso4
  !  write(6,*) "         gg(1:10)", gg(1:10)
  !endif

  rewind nf

  ! At start-up, the sulphate fields will be held constant for the first
  ! half-month.

  if (kount == 0) then
    do l = 1, nlso4
      call getggbx (sfsapak(1,l), nc4to8(" SO4"), nf, nlon, nlat, &
                    mdayr, int(pso4(l)), gg)
    enddo
    rewind nf
    do l = 1, nlso4
      call getggbx (sfsapal(1,l), nc4to8(" SO4"), nf, nlon, nlat, &
                    mdayr, int(pso4(l)), gg)
    enddo
  else
    do l = 1, nlso4
      call getggbx (sfsapal(1,l), nc4to8(" SO4"), nf, nlon, nlat, &
                    mdayr, int(pso4(l)), gg)
    enddo
  endif
  !
  end subroutine getsfsa

! ----------------------------------------------------------------------------

  subroutine getemis (sfempak, sfempal, nf, nlon, nlat, &
                      ijpak, iday, kount, namts)

! ------------------------------------------------------------------------------
! Read in monthly 2-D emission files
!
! When reading the date, a timeslice run will cycle over the year given by
! IDYOVR (currently hard-coded to be 2000), while the transient run advances
! through time based on IYEAR.
! The IDYOVR flag allows for an override of the standard date calculation
! if it is greater than zero. When IDYOVR>0, a repeating annual cycle for the
! year given by IDYOVR is read.
! Incoming horizontal grid is 129x64.
! Called by /lsmod/read_chem_forcings.F90
! ------------------------------------------------------------------------------

  use chem_params_mod, only : idyovr, mmd
  implicit none

  integer, intent(in) :: nf  !< unit number of file to read: nusfcem=30
  integer, intent(in) :: ijpak   !< ijpak = lonsl*ilat+1 (128*(64/32)+1)
  integer, intent(in) :: iday, kount, namts, nlon, nlat
  real, dimension(ijpak), intent(out) :: sfempak, sfempal

  integer :: incy, m, mbuf2, n

  real, dimension(nlon, nlat) :: gg
  integer :: iyear, imdh, myrssti, isavdts
  common /keeptim/ iyear, imdh, myrssti, isavdts

! ------------------------------------------------------------------------------

  m = 0
  incy = 0

  do n = 1, 12
    if (iday >= mmd(n)) m = n + 1
  enddo
  if (m == 0) m=1
  if (m == 13) then
    m = 1
    incy = 1
  endif
  !
  if (myrssti == 0) then
    mbuf2 = 200000 + m
  else
    mbuf2 = 100*(iyear+incy) + m
  endif

  if (idyovr > 0) mbuf2 = 100*idyovr + m

 ! write(6,*) 'getemis: Reading new emission fields', iday, mbuf2

  if (kount == 0) then
    call getagbx (sfempak, namts, nf, nlon, nlat, mbuf2, gg)
    call getagbx (sfempal, namts, nf, nlon, nlat, mbuf2, gg)
  else
    call getagbx (sfempal, namts, nf, nlon, nlat, mbuf2, gg)
  endif
  !
  end subroutine getemis

! ----------------------------------------------------------------------------

  subroutine getacem (acempak, acempal, lace, nf, nlon, nlat, ijpak,  &
                      iday, kount, namts)
  !
  ! Read in the 3-D fields of monthly aircraft emissions that are provided on
  ! a constant-height grid. The IDYOVR flag allows overriding the standard date
  ! calculation when it is set to a value greater than zero. When this is the
  ! case, a repeating annual cycle for the year given by IDYOVR is read.
  !
  ! Called by /lsmod/read_chem_forcings.F90
  !
  use chem_params_mod, only : idyovr, mmd, nlvarc

  implicit none

  integer, intent(in) :: nf  !< unit number of file to read: nuacftem=31
  integer, intent(in) :: ijpak   !< ijpak = lonsl*ilat+1 (128*(64/32)+1)
  integer, intent(in) :: iday, kount, namts, nlat, nlon
  integer, dimension(nlvarc), intent(in) :: lace

  real, dimension(ijpak,nlvarc), intent(out) :: acempak, acempal

  real, dimension(nlon, nlat) :: gg
  integer :: i, k, m, n, incy, mbuf2

  integer :: iyear, imdh, myrssti, isavdts
  common /keeptim/ iyear, imdh, myrssti, isavdts

! -----------------------------------------------------------------------------
  m = 0
  incy = 0
  do n = 1, 12
    if (iday >= mmd(n)) m = n + 1
  enddo

  if (m == 0) m=1
  if (m == 13) then
    m = 1
    incy = 1
  endif

  if (myrssti == 0) then
    mbuf2 = 200000 + m
  else
    mbuf2 = 100*(iyear+incy) + m
  endif
  if (idyovr > 0) mbuf2 = 100*idyovr + m

!  write(6,*) "getemis: Reading new ACFT emission fields", iday, mbuf2
  rewind nf
  !
  ! Aircraft emissons are zero for years before 1920 and no fields have been
  ! included in the AC emissions file.

  if (mbuf2 > 192000) then
    if (kount == 0) then
      do k = 1, nlvarc
        call getggbx (acempak(1,k), namts, nf, nlon, nlat, mbuf2, lace(k), gg)
      enddo

      rewind nf
      do k = 1, nlvarc
        call getggbx (acempal(1,k), namts, nf, nlon, nlat, mbuf2, lace(k), gg)
      enddo
    else
      do k = 1, nlvarc
        call getggbx (acempal(1,k), namts, nf, nlon, nlat, mbuf2, lace(k), gg)
      enddo
    endif

  else

    do k = 1, nlvarc
      do i = 1, ijpak
        acempak(i,k) = 0.0
      enddo
    enddo
    do k = 1, nlvarc
      do i = 1, ijpak
        acempal(i,k) = 0.0
      enddo
    enddo
  endif

  end subroutine getacem

! ----------------------------------------------------------------------------

end module read_def_arrays_mod
