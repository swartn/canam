!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine swaps(s,wrks,la,ilev)

  !     * nov 12/92. -  a.j.stacey.
  !
  !     * transpose s(2,la,ilev) to s(2,ilev,la)
  !
  implicit none
  integer :: i
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: j
  integer, intent(in) :: la

  real, intent(in), dimension(2,la,ilev) :: s !< Variable description\f$[units]\f$
  real, intent(inout), dimension(2,ilev,la) :: wrks !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-----------------------------------------------------------------------
  do j=1,la
    do i=1,ilev
      wrks(1,i,j) = s(1,j,i)
      wrks(2,i,j) = s(2,j,i)
    end do
  end do ! loop 10
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
