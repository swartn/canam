!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine swapf4x(fout,fin,ilev,nlat,ilat,joff,ilh)

  !     * aug 3/2003 - m.lazare. like swapf4 except input array
  !     *                        is fin(2,ilh,nlat,ilev) instead of
  !     *                        f(ilev,nlat,2,ilh), and output array
  !     *                        is over all ilat latitudes (no
  !     *                        works work array).
  !     * nov 12/92. -  a.j.stacey. previous version swapf4.
  !
  !     * transpose fin(2,ilh,nlat,ilev) to fout(2,ilev,ilat,ilh).
  !
  implicit none
  integer, intent(in) :: ilat
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilh
  integer, intent(in) :: joff
  integer, intent(in) :: nlat

  real, intent(in),  dimension(2,ilh,nlat,ilev) :: fin !< Variable description\f$[units]\f$
  real, intent(out), dimension(2,ilev,ilat,ilh) :: fout !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer :: i
  integer :: j
  integer :: l
  !-----------------------------------------------------------------------
  do i=1,ilh
    do j=1,nlat
      do l=1,ilev
        fout(1,l,joff+j,i) = fin(1,i,j,l)
        fout(2,l,joff+j,i) = fin(2,i,j,l)
      end do
    end do
  end do
  !
  return
end subroutine swapf4x
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
