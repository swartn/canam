!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!     * mar 16/2019 - j. cole     - add  3-hour 2d fields (cf3hr,e3hr)
!     * nov 29/2018 - j. cole     - add/adjust tendencies
!     * nov 02/2018 - j. cole     - add radiative flux profiles
!     * nov 20/2018 - m.lazare.   - ensure eco2pak and eco2pal are defined for all cases.
!     * nov 03/2018 - m.lazare.   - added gsno and fnla.
!     *                           - added 3hr save fields.
!     * oct 01/2018 - s.kharin.   - add xtvipas for sampled burdens.
!     * aug 23/2018 - vivek arora - remove pfhc and pexf
!     * aug 21/2018 - m.lazare.     remove {alsw,allw}.
!     * aug 14/2018 - m.lazare.     remove maskpak,gtpal.
!     * aug 14/2018 - m.lazare.     remove {gtm,sicp,sicm,sicnp,sicm}.
!     * aug 01/2018 - m.lazare.     remove {obeg,obwg,res,gc}.
!     * jul 30/2018 - m.lazare.   - unused fmirow removed.
!     *                           - removed non-transient aerosol emission cpp blocks.
!     * mar 09/2018 - m.lazare.   - beg  and bwg  changed from pak/row to
!     *                             pal/rol.
!     * feb 27/2018 - m.lazare.   - qfsl and begl changed from pak/row to
!     *                             pal/rol.
!     *                           - added {begk,bwgk,bwgl,qfso}.
!     * apr 19/2018 - m.lazare.    - add eco2 arrays (current and target).
!     * feb 06/2018 - m.lazare.    - add {ftox,ftoy} and {tdox,tdoy}.
!     * nov 01/2017 - j. cole.     - add arrays to support cmip6 stratospheric
!     *                              aerosols.
!     * aug 10/2017 - m.lazare.    - add fnpat.
!     *                            - flak->{flkr,flku}.
!     *                            - licn->gicn.
!     *                            - add lakes arrays.
!     *                            - wsnopat dimension changed from
!     *                              to im to support use over
!     *                              sea/lake ice.
!     *                            - add local rflndpak to pass to
!     *                              coupler init for non-ocean fraction.
!     * aug 07/2017 - m.lazare.    initial git verison.
!     * mar 26/2015 - m.lazare.    final version for gcm18:
!     *                            - unused {cstpal,cltpal} removed.
!     *                            - add (for nemo/cice support):
!     *                              obegpal,obwgpal,begopal,bwgopal,
!     *                              begipal,hflipal,
!     *                              hseapal,rainspal,snowspal.
!     *                            - remove: ftilpak.
!     *                            - add (for harmonization of field
!     *                              capacity and wilting point between
!     *                              ctem and class): thlwpat.
!     *                            - add (for soil colour index look-up
!     *                              for land surface albedo):
!     *                              algdvpat,algdnpat,
!     *                              algwvpat,algwnpat,socipat.
!     *                            - add (for fractional land/water/ice):
!     *                              salbpat,csalpat,emispak,emispat,
!     *                              wrkapal,wrkbpal,snopako.
!     *                            - add (for pla):
!     *                              psvvpak,pdevpak,pdivpak,prevpak,
!     *                              privpak.
!     * feb 04/2014 - m.lazare.    interim version for gcm18:
!     *                            - ntld used instead of im for land-only
!     *                              mosaic fields.
!     *                            - flakpak,flndpak,gtpat,licnpak added.
!     *                            - {thlqpak,thicpak} removed.
!     * nov 19/2013 - m.lazare.    cosmetic: remove {gflx,ga,hbl,ilmo,pet,ue,
!     *                                      wtab,rofs,rofb) "PAT" arrays.
!     * jul 10/2013 - m.lazare/    previous version compak11 for gcm17:
!     *               k.vonsalzen/ - fsf added as prognostic field
!     *               j.cole/        rather than residual.
!     *                            - extra diagnostic microphysics
!     *                              fields added:
!     *                              {sedi,rliq,rice,rlnc,cliq,cice,
!     *                               vsedi,reliq,reice,cldliq,cldice,ctlnc,cllnc}.
!     *                            - new emission fields:
!     *                              {sbio,sshi,obio,oshi,bbio,bshi} and
!     *                              required altitude field alti.
!     *                            - many new aerosol diagnostic fields
!     *                              for pam, including those for cpp options:
!     *                              "pfrc", "xtrapla1" and "xtrapla2".
!     *                            - the following fields were removed
!     *                              from the "aodpth" cpp option:
!     *                              {sab1,sab2,sab3,sab4,sab5,sabt} and
!     *                              {sas1,sas2,sas3,sas4,sas5,sast},
!     *                              (both pak and pal).
!     *                            - due to the implementation of the mosaic
!     *                              for class_v3.6, prognostic fields
!     *                              were changed from pak/row to pat/rot,
!     *                              with an extra "IM" dimension (for
!     *                              the number of mosaic tiles).
!     *                            - the instantaneous band-mean values for
!     *                              solar fields are now prognostic
!     *                              as well: {fsdb,fsfb,csdb,csfb,fssb,fsscb}.
!     *                            - removed "histemi" fields.
!     * nb: the following are intermediate revisions to frozen gcm16 code
!     *     (upwardly compatible) to support the new class version in development:
!     * oct 19/2011 - m.lazare.    - add thr,thm,bi,psis,grks,thra,hcps,tcs,thfc,
!     *                              psiw,algd,algw,zbtw,isnd,igdr.
!     * oct 07/2011 - m.lazare.    - add gflx,ga,hbl,pet,ilmo,rofb,rofs,ue,wtab.
!     * jul 13/2011 - e.chan.      - for selected variables, add new packed tile
!     *                              versions (pat) or convert from pak to pat.
!     *                            - add tpnd, zpnd, tav, qav, wsno, tsfs.
!     * may 07/2012 - m.lazare/    previous version compak10 for gcm16:
!     *               k.vonsalzen/ - modify fields to support a newer
!     *               j.cole/        version of cosp which includes
!     *               y.peng.        the modis, misr and ceres and
!     *                              save the appropriate output.
!     *                            - remove {fsa,fla,fstc,fltc} and
!     *                              replace by {fsr,olr,fsrc,olrc}.
!     *                            - new conditional diagnostic output
!     *                              under control of "XTRADUST" and
!     *                              "AODPTH".
!     *                            - additional mam radiation output
!     *                              fields {fsan,flan}.
!     *                            - additional correlated-k output
!     *                              fields: "FLG", "FDLC" and "FLAM".
!     *                            - "FLGROL_R" is actual calculated
!     *                              radiative forcing term instead
!     *                              of "FDLROL_R-SIGMA*T**4".
!     *                            - include "ISEED" for random number
!     *                              generator seed now calculated
!     *                              in model driver instead of physics.
!     * may 02/2010 - m.lazare/    previous version compak9i for gcm15i:
!     *               k.vonsalzen/ - add fsopal/fsorol ("FSOL").
!     *               j.cole.      - add fields {rmix,smix,rref,sref}
!     *                              for cosp input, many fields for
!     *                              cosp output (with different options)
!     *                              and remove previous direct isccp
!     *                              fields.
!     *                            - add new diagnostic fields:
!     *                              swa (pak/row and pal/rol),swx,swxu,
!     *                              swxv,srh,srhn,srhx,fsdc,fssc,fdlc
!     *                              and remove: swmx.
!     *                            - for convection, add: dmcd,dmcu and
!     *                              remove: acmt,dcmt,scmt,pcps,clds,
!     *                              lhrd,lhrs,shrd,shrs.
!     *                            - add fields for ctem under control
!     *                              of new cpp directive: "COUPLER_CTEM".
!     *                            - add new fields for coupler: ofsg,
!     *                              phis,pmsl,xsrf.
!     *                            - previous update direcive "HISTEMI"
!     *                              converted to cpp:
!     *                              "TRANSIENT_AEROSOL_EMISSIONS" with
!     *                              further choice of cpp directives
!     *                              "HISTEMI" for historical or
!     *                              "EMISTS" for future emissions.
!     *                              the "HISTEMI" fields are the
!     *                              same as previously. for "EMISTS",
!     *                              the following are added (both pak/row
!     *                              and pal/rol since interpolated):
!     *                              sair,ssfc,sstk,sfir,oair,osfc,ostk,ofir,
!     *                              bair,bsfc,bstk,bfir. the first letter
!     *                              indicates the species ("B" for black
!     *                              carbon, "O" for organic carbon and
!     *                              "S" for sulfur), while for each of
!     *                              these, there are "AIR" for aircraft,
!     *                              "SFC" for surface, "STK" for stack
!     *                              and "FIR" for fire, each having
!     *                              different emission heights.
!     *                            - for the chemistry, the following
!     *                              fields have been added:
!     *                              ddd,ddb,ddo,dds,wdld,wdlb,wdlo,wdls,
!     *                              wddd,wddb,wddo,wdds,wdsd,wdsb,wdso,
!     *                              wdss,esd,esfs,eais,ests,efis,esfb,
!     *                              eaib,estb,efib,esfo,eaio,esto,efio
!     *                              and the following have been removed:
!     *                              asfs,ashp,aso3,awds,dafx,dcfx,dda,ddc,
!     *                              esbt,esff,eoff,ebff,eobb,ebbb,eswf,
!     *                              sfd,sfs,wdda,wddc,wdla,wdlc,wdsa,wdsc,
!     *                              cdph,clph,csph.
!     *                            - fairpak/fairrow and fairpal/fairrol
!     *                              added for aircraft emissions
!     *                            - o3cpak/o3crow and o3cpal/o3crol
!     *                              added for chemical ozone interpolation.
!     *                            - update directives turned into
!     *                              cpp directives.
!     * feb 20/2009 - m.lazare.    previous version compak9h for gcm15h:
!     *                            - add new fields for emissions: eoff,
!     *                              ebff,eobb,ebbb.
!     *                            - add new fields for diagnostics of
!     *                              conservation: qtpt,xtpt.
!     *                            - add new field for chemistry: sfrc.
!     *                            - add new diagnostic cloud fields
!     *                              (using optical depth cutoff):
!     *                              cldo (both pak and pal).
!     *                            - reorganize emission forcing fields
!     *                              dependant whether are under
!     *                              historical emissions (%df histemi)
!     *                              or not.
!     *                            - add fields for explosive volcanoes:
!     *                              vtau and trop under control of
!     *                              "%DF EXPLVOL". each have both pak
!     *                              and pal.
!     * apr 21/2008 - l.solheim/   previous version compak9g for gcm15g:
!     *               m.lazare/    -  add new radiative forcing arrays
!     *               k.vonsalzen/    (under control of "%DF RADFORCE").
!     *               x.ma.        - new diagnostic fields: wdd4,wds4,edsl,
!     *                              esbf,esff,esvc,esve,eswf along with
!     *                              tdem->edso (under control of
!     *                              "XTRACHEM"), as well as almx,almc
!     *                              and instantaneous clwt,cidt.
!     *                            - new aerocom forcing fields:
!     *                              ebwa,eowa,eswa,eost (each with accumulated
!     *                              and target),escv,ehcv,esev,ehev,
!     *                              ebbt,eobt,ebft,eoft,esdt,esit,esst,
!     *                              esot,espt,esrt.
!     *                            - remove unused: qtpn,utpm,vtpm,tsem,ebc,
!     *                              eoc,eocf,eso2,evol,hvol.
!     * jan 11/2006 - j.cole/      previous version compak9f for gcm15f:
!     *               m.lazare.    - add isccp simulator fields from jason.
!     * nov 26/2006 - m.lazare.    - two extra new fields ("DMC" and "SMC")
!     *                              under control of "%IF DEF,XTRACONV".
!=========================================================================

module compak

  use psizes_19
  use compar,          only : nrmfld, kextt, isdust, isdiag, isdnum
  use chem_params_mod, only : levssad, nlvarc, nchdg, nlso4, nsfcem, nvdep, &
                              nwflx, nchl

  implicit none

#  define PAKARRDCL
#  include "array_operations.inc"
#  include "phys_arrays.inc"

    real,   allocatable, dimension(:) :: dlatpak   ! latitude in degrees
    real,   allocatable, dimension(:) :: dlonpak
    real,   allocatable, dimension(:) :: eco2pak
    real,   allocatable, dimension(:) :: eco2pal
    real,   allocatable, dimension(:) :: fcoopak
    real,   allocatable, dimension(:) :: ilslpak
    real,   allocatable, dimension(:) :: jlpak
    real,   allocatable, dimension(:) :: latupak
    real,   allocatable, dimension(:) :: latdpak
    real,   allocatable, dimension(:) :: lonupak
    real,   allocatable, dimension(:) :: londpak
    real,   allocatable, dimension(:) :: randompak
    real,   allocatable, dimension(:) :: rflndpak
    real*8, allocatable, dimension(:) :: radjpak
    real*8, allocatable, dimension(:) :: wjpak
!
!     * river routing fields passed through restart.
!     * note !! ! THESE MAY ULTIMATELY CONVERTED TO "PAK" FIELDS
!     *         at some point.
!
    real,   allocatable, dimension(:,:) :: ygwogrd
    real,   allocatable, dimension(:,:) :: ygwsgrd
    real,   allocatable, dimension(:,:) :: yswigrd
    real,   allocatable, dimension(:,:) :: yswogrd
    real,   allocatable, dimension(:,:) :: yswsgrd
    real,   allocatable, dimension(:,:) :: ydepgrd
!                      , dimension
!     * river routing fields saved to history file.
!     * note !! ! THESE MAY ULTIMATELY CONVERTED TO "PAK" FIELDS
!     *         at some point.
!
    real,   allocatable, dimension(:,:) :: rvelgrd
    real,   allocatable, dimension(:,:) :: foulgrd
!
!     * river routing fields saved to history file **and**
!     * written to coupler.
!     * note !! ! THESE MAY ULTIMATELY CONVERTED TO "PAK" FIELDS
!     *         at some point.
!
    real,   allocatable, dimension(:,:) :: rivogrd

#  undef PAKARRDCL
#  define PHYBUSINDDCL
#  include "array_operations.inc"
#  include "phys_arrays.inc"

save

 contains

  subroutine calc_pak_indices(pak_array_len)
    use agcm_config_mod, only : agcm_options
    use agcm_types_mod,  only : options => phys_options
    use agcm_types_mod,  only : diag_options => phys_diag

    implicit none
    integer, intent(out) :: pak_array_len

    pak_array_len = 0

#undef PAKARRDCL
#undef PHYBUSINDDCL
#define PAKINDLEN_

#  include "array_operations.inc"
#  include "phys_arrays.inc"

    return
  end subroutine calc_pak_indices

  subroutine pak_arrays_constructor(pak_array_len, pak_array)
    use agcm_config_mod, only : agcm_options
    use agcm_types_mod,  only : options => phys_options
    use agcm_types_mod,  only : diag_options => phys_diag

    implicit none

    integer, intent(in) :: pak_array_len
    real, dimension(pak_array_len), target, intent(in) :: pak_array

#undef PAKARRDCL
#undef PHYBUSINDDCL
#undef PAKINDLEN_
#define PAKPOINT_

#  include "array_operations.inc"
#  include "phys_arrays.inc"

    allocate(dlatpak  (ip0j))
    allocate(dlonpak  (ip0j))
    allocate(eco2pak  (ip0j))
    allocate(eco2pal  (ip0j))
    allocate(fcoopak  (ip0j))
    allocate(ilslpak  (ip0j))
    allocate(jlpak    (ip0j))
    allocate(latupak  (ip0j))
    allocate(latdpak  (ip0j))
    allocate(lonupak  (ip0j))
    allocate(londpak  (ip0j))
    allocate(randompak(ip0j))
    allocate(rflndpak (ip0j))
    allocate(radjpak  (ip0j))
    allocate(wjpak    (ip0j))

    if (agcm_options%agcm_river_routing) then
       allocate(ygwogrd(lonsl + 1,nlat))
       allocate(ygwsgrd(lonsl + 1,nlat))
       allocate(yswigrd(lonsl + 1,nlat))
       allocate(yswogrd(lonsl + 1,nlat))
       allocate(yswsgrd(lonsl + 1,nlat))
       allocate(ydepgrd(lonsl + 1,nlat))
       allocate(rvelgrd(lonsl + 1,nlat))
       allocate(foulgrd(lonsl + 1,nlat))
    end if
    allocate(rivogrd(lonsl + 1,nlat))

    return
  end subroutine pak_arrays_constructor

  subroutine deallocate_pak_arrays()
     use agcm_config_mod, only : agcm_options

    implicit none

    deallocate(dlatpak  )
    deallocate(dlonpak  )
    deallocate(eco2pak  )
    deallocate(eco2pal  )
    deallocate(fcoopak  )
    deallocate(ilslpak  )
    deallocate(jlpak    )
    deallocate(latupak  )
    deallocate(latdpak  )
    deallocate(lonupak  )
    deallocate(londpak  )
    deallocate(randompak)
    deallocate(rflndpak )
    deallocate(radjpak  )
    deallocate(wjpak    )

    if (agcm_options%agcm_river_routing) then
       deallocate(ygwogrd)
       deallocate(ygwsgrd)
       deallocate(yswigrd)
       deallocate(yswogrd)
       deallocate(yswsgrd)
       deallocate(ydepgrd)
       deallocate(rvelgrd)
       deallocate(foulgrd)
    end if
    deallocate(rivogrd)

    return
  end subroutine deallocate_pak_arrays
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

end module compak
