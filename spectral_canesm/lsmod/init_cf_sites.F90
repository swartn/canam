!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine init_cf_sites(mynode,cf_sites_flag)
  !***********************************************************************
  ! read list of sites needed for cmip6 data request.
  !
  ! input
  !   integer*4 :: mynode  ...mpi node (used to restrict i/o to node 0)
  !
  ! output

  ! jason cole 22 march 2019 - initial version
  !***********************************************************************
  use cf_sites_defs, only: num_sites, &
                          cf_site_lon, &
                          cf_site_lat, &
                          cf_site_ind

  implicit none

  !--- input
  integer*4, intent(in) :: mynode !< Variable description\f$[units]\f$
  logical, intent(in) :: cf_sites_flag !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !--- local
  integer :: iu !<
  integer :: newunit !<
  integer :: isite !<
  integer :: idummy !<
  logical :: ok !<

  !-----------------------------------------------------------------------
  !--- if the sites are not being used for this run, just allocate the
  !--- arrays with size 1
  if (.not. cf_sites_flag) then
    num_sites=1
    allocate(cf_site_lon(num_sites))
    allocate(cf_site_lat(num_sites))
    allocate(cf_site_ind(num_sites))
  else
    !--- read list of latitude and longitudes for sites from a file
    inquire(file='CF_SITES',exist=ok)
    if (ok) then
      iu=newunit(0)
      open(iu,file='CF_SITES',form='formatted')
      read(iu,*) num_sites
      !--- allocate the number of sites that will be used.
      allocate(cf_site_lon(num_sites))
      allocate(cf_site_lat(num_sites))
      allocate(cf_site_ind(num_sites))
      !--- read the latitude/longitude information
      do isite = 1, num_sites
        read(iu,*) idummy, cf_site_lon(isite),cf_site_lat(isite)
      end do ! isite
      close(iu)

      if (mynode==0) then
        write(6,*)' '
        write(6,*) 'Number of sites ',num_sites
        write(6,*)' '
        call flush(6)
      end if
    else
      write(6,*)'Namelist file CF_SITES is missing.'
      call xit("INIT_CF_SITES",-1)
    end if ! ok
  end if ! cf_sites_flag

end subroutine init_cf_sites
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
