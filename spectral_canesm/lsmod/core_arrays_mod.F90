!> \file core_arrays_mod.F90
!> \brief Defines pointer arrays and their indices for core15d/core18p fields.
!!
!! @author Routine author name(s)
!
 module core_arrays_mod

  implicit none
  !
  !=====================================================================
  !
  !     * grid slice work fields for physics (core18p).
  !
  !     * FOR I/O PURPOSES, (U,V,T,ES,(TRAC))'TG MUST BE CONSECUTIVE.
  !     * the following fields are mapped into the "FOUR1" work array
  integer :: ttg_ind    = 0
  integer :: estg_ind   = 0
  integer :: tractg_ind = 0
  integer :: vtg_ind    = 0
  integer :: utg_ind    = 0
  integer :: pstg_ind   = 0
  integer :: pressg_ind = 0
  integer :: pg_ind     = 0
  integer :: cg_ind     = 0
  integer :: tg_ind     = 0
  integer :: esg_ind    = 0
  integer :: tracg_ind  = 0
  integer :: psdpg_ind  = 0
  integer :: ug_ind     = 0
  integer :: vg_ind     = 0
  integer :: psdlg_ind  = 0
  !
  !     * grid slice work fields for dynamic (core15d).
  !
  integer :: tvtg_ind    = 0
  integer :: svtg_ind    = 0
  integer :: xvtg_ind    = 0
  integer :: utmp_ind    = 0
  integer :: vtmp_ind    = 0
  integer :: tutg_ind    = 0
  integer :: sutg_ind    = 0
  integer :: xutg_ind    = 0
  integer :: eg_ind      = 0
  integer :: ttgd_ind    = 0
  integer :: estgd_ind   = 0
  integer :: tractgd_ind = 0
  integer :: vtgd_ind    = 0
  integer :: utgd_ind    = 0
  integer :: pstgd_ind   = 0
  integer :: pressgd_ind = 0
  integer :: pgd_ind     = 0
  integer :: cgd_ind     = 0
  integer :: tgd_ind     = 0
  integer :: esgd_ind    = 0
  integer :: tracgd_ind  = 0
  integer :: psdpgd_ind  = 0
  integer :: ugd_ind     = 0
  integer :: vgd_ind     = 0
  integer :: psdlgd_ind  = 0
  !
  !=====================================================================
  !
  !     * Pointer indices for the conservation arrays.
  !
  integer*4 :: tficqj_ind   = 0
  integer*4 :: totenerj_ind = 0
  integer*4 :: tracolj_ind  = 0
  integer*4 :: tficxj_ind   = 0
  integer*4 :: tphsxj_ind   = 0
  integer*4 :: qmomj_ind    = 0
  integer*4 :: scalj_ind    = 0
  integer*4 :: tmomj_ind    = 0
  integer*4 :: tscalj_ind   = 0
  integer*4 :: bwgk_ind     = 0
  integer*4 :: qfso_ind     = 0
  integer*4 :: xsfx_ind     = 0
  integer*4 :: xtvi_ind     = 0
  integer*4 :: xfer_ind     = 0
  !
  integer :: phis_ind  = 0
  integer :: tt_ind    = 0
  integer :: est_ind   = 0
  integer :: tract_ind = 0
  integer :: pt_ind    = 0
  integer :: ct_ind    = 0
  integer :: pst_ind   = 0
  integer :: press_ind = 0
  integer :: ps_ind    = 0
  integer :: p_ind     = 0
  integer :: c_ind     = 0
  integer :: t_ind     = 0
  integer :: es_ind    = 0
  integer :: trac_ind  = 0
  integer :: spca_ind  = 0

  contains

  ! * Initialize grid fields array indices
  subroutine core_arrays_indices_init()
   use msizes, only : ilg_tp, ilg_td, ilev, levs, ntraca, ntrac, ntask_p, &
                      rl, la, rs
   implicit none

   integer :: nn
   integer, dimension(17) :: countp
   integer, dimension(26) :: countd
   integer, dimension(14) :: countx
   integer, dimension(15) :: counts

   ! Setup core18p array indices
   nn = 1 ; countp(nn) = 1
   ttg_ind    = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp * ilev ; nn = nn + 1
   estg_ind   = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp * levs ; nn = nn + 1
   tractg_ind = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp * ilev * ntraca ; nn = nn + 1
   vtg_ind    = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp * ilev ; nn = nn + 1
   utg_ind    = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp * ilev ; nn = nn + 1
   pstg_ind   = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp ; nn = nn + 1
   pressg_ind = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp ; nn = nn + 1
   pg_ind     = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp * ilev ; nn = nn + 1
   cg_ind     = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp * ilev ; nn = nn + 1
   tg_ind     = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp * ilev ; nn = nn + 1
   esg_ind    = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp * levs ; nn = nn + 1
   tracg_ind  = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp * ilev * ntraca ; nn = nn + 1
   psdpg_ind  = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp ; nn = nn + 1
   ug_ind     = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp * ilev ; nn = nn + 1
   vg_ind     = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp * ilev ; nn = nn + 1
   psdlg_ind  = countp(nn) ; countp(nn + 1) = countp(nn) + ilg_tp ; nn = nn + 1


   ! Setup core15d array indices
   nn = 1 ; countd(nn) = 1
   tvtg_ind    = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev ; nn = nn + 1
   svtg_ind    = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * levs ; nn = nn + 1
   xvtg_ind    = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev * ntraca ; nn = nn + 1
   utmp_ind    = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev ; nn = nn + 1
   vtmp_ind    = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev ; nn = nn + 1
   tutg_ind    = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev ; nn = nn + 1
   sutg_ind    = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * levs ; nn = nn + 1
   xutg_ind    = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev * ntraca ; nn = nn + 1
   eg_ind      = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev ; nn = nn + 1
   ttgd_ind    = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev ; nn = nn + 1
   estgd_ind   = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * levs ; nn = nn + 1
   tractgd_ind = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev * ntraca ; nn = nn + 1
   vtgd_ind    = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev ; nn = nn + 1
   utgd_ind    = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev ; nn = nn + 1
   pstgd_ind   = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td ; nn = nn + 1
   pressgd_ind = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td ; nn = nn + 1
   pgd_ind     = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev ; nn = nn + 1
   cgd_ind     = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev ; nn = nn + 1
   tgd_ind     = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev ; nn = nn + 1
   esgd_ind    = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * levs ; nn = nn + 1
   tracgd_ind  = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev * ntraca ; nn = nn + 1
   psdpgd_ind  = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td ; nn = nn + 1
   ugd_ind     = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev ; nn = nn + 1
   vgd_ind     = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td * ilev ; nn = nn + 1
   psdlgd_ind  = countd(nn) ; countd(nn + 1) = countd(nn) + ilg_td ; nn = nn + 1

   ! Setup xfer arrays' indices
   nn = 1 ; countx(nn) = 1
   tficqj_ind   = countx(nn) ; countx(nn + 1) = countx(nn) + ilev * ntask_p ; nn = nn + 1
   totenerj_ind = countx(nn) ; countx(nn + 1) = countx(nn) + ilev * 7 * ntask_p ; nn = nn + 1
   tracolj_ind  = countx(nn) ; countx(nn + 1) = countx(nn) + ilev * ntrac * ntask_p ; nn = nn + 1
   tficxj_ind   = countx(nn) ; countx(nn + 1) = countx(nn) + ilev * ntrac * ntask_p ; nn = nn + 1
   tphsxj_ind   = countx(nn) ; countx(nn + 1) = countx(nn) + ilev * ntrac * ntask_p ; nn = nn + 1
   qmomj_ind    = countx(nn) ; countx(nn + 1) = countx(nn) + ilev * ntask_p ; nn = nn + 1
   scalj_ind    = countx(nn) ; countx(nn + 1) = countx(nn) + ilev * ntask_p ; nn = nn + 1
   tmomj_ind    = countx(nn) ; countx(nn + 1) = countx(nn) + ilev * ntrac * ntask_p ; nn = nn + 1
   tscalj_ind   = countx(nn) ; countx(nn + 1) = countx(nn) + ilev * ntrac * ntask_p ; nn = nn + 1
   bwgk_ind     = countx(nn) ; countx(nn + 1) = countx(nn) + ntask_p ; nn = nn + 1
   qfso_ind     = countx(nn) ; countx(nn + 1) = countx(nn) + ntask_p ; nn = nn + 1
   xsfx_ind     = countx(nn) ; countx(nn + 1) = countx(nn) + ntrac * ntask_p ; nn = nn + 1
   xtvi_ind     = countx(nn) ; countx(nn + 1) = countx(nn) + ntrac * ntask_p ; nn = nn + 1

   xfer_ind = countx(14)

   ! Setup indices for the spectral state arrays
   nn = 1 ; counts(nn) = 1
   phis_ind  =  counts(nn) ; counts(nn + 1) = counts(nn) + la ; nn = nn + 1
   tt_ind    =  counts(nn) ; counts(nn + 1) = counts(nn) + rl ; nn = nn + 1
   est_ind   =  counts(nn) ; counts(nn + 1) = counts(nn) + rs ; nn = nn + 1
   tract_ind =  counts(nn) ; counts(nn + 1) = counts(nn) + rl * ntraca ; nn = nn + 1
   pt_ind    =  counts(nn) ; counts(nn + 1) = counts(nn) + rl ; nn = nn + 1
   ct_ind    =  counts(nn) ; counts(nn + 1) = counts(nn) + rl ; nn = nn + 1
   pst_ind   =  counts(nn) ; counts(nn + 1) = counts(nn) + la ; nn = nn + 1
   press_ind =  counts(nn) ; counts(nn + 1) = counts(nn) + la ; nn = nn + 1
   ps_ind    =  counts(nn) ; counts(nn + 1) = counts(nn) + la ; nn = nn + 1
   p_ind     =  counts(nn) ; counts(nn + 1) = counts(nn) + rl ; nn = nn + 1
   c_ind     =  counts(nn) ; counts(nn + 1) = counts(nn) + rl ; nn = nn + 1
   t_ind     =  counts(nn) ; counts(nn + 1) = counts(nn) + rl ; nn = nn + 1
   es_ind    =  counts(nn) ; counts(nn + 1) = counts(nn) + rs ; nn = nn + 1
   trac_ind  =  counts(nn) ; counts(nn + 1) = counts(nn) + rl * ntraca ; nn = nn + 1

   spca_ind  = counts(15)

   return
  end subroutine core_arrays_indices_init

 end module core_arrays_mod
