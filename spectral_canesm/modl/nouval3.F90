!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine nouval3(zp, dp, tp, qp, lpsp, xp, itrac, ntrac, &
                         ir1, la, lm, lsr, nk, nm, nmax, &
                         rz, rd, rt, rq, rps, rx, &
                         osiila, del2la, &
                         am, bm, cm, &
                         phis,tref,r,dt)

  !     * feb 02/94 - m.lazare. use triangular-filled "DEL2LA"/"OSIILA"
  !     *                       instead of "DEL2"/"OSII" to optimize loops
  !     *                       1001-1003 and 2002..
  !     * aug 14/90 - j-d.grandpre. - previous version nouval2.
  !
  !     * modele spectral,semi-implicite,elements finis constants nouval
  !     * solution du scheme semi-implicite
  !     * on combine les cotes droits rd, rt et rps et
  !     * on multiplie le resultat par osii (calcule par matcal)
  !     * POUR OBTENIR D+, ON SUBSTITUE ENSUITE D+ DANS L'EQN
  !     * THERMODYNAMIQUE POUR OBTENIR T+ ET DANS L'EQN DE
  !     * continuite pour obtenir lps+.
  !
  !     on calcule les quantites
  !
  ! s          zp(mn,k)        : z+, tourbillon a t+dt.
  ! s          dp(mn,k)        : d+, divergence a t+dt.
  ! s          tp(mn,k)        : t+, temperature a t+dt.
  ! s          qp(mn,km)       : q+, variable humidite a t+dt.
  ! s          xp(mn,k)        : x+, f(traceur) a t+dt.
  ! s          lpsp(mn)        : lps+, log de la pression de surface a t
  !
  !    pour chaque composante spectrale mn et chaque couche k
  !
  ! e          ir1             : ir1=lrs(1), longueur max. de la colonne m
  ! e          khem            :  0 (global), 1 (hemis. n.), 2 (hemis. s.)
  ! e          la              : nombre total de composantes spectrales
  ! E          LM              : (NOMBRE D'ONDE EST-OUEST MAXIMUM+1)
  ! E          LRA(M)          : NOMBRE DE COMP. SPECT. ANTIS. D'ORDRE M.
  ! E          LRS(M)          : NOMBRE DE COMP. SPECT. SYM. D'ORDRE M.
  ! e          nk              : nombre de couches.
  ! E          NM              : NOMBRE DE COUCHES D'HUMIDITIE.
  ! e          nmax            : (valeur  maximum de n)+1
  !
  !     A PARTIR DES DONNEES D'ENTREE
  !
  ! e          rz(mn,k)        : cote droit, equation du tourbillon.
  ! e          rd(mn,k)        : cote droit, equation de la divergence.
  ! e          rt(mn,k)        : cote droit, equation thermodynamique.
  ! E          RQ(MN,KM)       : COTE DROIT, EQUATION D'HUMIDITE.
  ! e          rx(mn,k)        : cote droit, equation du traceur.
  ! e          rps(mn)         : cote droit, equation de continuite.
  ! e          phis(mn)        : geopotentiel de surface
  !
  !     on se sert des champs de travail suivants
  !     SSDPK ET SSRTK PEUVENT ETRE EQUIVALENCES DANS L'APPEL.
  !
  ! e          ssdpk(mn)       : somme des couches 1 a k de d+*dsigma.
  ! e          ssrtk(mn)       : somme de k+1 a nk de r*dlnsigma*rt.
  !
  !     on se sert aussi des parametres suivants
  !
  ! E          OSII(N,K,K)     : MATRICE INVERSE DANS L'EQUATION DE DIVERG
  ! e          fdif (n)         : constante de diffusion horiz.
  ! e          del2(n+1)         : -n(n+1)/a**2.
  ! e          alfa1(k)        : ( 1.-sigmam*dlnsigma/dsigma).
  ! e          alfa2(k)        : ( 1.-sigmam*dlnsigma/dsigma).
  ! e          dsg(k)          : dsigma, epaisseur de la couche k.
  ! e          tref            : tref,une constante.
  ! e          r               : constante des gaz parfaits.
  ! e          dt              : longeur du pas de temps en seconde.
  !
  implicit none
  real, intent(in) :: dt
  integer, intent(in) :: ir1
  integer, intent(in) :: itrac  !< Switch to indicate use of tracers in CanAM (0 = no, 1 = yes) \f$[unitless]\f$
  integer, intent(in) :: la
  integer, intent(in) :: lm
  integer, intent(in) :: nk
  integer, intent(in) :: nm
  integer, intent(in) :: nmax
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  real, intent(in) :: r
  real, intent(in) :: tref

  complex, intent(inout) :: zp (la,nk) !< Variable description\f$[units]\f$
  complex, intent(inout) :: dp (la,nk) !< Variable description\f$[units]\f$
  complex, intent(inout) :: lpsp(la) !< Variable description\f$[units]\f$
  complex, intent(inout) :: tp (la,nk) !< Variable description\f$[units]\f$
  complex, intent(inout) :: qp (la,nm) !< Variable description\f$[units]\f$
  complex, intent(in) :: rt (la,nk) !< Variable description\f$[units]\f$
  complex, intent(in) :: rq (la,nm) !< Variable description\f$[units]\f$
  complex, intent(in) :: rps (la) !< Variable description\f$[units]\f$
  complex, intent(inout) :: rd (la,nk) !< Variable description\f$[units]\f$
  complex, intent(in) :: rz (la,nk) !< Variable description\f$[units]\f$
  complex, intent(inout) :: xp(la,nk,ntrac) !< Variable description\f$[units]\f$
  complex, intent(in) :: rx(la,nk,ntrac) !< Variable description\f$[units]\f$
  complex, intent(in) :: phis(la) !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(la,nk,nk) :: osiila !< Variable description\f$[units]\f$
  real, intent(in), dimension(la) :: del2la !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk,nk) :: am !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk,nk) :: bm !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk,nk) :: cm !< Variable description\f$[units]\f$
  !
  integer, intent(in), dimension(2,lm) :: lsr !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  integer :: k
  integer :: l
  integer :: mn
  integer :: n
  !
  !            champs de travail.
  !
  complex  :: ssdpk(la) !< Variable description\f$[units]\f$
  complex  :: ssrtk(la) !< Variable description\f$[units]\f$
  !
  !
  !-----------------------------------------------------------------------
  !            etape 1 :     solution du scheme semi-implicite
  !                      c)  on finalise le calcul de rd en y ajoutant
  !                          les contributions de rt et rps.
  !     ------------------------------------------------------------------
  !
  k=nk
  do mn=1,la
    ssrtk(mn)  = r*tref*rps(mn) + phis(mn)/dt
    rd(mn,k)   = rd(mn,k) &
                       -dt*del2la(mn)*(ssrtk(mn) + am(k,k)*rt(mn,k) &
                       + am(k-1,k)*rt(mn,k-1) )
  end do ! loop 1001
  !
  do k=nk-1,2,-1
    do mn=1,la
      ssrtk(mn)  = ssrtk(mn) + am(k+1,k)*rt(mn,k+1)
      rd(mn,k)   = rd(mn,k) &
                       -dt*del2la(mn)*(ssrtk(mn) + am(k,k)*rt(mn,k) &
                       + am(k-1,k)*rt(mn,k-1) )
    end do
  end do ! loop 1002
  !
  k=1
  do mn=1,la
    ssrtk(mn)  = ssrtk(mn) + am(k+1,k)*rt(mn,k+1)
    rd(mn,k)   = rd(mn,k) &
                       -dt*del2la(mn)*(ssrtk(mn) + am(k,k)*rt(mn,k) )
  end do ! loop 1003
  !
  !     ------------------------------------------------------------------
  !            etape 2 :     solution du scheme semi-implicite
  !                      d)  on obtient les valeurs de d+
  !     ------------------------------------------------------------------
  !
  do k=1,nk
    do mn=1,la
      dp(mn,k)=(0.,0.)
    end do
  end do ! loop 2001
  !
  do k=1,nk
    do l=1,nk
      do mn=1,la
        dp(mn,k) = dp(mn,k) + osiila(mn,l,k)*rd(mn,l)
      end do
    end do
  end do ! loop 2002
  !
  do k=1,nk
    do mn=1,la
      dp(mn,k)=dp(mn,k)*2.*dt
    end do
  end do ! loop 2003
  !
  !     ------------------------------------------------------------------
  !            etape 3 :     solution du scheme semi-implicite
  !                      e)  on subtitue les valeurs de rt et rps
  !                          pour obtenir t+ et lps+.
  !     ------------------------------------------------------------------
  !
  k=1
  do mn=1,la
    ssdpk(mn) = (0.,0.)
    tp(mn,k) = ( 2.*rt(mn,k) &
                          -bm(k,k)*dp(mn,k)-bm(k+1,k)*dp(mn,k+1) )*dt
  end do ! loop 3001
  !
  do k=2,nk-1
    do mn=1,la
      ssdpk(mn)  = ssdpk(mn) + dp(mn,k-1)*cm(k-1,k)
      tp(mn,k) = ( 2.*rt(mn,k) &
                          - (bm(k-1,k)/cm(k-1,k))*ssdpk(mn) &
                          -bm(k,k)*dp(mn,k)-bm(k+1,k)*dp(mn,k+1) )*dt
    end do
  end do ! loop 3002
  !
  k=nk
  do mn=1,la
    ssdpk(mn)  = ssdpk(mn) + dp(mn,k-1)*cm(k-1,k)
    tp(mn,k) = ( 2.*rt(mn,k) &
                          - (bm(k-1,k)/cm(k-1,k))*ssdpk(mn) &
                          -bm(k,k)*dp(mn,k) )*dt
    ssdpk(mn) = ssdpk(mn) +dp(mn,k)*cm(k,k)
    lpsp(mn) = ( 2.*rps(mn) - ssdpk(mn) )*dt
  end do ! loop 3003
  !
  !     ------------------------------------------------------------------
  !            etape 4 :     on multiplie rz et rq par 2dt
  !                          ainsi que rx.
  !     ------------------------------------------------------------------
  !
  do k=1,nk
    do mn=1,la
      zp(mn,k) = rz(mn,k)*2.*dt
    end do
  end do ! loop 4001
  !
  do k=1,nm
    do mn=1,la
      qp(mn,k) = rq(mn,k)*2.*dt
    end do
  end do ! loop 4002
  !
  if (itrac/=0) then
    do n=1,ntrac
      do k=1,nk
        do mn=1,la
          xp(mn,k,n)=rx(mn,k,n)*2.*dt
        end do
      end do
    end do ! loop 4003
  end if

  return
  !-----------------------------------------------------------------------
end subroutine nouval3
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
