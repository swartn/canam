!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine ingcm12(sg, sh, idiv, kfinal, avgps, levoz, nilev, nlevs, ntrac)

  !     * jun 28/2018 - s.kharin.  keep kstart from ibuf(2) instead of f(1)
  !     *                          replace int(*) with nint(*).
  !     * may 24/2017 - m.lazare.  for conversion to xc40:
  !     *                          - replace "DTIME" by "CPU_TIME".
  !     *                          - xit if levoz/=int(f(12)) and adjust
  !     *                            following xit numbers accordingly.
  !     * sep 28/2006 - f.majaess. new version for gcm15f, based on ingcm11:
  !     *                          - replace direct binary "DATA" record
  !     *                            reads by "GETFLD2" calls.
  !     * apr 05/2006 - f.majaess. previous version ingcm11 for gcm15c/d/e:
  !     *                          - modified for exit processing on rigel.
  !     * feb 15/2005 - m.lazare.new version for gcm15c:
  !     *                        include reading of high-frequency save
  !     *                        switch.
  !     * dec 15/2003 - m.lazare.new version for gcm15b:
  !     *                        include qsrc0,qsrcm,xsrc0,xsrcm in
  !     *                        variables read in from data restart file.
  !     * dec 10/2003 - m.lazare.previous version ingcm9x for gcm13b.
  !
  !     * reads controls for hybrid gcm from cards and the start file nusp.
  !     * prints model controls and parameters.
  !     * for use with model versions gcm13 or later which support
  !     * runs with surface tracer flux input.
  use iso_fortran_env, only : STDERR => error_unit, STDOUT => output_unit
  use comopen,   only : modl_dat_iu, nusp
  use times_mod, only : delt, ifdiff, kstart, ktotal, newrun, &
                        ndays, nsecs, iday, lday, mday, mdayt, incd
  use intervals, only : isbeg
  use tracers_info_mod, only: xsrcrat, xsrcrm1, xpp, xp0, xpm, tficxm, &
                              xsrc0,  xsrcm, xscl0, xsclm, wp0, wpm, wpp, &
                              qscl0, qsclm, qsrc0, qsrcm, qsrcrat, qsrcrm1, tficqm

  implicit none
  real, intent(inout) :: avgps
  integer, intent(inout) :: kfinal
  integer, intent(inout) :: levoz  !< Number of vertical layers for ozone input data (other than chemistry) \f$[unitless]\f$
  integer, intent(inout) :: nilev
  integer, intent(inout) :: nlevs
  integer, intent(inout) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  logical, intent(in)    :: idiv

  real, intent(inout), dimension(nilev) :: sg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nilev) :: sh !< Variable description\f$[units]\f$

  real :: dt
  real :: gmt
  integer :: agcm_rstctl
  integer :: i
  integer :: ibuf
  integer :: icoord
  integer :: idat
  integer :: idd
  integer :: ihh
  integer :: ilat
  integer :: ilev
  integer :: ilg
  integer :: imdh
  integer :: isavdts
  integer :: iyear
  integer :: jlatpr
  integer :: k
  integer :: kstart_from_restart
  integer :: ksteps
  integer :: l
  integer :: lay
  integer :: levs
  integer :: levs1
  integer :: lrlmt
  integer :: mm
  integer :: mx
  integer :: myrssti
  integer :: n
  integer :: nc4to8
  real :: ptoit
  real :: xxxx

  integer, parameter :: imx = 2000

  logical :: ok !<
  real*4 , dimension(4) :: tarray !<
  real, dimension(imx) :: f !<
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  integer, dimension(13) :: nfdm !<
  integer*4 :: mynode !<

  character(len=4) :: icoordc

  common /sizes/  ilg,ilat,ilev,levs,lrlmt,icoord,lay,ptoit
  common /keeptim/iyear,imdh,myrssti,isavdts
  common /mpinfo/ mynode

  common /icom/ ibuf(8), idat(imx)
  !
  namelist /ingcm/ ksteps, kstart, kfinal, agcm_rstctl, newrun, levs1, incd, &
                   jlatpr, delt, ifdiff, icoordc, iyear

  data mx/imx/
  data nfdm/1,32,60,91,121,152,182,213,244,274,305,335,366/
  !---------------------------------------------------------------------
  f=0.0
  !
  if (mynode==0)      write(6,6005)

  !     * read model parameters from agcm configuration namelist (modl.dat)

  !     * ksteps        = number of timesteps between coupling cycle
  !     * kfinal        = final timestep to end a run
  !     * kstart        = the starting step of the run (taken to be the last step of the restarting job)
  !     * agcm_rstctl   = controlling switch for consistency checks between namelist values and those in the restart
  !                     = 0 simply output warnings if inconsistencies noted
  !                     = 1 throw error if inconsistencies noted
  !     * newrun        = 1 at the start of a new run
  !     *               = 0 for restart inside a run
  !     * levs1         = number of moisture levels in the model
  !     *                   (cannot be changed after the initial start-up at step 0)
  !     * incd          = 0 to keep iday fixed, 1 to increment iday normally.
  !     * delt          = model timestep in seconds.
  !     * ifdiff        = 1 for forward step, 0 for leapfrog step.
  !     * icoordc       = hybrid vertical coordinate (' et15',' eta',' sig).
  !     * iyear         = year which model starts from (i.e. '1979').

  ! Read in the "GCM" namelist from modl.dat to set values needed by CanAM.
  rewind(modl_dat_iu)
  read(modl_dat_iu, nml=INGCM)
  ! Convert the ICOORDC from the namelist to integers
  icoord = nc4to8(icoordc)

  ! make sure that we have an acceptable agcm_rstctl value
  if ( agcm_rstctl < 0 .or. agcm_rstctl > 1 ) then
    call xit('INGCM12-invalid-agcm-rstcl', -1)
  endif

  !     * check timestep counters
  !     *   ksteps -> number of steps between coupling interval
  !     *   kstart -> initial time-step
  !     *   kfinal -> final time-step in run
  !     *   ktotal -> used to flag iterates that correspond to coupling steps
  if (kstart>=kfinal) then
    if (mynode==0) then
      write(STDOUT,6008)
    end if
    call xit('INGCM12',0)
  end if

  ktotal=kstart+ksteps
  if (ktotal>kfinal) ktotal=kfinal
  !--------------------------------------------------------------------
  !     * for an initial startup, nusp is the output file from
  !     * the spectral initialization program with kstart=0.
  !     * moisture levels (levs) is set at initial start-up only.
  !     * the first step must be a forward timestep.
  rewind nusp
  if (kstart<=0) then

    if (2*nilev+5>mx)  call               xit('INGCM12',-2)
    !
    !         * input data record #1.
    !
    call getfld2(nusp,f,-1,0,0,0,ibuf,mx,ok)
    if (.not. ok)        call               xit('INGCM12',-3)

    !         alabl=f(1)
    kstart_from_restart=nint(f(2))
    ilev=nint(f(3))
    do l=1,ilev
      sg(l)=f(3+l)
      sh(l)=f(3+ilev+l)
    end do
    lay=nint(f(3+2*ilev+1))
    ptoit=f(3+2*ilev+2)

    !
    !         * input data record #2.
    !
    call getfld2(nusp,f,-1,0,0,0,ibuf,mx,ok)
    if (.not. ok)         call              xit('INGCM12',-4)
    !
    !         labl=nint(f(1))
    lrlmt=nint(f(2))
    ilg=nint(f(3))
    ilat=nint(f(4))

    !
    !         * input data record #3.
    !
    call getfld2(nusp,f,-1,0,0,0,ibuf,mx,ok)
    if (.not. ok) call                      xit('INGCM12',-5)

    !         alabl=f(1)
    avgps=f(2)
    !
    ifdiff=1
    newrun=1
    levs  =levs1
    if (levs>ilev) levs=ilev
    !--------------------------------------------------------------------
  else
    !         * for a restart, nusp is a file produced by a previous run
    !         * of the model, and kstart is greater than zero.

    if (2*nilev+6>mx)  call               xit('INGCM12',-6)
    !
    !         * input data record #1.
    !
    call getfld2(nusp,f,-1,0,0,0,ibuf,mx,ok)
    if (.not. ok) call                      xit('INGCM12',-7)
    !
    kstart_from_restart = nint(f(1))
    lrlmt  =nint(f(2))
    xxxx   =f(3)
    ilev   =nint(f(4))
    lay    =nint(f(4+2*ilev+1))
    ptoit  =f(4+2*ilev+2)
    !
    do i=1,ilev
      sg(i)   =f(4+i)
      sh(i)   =f(4+i+ilev)
    end do ! loop 100


    if (10*ntrac+22>mx) call              xit('INGCM12',-8)
    !
    !         * input data record #2.
    !
    call getfld2(nusp,f,-1,0,0,0,ibuf,mx,ok)
    if (.not. ok) call                      xit('INGCM12',-9)
    !
    ilg    =nint(f(1))
    ilat   =nint(f(2))
    levs   =nint(f(3))
    dt     =f(4)
    avgps  =f(11)
    if (levoz/=nint(f(12))) call xit('INGCM12',-10)
    qsrcrat=f(13)
    qsrcrm1=f(14)
    qsrc0  =f(15)
    qsrcm  =f(16)
    qscl0  =f(17)
    qsclm  =f(18)
    wpp    =f(19)
    wp0    =f(20)
    wpm    =f(21)
    tficqm =f(22)
    if (ntrac>0) then
      do n=1,ntrac
        xsrcrat(n)=f(22+n)
        xsrcrm1(n)=f(22+ntrac+n)
        xpp    (n)=f(22+2*ntrac+n)
        xp0    (n)=f(22+3*ntrac+n)
        xpm    (n)=f(22+4*ntrac+n)
        tficxm (n)=f(22+5*ntrac+n)
        xsrc0  (n)=f(22+6*ntrac+n)
        xsrcm  (n)=f(22+7*ntrac+n)
        xscl0  (n)=f(22+8*ntrac+n)
        xsclm  (n)=f(22+9*ntrac+n)
      end do ! loop 200
    end if
  end if

  ! compare kstart between restart and namelist
  !   (can be extended to included other checks)
  write(STDOUT,*) 'INGCM : kstart in restart   ', kstart_from_restart
  write(STDOUT,*) '        kstart from namelist', kstart
  if ( kstart_from_restart /= kstart ) then
    if ( agcm_rstctl == 0 ) then
      ! just output a warning
      write(STDERR,*) 'Kstart from input restart is different than that from the ingcm namelist!'
    elseif ( agcm_rstctl == 1 ) then
      ! exit!
      write(STDERR,*) 'Kstart is not consistent between ingcm namelist and input restart! Bailing!'
      call xit('INGCM12 - kstart check', -1)
    endif
  endif

  ! initialize time parameters from kstart and delt
  call init_time_params(kstart, delt, iyear, iday, lday, mday, mdayt, &
                        ndays, nsecs, imdh)

  if (ilev/=nilev) call                      xit('INGCM12',-11)
  if (levs/=nlevs) call                      xit('INGCM12',-12)
  !---------------------------------------------------------------------
  if (mynode==0) then
    !
    !     * print out model parameters.
    !
    write(STDOUT,6010) kstart,ksteps,ktotal,kfinal,ifdiff,newrun
    write(STDOUT,6020) ilg,ilat,ilev,levs,lrlmt
    write(STDOUT,6022) ndays,nsecs,iday,lday,mday,mdayt,incd
    call writlev  (sg,ilev,' SG ')
    call writlev  (sh,ilev,' SH ')
    write(STDOUT,6025) lay,icoord,ptoit
    write(STDOUT,6026) delt
    write(STDOUT,6030) idiv
    write(STDOUT,6050) avgps
    write(STDOUT,6060) iyear,imdh
  end if

  return

  901 call                                        xit('INGCM12',-13)
  902 call                                        xit('INGCM12',-14)
  903 call                                        xit('INGCM12',-15)
  904 call                                        xit('INGCM12',-16)
  905 call                                        xit('INGCM12',-17)
  906 call                                        xit('INGCM12',-18)
  907 call                                        xit('INGCM12',-19)
  908 call                                        xit('INGCM12',-20)
  909 call                                        xit('INGCM12',-21)
  910 call                                        xit('INGCM12',-22)
  !-----------------------------------------------------------------------
  5005 format(10x,i5,i10,3i5)
  5010 format(10x,12i5)
  5015 format(10x,2i5,a4)
  5020 format(f10.0,i5,1x,a4,1x,a4,i5)
  6005 format('1C.C.R.N. GENERAL CIRCULATION MODEL')
  6008 format('0*** GCM RUN HAS COMPLETED SUCCESSFULLY ***')
  6010 format('0KSTART,KSTEPS,KTOTAL,KFINAL,IFDIFF,NEWRUN',8x,4i10,2i5)
  6020 format('0LON,ILAT,ILEV,LEVS,LRLMT',29x,4i5,i10)
  6022 format('0NDAYS,NSECS,IDAY,LDAY,MDAY,MDAYT,INCD', 22x,2i10,5i5/)
  6025 format('0LAY=',i5,', ICOORD=',a4,', P.LID (PA)=',f10.3)
  6026 format('0TIMESTEP (SEC) =',f8.1)
  6030 format('0IDIV = ',l6)
  6050 format('0AVGPS = ',f10.3)
  6060 format('0IYEAR,IMDH = ',i5,5x,i6/)
  6070 format('0RESTART FILE DOES NOT CONTAIN IYEAR/IMDH DATA,', &
        ' SO THESE INITIALIZED TO ZERO')
end subroutine ingcm12
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
