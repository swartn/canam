!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine trigml4a(nlath,ilat,sr,wr,cr,radr,wosq, &
                          srx,wrx,crx,radrx,wosqx,pi)
  !
  !     * jun 12/06 - m. lazare. new version for gcm15f:
  !     *                        - passes in pi and removes
  !     *                          "PARAMS" common block.
  !     * jun 30/03 - m. lazare. previous version trigml3a:
  !     *                        like previous version trigml2, except
  !     *                        re-ordered chaining to ensure optimal
  !     *                        load-balancing for all physics
  !     *                        processes.
  !
  !     * this routine is based on trigl, except that the trignometric
  !     * arrays are re-ordered in chained s-n pairs, based on the symmetry
  !     * properties of the particular trignometric function, for use
  !     * in a multiple-latitude model simulation. latitudes are alternated
  !     * between poles and equator to ensure average optimal load-balancing
  !     * for all physics processes.
  !
  !     * the ordering is thus: (1,ilat,ilath,ilath+1,2,ilat-1,...).

  !     * the routine gaussg fills only the n hem ordered n to s and is
  !     * called immediately prior to this routine. be aware that its
  !     * output for "RADR" defines co-latitude; this routine converts
  !     * that to latitude.
  !
  !     *      sr=sin(lat),  cr=cos(lat),  radr=latitude in radians.
  !     *      wr = gaussian weights,  wosq = wr/(sr**2).
  !
  implicit none
  integer, intent(in) :: ilat
  integer, intent(in) :: nlath
  real, intent(in) :: pi
  !
  real*8, intent(in) , dimension(ilat) :: srx !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(ilat) :: wrx !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(ilat) :: crx !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(ilat) :: radrx !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(ilat) :: wosqx !< Variable description\f$[units]\f$
  real*8, intent(out), dimension(ilat) :: sr !< Variable description\f$[units]\f$
  real*8, intent(out), dimension(ilat) :: wr !< Variable description\f$[units]\f$
  real*8, intent(out), dimension(ilat) :: cr !< Variable description\f$[units]\f$
  real*8, intent(out), dimension(ilat) :: radr !< Variable description\f$[units]\f$
  real*8, intent(out), dimension(ilat) :: wosq !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer :: j
  integer :: j0
  integer :: jq
  integer :: nj
  integer :: nlat
  integer :: nlatq
  real :: pih

  !--------------------------------------------------------------------
  pih=pi/2.
  nlat=nlath*2
  if (mod(nlath,2)/=0)       call xit('TRIGML4A',-1)
  nlatq=nlath/2
  !
  !     * fill ouput arrays based on symmetry arguements.
  !     * cr,wr,wosq are symmetric about the equator.
  !     * sr and radr are antisymmetric.
  !     * note that radr is converted from co-latitude to latitude here !
  !     * note also that only the values relevant to the node are obtained !
  !     * "J" is the counter index for the whole set of gaussian latitudes
  !     * while "NJ" is for the particular node only.
  !
  nj=0
  j=0
  do jq=1,nlatq
    j0      = nlath-jq+1
    !
    j=j+1
    nj      = nj+1
    sr(nj)  =-srx(jq)
    cr(nj)  = crx(jq)
    wr(nj)  = wrx(jq)
    wosq(nj)= wosqx(jq)
    radr(nj)= radrx(jq)-pih
    !
    j=j+1
    nj      = nj+1
    sr(nj)  = srx(jq)
    cr(nj)  = crx(jq)
    wr(nj)  = wrx(jq)
    wosq(nj)= wosqx(jq)
    radr(nj)= pih-radrx(jq)
    !
    j=j+1
    nj      = nj+1
    sr(nj)  =-srx(j0)
    cr(nj)  = crx(j0)
    wr(nj)  = wrx(j0)
    wosq(nj)= wosqx(j0)
    radr(nj)= radrx(j0)-pih
    !
    j=j+1
    nj      = nj+1
    sr(nj)  = srx(j0)
    cr(nj)  = crx(j0)
    wr(nj)  = wrx(j0)
    wosq(nj)= wosqx(j0)
    radr(nj)= pih-radrx(j0)
  end do ! loop 150
  !
  return
end subroutine trigml4a
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
