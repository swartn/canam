!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine qdtfpc3(q,d,p,c,rns2la,la,ilev,kase)
  !
  !     * nov 12/03 - m.lazare. account for fact that rns2la(1) may not
  !     *                       be zero in a multi-node environment, ie
  !     *                       it is only zero on node 0.
  !     * dec 10/97 - b.denis.  previous version qdtfpc2.
  !
  !     * apr 11/79 - j.d.henderson. original version qdtfpc.
  !
  !     * converts between (p,c) and (q,d) based on value of kase.
  !
  !     * q(la,ilev) = spectral vorticity
  !     * d(la,ilev) = spectral divergence
  !     * p(la,ilev) = spectral streamfunction
  !     * c(la,ilev) = spectral velocity potential
  !
  !     * kase = +1 converts p,c to q,d.
  !     * kase = -1 converts q,d to p,c.
  !
  !     * level 2,q,d,p,c

  implicit none
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: kase
  integer, intent(in) :: la

  real, intent(in), dimension(la) :: rns2la !< Variable description\f$[units]\f$
  complex, intent(inout) :: q(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: d(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: p(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: c(la,ilev) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer :: k
  integer :: l
  real :: rfns1

  !-------------------------------------------------------------------
  !     * kase = +1 converts p,c to q,d.
  !
  if (kase>=0) then
    do l=1,ilev
      do k=1,la
        q(k,l)=-1.*rns2la(k)*p(k,l)
        d(k,l)=-1.*rns2la(k)*c(k,l)
      end do
    end do ! loop 290
    return
  end if
  !     * kase = -1 converts q,d to p,c.
  !
310 continue
  do l=1,ilev
    if (rns2la(1)==0.) then
      p(1,l)=0.
      c(1,l)=0.
    else
      rfns1 =1./rns2la(1)
      p(1,l)=-rfns1*q(1,l)
      c(1,l)=-rfns1*d(1,l)
    end if
    !
    do k=2,la
      rfns1 =1./rns2la(k)
      p(k,l)=-rfns1*q(k,l)
      c(k,l)=-rfns1*d(k,l)
    end do ! loop 380
  end do ! loop 390
  !
  return
end subroutine qdtfpc3
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
