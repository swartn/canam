!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine sptemp(in,p,  c,  t,  es,  ps,  trac, &
                        pmb,cmb,tmb,esmb,psmb,tracmb, &
                        itrac,ntrac,ilev,levs,la)
  !
  !     * nov 25/94 - m.lazare. memory storage replacement for internal
  !     *                       spectral i/o done in gcm.
  !
  !     *                       in = +1 => write (store).
  !     *                       in = -1 => read  (fetch).
  !
  implicit none
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: in
  integer, intent(in) :: itrac  !< Switch to indicate use of tracers in CanAM (0 = no, 1 = yes) \f$[unitless]\f$
  integer, intent(in) :: la
  integer, intent(in) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$

  complex, intent(inout) :: p  (la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: c  (la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: t  (la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: es (la,levs) !< Variable description\f$[units]\f$
  complex, intent(inout) :: ps (la) !< Variable description\f$[units]\f$
  complex, intent(inout) :: trac  (la,ilev,ntrac) !< Variable description\f$[units]\f$
  !
  complex, intent(inout) :: pmb(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: cmb(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: tmb(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: esmb(la,levs) !< Variable description\f$[units]\f$
  complex, intent(inout) :: psmb(la) !< Variable description\f$[units]\f$
  complex, intent(inout) :: tracmb(la,ilev,ntrac) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer :: l
  integer :: mn
  integer :: n

  !=====================================================================
  if (in==+1) then
    !
    !       * write into holding arrays.
    !
    do mn=1,la
      psmb(mn)=ps  (mn)
    end do ! loop 100
    !
    do l=1,ilev
      do mn=1,la
        pmb(mn,l)=p  (mn,l)
        cmb(mn,l)=c  (mn,l)
        tmb(mn,l)=t  (mn,l)
      end do
    end do ! loop 120
    !
    do l=1,levs
      do mn=1,la
        esmb(mn,l)=es  (mn,l)
      end do
    end do ! loop 140
    !
    if (itrac/=0) then
      do n=1,ntrac
        do l=1,ilev
          do mn=1,la
            tracmb(mn,l,n)=trac  (mn,l,n)
          end do
        end do
      end do ! loop 160
    end if

  else
    !
    !       * read from holding arrays.
    !
    do mn=1,la
      ps(mn)=psmb(mn)
    end do ! loop 200
    !
    do l=1,ilev
      do mn=1,la
        p  (mn,l)=pmb(mn,l)
        c  (mn,l)=cmb(mn,l)
        t  (mn,l)=tmb(mn,l)
      end do
    end do ! loop 220
    !
    do l=1,levs
      do mn=1,la
        es  (mn,l)=esmb(mn,l)
      end do
    end do ! loop 240
    !
    if (itrac/=0) then
      do n=1,ntrac
        do l=1,ilev
          do mn=1,la
            trac  (mn,l,n)=tracmb(mn,l,n)
          end do
        end do
      end do ! loop 260
    end if

  end if
  !==================================================================
  return
end subroutine sptemp
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
