!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine nouvalp(zp, dp, tp, qp, xp, &
                         la, nk, nm, dt, itrac, ntrac, &
                         rz, rd, rt, rq, rx)
  !
  !     * feb 28/94 - m.lazare.
  !
  !     * modele spectral,semi-implicite,elements finis constants nouval
  !     * solution du scheme explicite pour les termes physics.
  !     * adapte de routine nouval2.
  !
  !     on calcule les quantites
  !
  ! s          zp(mn,k)        : z+, tourbillon a t+dt.
  ! s          dp(mn,k)        : d+, divergence a t+dt.
  ! s          tp(mn,k)        : t+, temperature a t+dt.
  ! s          qp(mn,km)       : q+, variable humidite a t+dt.
  ! s          xp(mn,k)        : x+, f(traceur) a t+dt.
  !
  !    pour chaque composante spectrale mn et chaque couche k
  !
  ! e          la              : nombre total de composantes spectrales
  ! e          nk              : nombre de couches.
  ! E          NM              : NOMBRE DE COUCHES D'HUMIDITIE.
  !
  !     A PARTIR DES DONNEES D'ENTREE
  !
  ! e          rz(mn,k)        : cote droit, equation du tourbillon.
  ! e          rd(mn,k)        : cote droit, equation de la divergence.
  ! e          rt(mn,k)        : cote droit, equation thermodynamique.
  ! E          RQ(MN,KM)       : COTE DROIT, EQUATION D'HUMIDITE.
  ! e          rx(mn,k)        : cote droit, equation du traceur.
  !
  ! e          dt              : longeur du pas de temps en seconde.
  !
  implicit none
  real, intent(inout) :: dt
  integer, intent(inout) :: itrac  !< Switch to indicate use of tracers in CanAM (0 = no, 1 = yes) \f$[unitless]\f$
  integer :: k
  integer, intent(inout) :: la
  integer :: mn
  integer :: n
  integer, intent(inout) :: nk
  integer, intent(inout) :: nm
  integer, intent(inout) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$

  complex, intent(inout) :: zp (la,nk) !< Variable description\f$[units]\f$
  complex, intent(inout) :: dp (la,nk) !< Variable description\f$[units]\f$
  complex, intent(inout) :: tp (la,nk) !< Variable description\f$[units]\f$
  complex, intent(inout) :: qp (la,nm) !< Variable description\f$[units]\f$
  complex, intent(in) :: rz (la,nk) !< Variable description\f$[units]\f$
  complex, intent(in) :: rd (la,nm) !< Variable description\f$[units]\f$
  complex, intent(in) :: rt (la,nk) !< Variable description\f$[units]\f$
  complex, intent(in) :: rq (la,nm) !< Variable description\f$[units]\f$
  complex, intent(inout) :: xp(la,nk,ntrac) !< Variable description\f$[units]\f$
  complex, intent(in) :: rx(la,nk,ntrac) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !-----------------------------------------------------------------------
  !     * perform an explicit timestep.
  !     * (T-1) TENDENCIES HAVE ALREADY BEEN INCLUDED IN R.H.S.'S.
  !     ------------------------------------------------------------------
  !
  do k=1,nk
    do mn=1,la
      !
      zp(mn,k) = rz(mn,k)*2.*dt
      dp(mn,k) = rd(mn,k)*2.*dt
      tp(mn,k) = rt(mn,k)*2.*dt
    end do
    !
  end do ! loop 4001
  !
  do k=1,nm
    do mn=1,la
      !
      qp(mn,k) = rq(mn,k)*2.*dt
    end do
    !
  end do ! loop 4002
  !
  !
  if (itrac/=0) then

    do n=1,ntrac
      do k=1,nk
        do mn=1,la
          !
          xp(mn,k,n)=rx(mn,k,n)*2.*dt
        end do
      end do
      !
    end do ! loop 4003

  end if
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
