!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine ener9 (tkine, tcpt, ttote, tvor2, tdiv2, tficx, &
                        tphsx, xadd, xtg, xref, ntrac, itrvar, &
                        tficq, qadd, &
                        pg, cg, tg, ug, vg, pressg, moist, &
                        dsgj, dshj, ilg, ilgp, il1, il2, ilev, lonsl, wj)
  !
  !     * apr 25/2010 - m.lazare. new version for gcm15i:
  !     *                         - passes in xtg and uses it to
  !     *                           do quadrature for new output
  !     *                           field tphsx.
  !     * dec 07/2004 - m.lazare. previous version for gcm15c/d/e/f/g/h:
  !     *                         - tficq,tficx now defined for each
  !     *                           level (vertical sum now done in
  !     *                           new enersv9 like other fields).
  !     *                         - calculation is same no matter
  !     *                           what choice of moisture/tracer
  !     *                           variable (much cleaner).
  !     * apr 01/2004 - m.lazare. previous version ener7x for gcm15b.
  !
  !     * this subroutine is purely diagnostic.
  !     * in the latitude loop it accumulates the following:
  !     *
  !     * tkine  = kinetic energy.
  !     * tcpt   = cp*t, will form potential energy once phis added to it.
  !     * ttote  = (unused here).
  !     * tvor2  = vorticity  squared /2.
  !     * tdiv2  = divergence squared /2.
  !     * tficq  = total fictitious water vapour added to fill holes(q)
  !     *        = total (e-p) (otherwise).
  !     * tphsx  = physics tracer tendencies.
  !     * qadd   = fictitious water vapour added to fill holes(q)
  !     *        = (e-p) (otherwise).
  !     * wj     = multiple-latitude array of gaussian weights.
  !     * ilgp   = first dimension of real :: grid arrays.
  !     * ilg    = number of distinct longitudes.
  !     * lonsl  = number of distinct longitudes in a single latitude.
  !     * jlat   = index of current latitude(s).
  !     * moist  = moisture variable.
  !
  use phys_consts, only : grav, cpres
  implicit none
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ilgp  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: itrvar
  integer, intent(in) :: lonsl
  integer, intent(in) :: moist
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  !
  real, intent(in), dimension(ilg,ilev) :: dsgj   !< Thickness of momentum layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ntrac) :: xref !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: qadd !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev,ntrac) :: xadd !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev,ntrac) :: xtg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev) :: tficq !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev,ntrac) :: tficx !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev,ntrac) :: tphsx !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilgp,ilev) :: pg !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilgp,ilev) :: cg !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilgp,ilev) :: tg !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilgp,ilev) :: ug !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilgp,ilev) :: vg !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real*8, intent(in) , dimension(ilg) :: wj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev) :: tkine !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev) :: tcpt !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilev) :: ttote !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev) :: tvor2 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev) :: tdiv2 !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  integer :: il
  integer :: l
  integer :: n
  !
  !     * work array:
  !
  real, dimension(ilg) :: wx !< Variable description\f$[units]\f$
  !-----------------------------------------------------------------------
  !     * set up array of "EFFECTIVE" weights used in quadrature.
  !
  do il=il1,il2
    wx(il)=.5*wj(il)/real(lonsl)
  end do ! loop 50
  !
  !     * accumulate the energetics.
  !
  do l=1,ilev
    do il=il1,il2
      tkine(l)=tkine(l) + dsgj(il,l)* pressg(il)*(wx(il)/grav)* &
                         (ug(il,l)**2+vg(il,l)**2)*0.5
      tcpt (l)=tcpt (l) + dshj(il,l)* pressg(il)*(wx(il)/grav)* &
                          tg(il,l)*cpres
      tvor2(l)=tvor2(l) + dsgj(il,l)* pressg(il)*(wx(il)/grav)* &
                         (pg(il,l)**2)*0.5
      tdiv2(l)=tdiv2(l) + dsgj(il,l)* pressg(il)*(wx(il)/grav)* &
                         (cg(il,l)**2)*0.5
    end do
  end do ! loop 200
  !
  !     * obtain global average of tficq (for each level).
  !     * if moist=4h   q, this represents fictitious moisture source
  !     * required to fill holes. otherwise, it represents physics
  !     * sources/sinks.
  !
  do l=1,ilev
    do il=il1,il2
      tficq(l) = tficq(l) + dshj(il,l)* pressg(il)*(wx(il)/grav)* &
                     qadd(il,l)
    end do
  end do ! loop 330
  !
  !     * obtain global average of tficx and tphsx (for each level).
  !
  do n=1,ntrac
    do l=1,ilev
      do il=il1,il2
        tficx(l,n) = tficx(l,n) + dshj(il,l)* pressg(il)*(wx(il)/grav)* &
                    xadd(il,l,n)
        tphsx(l,n) = tphsx(l,n) + dshj(il,l)* pressg(il)*(wx(il)/grav)* &
                    xtg (il,l,n)
      end do
    end do
  end do ! loop 350
  !
  return
  !-----------------------------------------------------------------------
end subroutine ener9
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
