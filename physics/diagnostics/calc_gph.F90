!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine calc_gph(gp, throw, shtj, pressg, phis, rgas, ilg, ilev, lev, il1, il2)
  !
  !     * jason cole - april 10, 2019  updated to work on xc40, remove ibm
  !     *                              intrinsics and use generic routines.
  !     * jason cole - march 30, 2010. new routine for gcm15h for
  !     *                              calculation of geopotential height
  !     *                              at model levels.
  !     *                              based on pahgt
  !
  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  real, intent(in) :: rgas  !< Ideal gas constant for dry air \f$[J kg^{-1} K^{-1}]\f$
  !
  real, intent(out), dimension(ilg,ilev) :: gp !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilg,ilev) :: throw   !< Physics internal field for temperature, including moon layer at atmospheric model top \f$[K]\f$
  real, intent(in),  dimension(ilg,lev)  :: shtj   !< Eta-level for top of thermodynamic layer, plus eta-level for surface (base of lowest layer) \f$[unitless]\f$
  real, intent(in),  dimension(ilg)      :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in),  dimension(ilg)      :: phis !< Variable description\f$[units]\f$
  !
  integer :: i
  integer :: il
  integer :: ilevm
  integer :: l
  real :: term1
  real, dimension(ilg,ilev) :: term2
  real, dimension(ilg,ilev) :: pf
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !---------------------------------------------------------------------
  !     * calculate geopotential height
  !
  do l = 1,ilev
    pf(il1:il2,l) = shtj(il1:il2,l + 1) * pressg(il1:il2)
  end do
  ilevm = ilev - 1
  gp(il1:il2,ilev) = 0.
  do l = 1, ilevm
    do i = il1, il2
      term1 = pf(i, l + 1) / pf(i, l)
      term2(i, l) = log(term1)
    end do
  end do

  do l = ilevm, 1, - 1
    do i = il1, il2
       gp(i, l) = gp(i, l + 1) + rgas * throw(i, l + 1) * term2(i, l)
    end do
  end do

  ! add surface geopotential
  do l = 1, ilev
    do il = il1, il2
      gp(il,l) = gp(il,l) + phis(il)
    end do ! il
  end do ! l
  !
  return
end subroutine calc_gph
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
