!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine calcanu2(anu, & ! output
                    cld, qcwvar, il1, il2, ilg, lay, use_mcica)     ! input

  !     * feb 13/2008 - jason cole. new version for gcm15h:
  !     *                           no cloud block minimum anu if mcica.
  !     * dec 05/2007 - jason cole. previous version calcanu for gcm15g:
  !     *                           calculates cloud inhomogeneity
  !     *                           factor "ANU".
  !     *

  implicit none
  !
  !     * output data
  !
  real, dimension(ilg,lay), intent(out) :: anu !< Variable description\f$[units]\f$
  !
  !     * input data
  !
  real, dimension(ilg,lay), intent(in) :: cld !< Cloud amount\f$[units]\f$
  real, dimension(ilg,lay), intent(in) :: qcwvar !< Variance of total cloud water\f$[units]\f$

  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay   !< Number of vertical layers \f$[unitless]\f$
  logical, intent(in) :: use_mcica   !< Switch to use McICA or determinstic radiative transfer \f$[unitless]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  ! local data
  !
  integer :: il
  integer :: k
  integer :: km1
  integer :: kp1
  integer :: laym1
  !
  ! parameters/data
  !
  real, parameter :: cut = 0.001 ! must be consistent with value in radiation
  !--------------------------------------------------------------------
  !     * compute the layer by layer values of anu
  !
  do k = 1, lay
    do il = il1, il2
      if (cld(il,k) >= cut) then
        anu(il,k) = 1.0/min(max(0.25,qcwvar(il,k)),2.0)
      else
        anu(il,k) = 1000.0
      end if
    end do ! il
  end do ! k

  if (.not. use_mcica) then
    !===================================================================
    !       * following needed for the 1d radiation code of li (non_mcica !)
    !       * minimum anu, it is extremely important to ensure consistency
    !       * between the definitions of anu here and their subsequent use in
    !       * lwtran4
    !===================================================================

    !       * compute the minimum value of anu for a cloud block
    !       * going from top to bottom

    do k = 1, lay
      km1 = k - 1
      do il = il1, il2
        if (cld(il,k) < cut) then
          anu(il,k) =  1000.0
        else
          if (k > 1) then
            anu(il,k)  =  min (anu(il,km1), anu(il,k))
          end if
        end if
      end do ! il
    end do ! k

    !       * compute the minimum value of anu for a cloud block
    !       * going from bottom to top

    laym1 = lay - 1
    do k = laym1, 1, - 1
      kp1 = k + 1
      do il = il1, il2
        if (cld(il,k) >= cut) then
          anu(il,k)  =  min (anu(il,kp1), anu (il,k))
        end if
      end do ! il
    end do ! k
  end if ! use_mcica

  return
end subroutine calcanu2
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
