!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine inthem2(ebocrow,ebocrol,ebbcrow,ebbcrol, &
                   eaocrow,eaocrol,eabcrow,eabcrol, &
                   easlrow,easlrol,eashrow,eashrol, &
                   ilg,il1,il2,levwf,delt,gmt,iday,mday)
  !
  !     * knut von salzen - jul 27,2009. new version for gcm15i:
  !     *                                - remove fbbc.
  !     * knut von salzen - feb 07,2009. previous version inthem for gcm15h:
  !     *                                - does interpolation of:
  !     *                                  eboc,ebbc,eaoc,eabc,easl,eash,fbbc.
  implicit none
  real, intent(in) :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  real :: fmsec
  real, intent(in) :: gmt  !< Real value of number of elapsed seconds in current day \f$[seconds]\f$
  integer :: i
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: levwf      !< Number of vertical layers for wildfire emissions input data \f$[unitless]\f$
  integer, intent(in) :: mday  !< Julian calendar day of next mid-month \f$[day]\f$
  real :: sec0
  real :: secsm
  !
  real, intent(inout), dimension(ilg) :: ebocrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ebocrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ebbcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ebbcrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: eaocrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: eaocrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: eabcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: eabcrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: easlrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: easlrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: eashrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: eashrol !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !--------------------------------------------------------------------
  !     * compute the number of timesteps from here to mday.
  !
  sec0 = real(iday) * 86400. + gmt
  !
  fmsec = real(mday) * 86400.
  if (fmsec < sec0) fmsec = fmsec + 365. * 86400.
  !
  secsm = fmsec - sec0
  !
  !     * general interpolation.
  !
  do i = il1,il2
    ebocrow(i) = ((secsm - delt) * ebocrow(i) + delt * ebocrol(i))/secsm
    ebbcrow(i) = ((secsm - delt) * ebbcrow(i) + delt * ebbcrol(i))/secsm
    eaocrow(i) = ((secsm - delt) * eaocrow(i) + delt * eaocrol(i))/secsm
    eabcrow(i) = ((secsm - delt) * eabcrow(i) + delt * eabcrol(i))/secsm
    easlrow(i) = ((secsm - delt) * easlrow(i) + delt * easlrol(i))/secsm
    eashrow(i) = ((secsm - delt) * eashrow(i) + delt * eashrol(i))/secsm
  end do ! loop 10
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
