!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine xtesoa3(xemis, pressg, dshj, eostrow, lpam, doc, &
                   ioco, iocy, ilg, il1, il2, ilev, ntrac, ztmst)
  !
  !     * jun 18/2013 - k.vonsalzen/ new version for gcm17+:
  !     *               m.lazare.    - add condition to set wgt=1. for
  !     *                              special case where hso2=lso2.
  !     *                            - add support for the pla option
  !     *                              "doc" array, "ipam" switch now passed.
  !     * apr 26/10 - k.vonsalzen. previous version xtesoa2 for gcm15i+:
  !     *                          - zf and pf calculated one-time at
  !     *                            beginning of physics and zf
  !     *                            passed in, with internal calculation
  !     *                            thus removed and no need to pass
  !     *                            in shbj,t.
  !     *                          - emissions accumulated into general
  !     *                            "xemis" array rather than updating
  !     *                            xrow (xrow updated from xemis in
  !     *                            the physics).
  !     *                          - removal of diagnostic fields
  !     *                            calculation, so no need to pass
  !     *                            in "saverad" or "isvchem".
  !     *                          - wgt=0 (ie no emissions in layer)
  !     *                            if it is above top or below base
  !     *                            of emissions.
  !     * knut von salzen - feb 07,2009. previous routine xtesoa for
  !     *                                gcm15h to apply emissions for esot.
  !     *                                this used to be in old xteaero.
  !
  use phys_consts, only : grav
  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ioco  !< Tracer index for organic carbon (hydrophobic) \f$[unitless]\f$
  integer, intent(in) :: iocy  !< Tracer index for organic carbon (hydrophylic) \f$[unitless]\f$
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  logical, intent(in) :: lpam  !< Switch to use PAM aerosol scheme \f$[unitless]\f$
  real, intent(in) :: ztmst
  !
  real, intent(in), dimension(ilg) :: eostrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(inout), dimension(ilg,ilev) :: doc !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev,ntrac) :: xemis !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  real :: eoc
  real :: fact
  integer :: il
  real :: pom2oc
  !-------------------------------------------------------------------
  pom2oc = 1./1.4
  do il = il1,il2
    fact = ztmst * grav/(dshj(il,ilev) * pressg(il)) * pom2oc
    eoc = eostrow(il)
    if (.not. lpam) then
      xemis(il,ilev,ioco) = xemis(il,ilev,ioco) + 0.5 * eoc * fact
      xemis(il,ilev,iocy) = xemis(il,ilev,iocy) + 0.5 * eoc * fact
    else
      doc(il,ilev) = doc(il,ilev) + eoc * fact/pom2oc/ztmst
    end if
  end do ! loop 30
  !
  return
end subroutine xtesoa3
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
