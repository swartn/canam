!> \file
!> \brief Compute oxidation and wet scavenging of chemical species
!!
!! @author Routine author name(s)
!
subroutine xtchemie6(xrow,th,qv,dqldt,dshj,shj,cszrow,zdayl,pressg,sfrc, &
                     ohrow,h2o2row,o3row,no3row,hno3row,nh3row,nh4row, &
                     wdld,wdlb,wdlo,wdls,slo3row,slhprow,phlrow, &
                     wdl4,wdl6,dox4row,doxdrow,noxdrow,depb,lsvchem, &
                     zclf,zmratep,zfsnow,zfrain,zmlwc,zfevap, &
                     clrfr,clrfs,zfsubl,kount,ztmst, &
                     ntrac,ilg,il1,il2,ilev,lev, &
  ! Additional optional variable required by gas-phase chemistry
                     wflxrow, switch_gas_chem)
  !
  !**** *xtchemie5*  calculates dry and wet chemistry
  !
  !     * jun 20/2013 - m.namazi.     new version for gcm17:
  !     *                             - calculate "depb" and pass out
  !     *                               to physics.
  !     * apr 25/2010 - m.lazare/     previous version xtchemie5 for
  !     *               k.vonsalzen.  gcm15i/gcm16:
  !     *                             - calls new oxistr3.
  !     *                             - if condition for night-time
  !     *                               chemistry now properly based
  !     *                               on "cszrow(il)<=0." instead
  !     *                               of "cszrow(il)<0.".
  !     *                             - under control of "isvchem" for
  !     *                               additional diagnostic chemistry
  !     *                               fields, {wdla,wdlc} removed and
  !     *                               {wdld,wdlb,wdlo,wdls} (for dust,
  !     *                               black carbon, organic carbon and
  !     *                               sea-salt, respectively) added.
  !     *                               also, clphrow removed and
  !     *                               calculation of all diagnostic
  !     *                               fields revised and streamlined.
  !     *                             - asrso4 increased from 0.75 to 0.9.
  !     * feb 16/2009 - k.vonsalzen.  previous version for gcm15h:
  !     *                             - initalize in-cloud and clear-sky
  !     *                               tracer mixing ratios using the
  !     *                               ratio of in-cloud over clear-sky
  !     *                               from the previous time step.
  !     *                             - calls new wetdep4.
  !     * dec 18/2007 - k.vonsalzen/  previous version xtchemie3 for gcm15g:
  !     *               m.lazare.     - remove unused khans=.true. code.
  !     *                             - pass in "adelt" from physics as
  !     *                               ztmst and use directly, rather than
  !     *                               passing in delt and forming
  !     *                               ztmst=2.*delt inside.c
  !     *                             - calls new wetdep3.
  !     *                             - remove setting of zclf to zero in
  !     *                               loop 210 if zclf<1.e-4.
  !     * nov 23/2006 - m.lazare.     previous version xtchemie2 for gcm15f:
  !     *                             - optimized to avoid unnecessary
  !     *                               calculations for khans=.true.
  !     *                             - zdep now an internal work array
  !     *                               and not passed out.
  !     *                             - calls revised wetdep2 and oxistr2.
  !     *                             - use variable instead of constant
  !     *                               in intrinsics such as "max",
  !     *                               so that can compile in 32-bit mode
  !     *                               with real(8).
  !     *                             - work arrays now local.
  !     *                             - zzo3 now a work array and not
  !     *                               passed (never really used in physics).
  !     * oct 24/2002 - k.vonsalzen.  previous version xtchemie up to gcm15e.
  !      m.lazare/k.vonsalzen    cccma          30/08/2000.
  !      j. feichter             uni hamburg    30/06/92
  !
  !      purpose
  !      ---------
  !      this routine computes the oxidation and the wet scavenging
  !      of chemical species.
  !
  !**    interface
  !      -----------
  !      *xtchemie5*   is called from   *physics*
  !
  !      externals
  !      ------------
  !          *wetdep4*       calculates wet deposition
  !          *wetdep4gasphs* calculates wet deposition for gas-phase species
  !
  use chem_params_mod,   only: jpwetr, nwflx, irnpos, chnrya, chnryb
  use wetdep4gasphs_mod, only: wetdep4gasphs
  use tracers_info_mod,  only: modl_tracers, iso2, iso4, idms, ihpo, &
                               issa, issc, idua, iduc, ibco, ibcy, ioco, iocy
  use phys_consts, only : grav, rgas, rgasv, eps1, mma_kg
  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  logical, intent(in) :: lsvchem  !< Switch to save extra chemistry diagnostics \f$[unitless]\f$
  logical, intent(in) :: switch_gas_chem !< Switch for gas-phase chemistry \f$[unitless]\f$
  real, intent(in) :: ztmst
  real, intent(inout), dimension(ilg,ilev,ntrac) :: xrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev,ntrac) :: sfrc !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(ilg,ilev) :: th !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: qv !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dqldt !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  !
  real, intent(in), dimension(ilg) :: cszrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in), dimension(ilg) :: zdayl  !< fraction of sunlit hours per day\f$[unitless]\f$
  !
  !     * species passed in vmr (dimensionless).
  !
  real, intent(in), dimension(ilg,ilev) :: ohrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: h2o2row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: o3row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: no3row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: hno3row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: nh3row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: nh4row !< Variable description\f$[units]\f$
  !
  real, intent(out), dimension(ilg) :: wdl4 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: wdl6 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: dox4row !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: doxdrow !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: noxdrow !< Variable description\f$[units]\f$
  !
  !     * arrays shared with cond in physics.
  !
  real, intent(out), dimension(ilg) :: wdld !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: wdlb !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: wdlo !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: wdls !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: depb !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: zclf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: zmratep !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: zfrain !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: clrfr !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: zfsnow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: zmlwc !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: zfevap !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: clrfs !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: zfsubl !< Variable description\f$[units]\f$
  !
  !     * optional diagnostic arrays under control of "isvchem":
  !
  real, intent(out), dimension(ilg,ilev) :: phlrow !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: slo3row !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: slhprow !< Variable description\f$[units]\f$
  !
  ! Field required for wet deposition of gas-phase chemical species:
  real, intent(out), dimension(ilg,nwflx) :: wflxrow

  ! Local variables used when switch_gas_chem is on
  integer :: ibcwash, jp
  real :: prx1     !< 1/R_air for air density calculation, units J/kg/K
  real :: rgcnst   !< Universal gas constant in units of (atm*m3)/(mol*K)
  real :: chenry, tinv1

  !
  real :: asrphob
  real :: asrso4
  real :: atmp
  real :: factcon
  integer :: il
  integer :: jk
  integer :: jt
  integer :: niter
  real :: pcons2
  real :: pqtmst
  real :: t
  real :: x
  real :: y
  real :: zavo
  real :: zdms
  real :: ze1h
  real :: ze1k
  real :: ze2h
  real :: ze2k
  real :: ze3h
  real :: ze3k
  real :: ze4h
  real :: ze4k
  real :: zexp
  real :: zf_h2o2
  real :: zh
  real :: zhil
  real :: zhp
  real :: zhpbase
  real :: zh_h2o2
  real :: zh_so2
  real :: zk
  real :: zk2
  real :: zk2f
  real :: zk2i
  real :: zk3
  real :: zlwcl
  real :: zlwcmin
  real :: zlwcv
  real :: zm
  real :: zmin
  real :: zmolgair
  real :: zmolgh2o2
  real :: zmolgs
  real :: zmolgw
  real :: znamair
  real :: zpfac
  real :: zp_h2o2
  real :: zp_so2
  real :: zq298
  real :: zqtp1
  real :: zrgas
  real :: zrk
  real :: zrke
  real :: zso2
  real :: ztk1
  real :: ztk2
  real :: ztk23b
  real :: ztk3
  real :: ztpq
  real :: zx
  real :: zxtp1dms
  real :: zxtp1so2
  !
  !     * internal work arrays:
  !
  !     * general work arrays for xtchemie5
  !
  real  ,  dimension(ilg,ilev,ntrac)  :: zxte
  real  ,  dimension(ilg,ilev,ntrac)  :: xcld
  real  ,  dimension(ilg,ilev,ntrac)  :: xclr
  real  ,  dimension(ilg,ilev)  :: zxtp10
  real  ,  dimension(ilg,ilev)  :: zxtp1c
  real  ,  dimension(ilg,ilev)  :: zhenry
  real  ,  dimension(ilg,ilev)  :: zso4
  real  ,  dimension(ilg,ilev)  :: zrkh2o2
  real  ,  dimension(ilg,ilev)  :: zzoh
  real  ,  dimension(ilg,ilev)  :: zzh2o2
  real  ,  dimension(ilg,ilev)  :: zzo3
  real  ,  dimension(ilg,ilev)  :: zzno3
  real  ,  dimension(ilg,ilev)  :: agthpo
  real  ,  dimension(ilg,ilev)  :: zrho0
  real  ,  dimension(ilg,ilev)  :: zrhctox
  real  ,  dimension(ilg,ilev)  :: zrhxtoc
  real  ,  dimension(ilg,ilev)  :: zmtof
  real  ,  dimension(ilg,ilev)  :: zdep3d
  real  ,  dimension(ilg,ilev)  :: pdclr
  real  ,  dimension(ilg,ilev)  :: pdcld
  real  ,  dimension(ilg)  :: zdt
  real  ,  dimension(ilg)  :: ze3
  real  ,  dimension(ilg)  :: zfac1
  real  ,  dimension(ilg)  :: zf_o3
  real  ,  dimension(ilg)  :: zf_so2
  real  ,  dimension(ilg)  :: zh2o2m
  real  ,  dimension(ilg)  :: zso2m
  real  ,  dimension(ilg)  :: zso4m
  real  ,  dimension(ilg)  :: zsumh2o2
  real  ,  dimension(ilg)  :: zsumo3
  real  ,  dimension(ilg)  :: zxtp1
  real  ,  dimension(ilg)  :: zza
  !
  real :: qmr
  real :: rgasa
  logical :: kchem !< Variable description\f$[units]\f$
  integer, dimension(ilg) :: ilwcp   !< Tracer index for cloud liquid water \f$[unitless]\f$
  logical, dimension(ilg,ilev) :: kcalc !< Variable description\f$[units]\f$
  real   , dimension(ilg,ilev) :: roarow !< Variable description\f$[units]\f$
  !
  !==================================================================
  ! physical (adjustable) parameters
  !
  real, parameter :: ytau = 1800.
  real, parameter :: plarge = 1.e+02
  real, parameter :: zfarr1 = 8.e+04
  real, parameter :: zfarr2 = -3650.
  real, parameter :: zfarr3 = 9.7e+04
  real, parameter :: zfarr4 = 6600.
  !==================================================================
  !
  real, parameter :: one = 1.
  real, parameter :: zero = 0.
  !=======================================================================
  !
  !     define function for changing the units
  !     from mass-mixing ratio to molecules per cm**3 and vice versa
  real :: ctox
  real :: xtoc
  real :: zfarr
  xtoc(x,y) = x * 6.022e+20/y
  ctox(x,y) = y/(6.022e+20 * x)
  !   x = density of air, y = mol weight in gramm
  !
  zfarr(zk,zh,ztpq) = zk * exp(zh * ztpq)
  !-----------------------------------------------------------------------
  !
  !     * initializations.
  !
  if (lsvchem) then
     wdl4 = 0.0
     wdl6 = 0.0
     wdlb = 0.0
     wdld = 0.0
     wdlo = 0.0
     wdls = 0.0
     dox4row = 0.0
     doxdrow = 0.0
     noxdrow = 0.0
     slo3row = 0.0
     slhprow = 0.0
     phlrow  = 0.0
  end if

  if (switch_gas_chem) wflxrow = 0.0
  !
  if (kount < 2) return
  !
  !    constants
  !
  pcons2 = 1./(ztmst * grav)
  pqtmst = 1./ztmst
  zmin = 1.e-20
  niter = 3
  asrso4 = 0.9
  asrphob = 0.
  !
  !    reaction rate so2-oh
  zk2i = 2.0e-12
  zk2 = 4.0e-31
  zk2f = 0.45
  !   reaction rate dms-no3
  zk3 = 1.9e-13
  !   molecular weights in g
  zmolgs = 32.064
  zmolgh2o2 = 34.01474
  zmolgair = 28.84
  zmolgw = 18.015
  !
  zhpbase = 2.5e-06
  ze1k = 1.1e-02
  ze1h = 2300.
  ze2k = 1.23
  ze2h = 3020.
  ze3k = 1.2e-02
  ze3h = 2010.
  ze4k = 6.6e-08
  ze4h = 1510.
  zq298 = 1./298.
  zrgas = 8.2e-02
  !
  zavo = 6.022e+23
  znamair = 1.e-03 * zavo/zmolgair
  !
  zlwcmin = 1.e-07
  !
  !  define constant 2-d slices.
  !
  do jk = 1,ilev
    do il = il1,il2
      zrho0(il,jk) = shj(il,jk) * pressg(il)/(rgas * th(il,jk) &
                     * (1. + (rgasv/rgas - 1.) * qv(il,jk)))
      zrhxtoc(il,jk) = xtoc(zrho0(il,jk),zmolgs)
      zrhctox(il,jk) = ctox(zrho0(il,jk),zmolgs)
      zmtof(il,jk) = pcons2 * dshj(il,jk) * pressg(il)
    end do
  end do ! loop 100
  !
  !  oxidant concentrations in molecule/cm**3
  !
  do jk = 1,ilev
    do il = il1,il2
      zx = zrho0(il,jk) * 1.e-03
      !
      !---    convert to molecules/cm3 from vmr
      !
      factcon = zx * 6.022045e23/zmolgair
      zzoh  (il,jk) = ohrow  (il,jk) * factcon
      zzh2o2(il,jk) = h2o2row(il,jk) * factcon
      zzo3  (il,jk) = o3row  (il,jk) * factcon
      zzno3 (il,jk) = no3row (il,jk) * factcon
    end do
  end do ! loop 115
  !
  !-----------------------------------------------------------------
  !  calculate hydrogen peroxide production and initialize fields.
  !
  do jk = 1,ilev
    do il = il1,il2
      !
      !---    convert molecules/cm**3 to kg-s/kg
      !
      agthpo(il,jk) = zzh2o2(il,jk) * 1.e+06 * 32.06e-03 &
                      / (6.022045e+23 * zrho0(il,jk))
      !
      if (kount == 2) xrow(il,jk,ihpo) = agthpo(il,jk)
      xrow(il,jk,ihpo) = xrow(il,jk,ihpo) - min (ztmst/ytau, one) &
                         * (xrow(il,jk,ihpo) - agthpo(il,jk) )
      !
      zdep3d(il,jk) = 0.
    end do
  end do ! loop 120
  !
  do jt = 1,ntrac
    do jk = 1,ilev
      do il = il1,il2
        zxte(il,jk,jt) = 0.
      end do
    end do
  end do ! loop 200
  !
  do jk = 1,ilev
    do il = il1,il2
      zhenry(il,jk) = 0.
      if (zclf(il,jk) < 1.e-04 .or. zmlwc(il,jk) < zmin) then
        zmratep(il,jk) = 0.
        zmlwc(il,jk) = 0.
      end if
    end do ! loop 210
  end do ! loop 212
  !
  !   calculates processes which are different inside and outside of clouds
  !
  jt = iso4
  do jk = 1,ilev
    do il = il1,il2
      xclr(il,jk,jt) = max(xrow(il,jk,jt),zero) &
                       /(zclf(il,jk) * (sfrc(il,jk,jt) - 1.) + 1.)
      zso4(il,jk) = sfrc(il,jk,jt) * xclr(il,jk,jt)
      xcld(il,jk,jt) = zso4(il,jk)
      zso4(il,jk) = max(zero,zso4(il,jk))
    end do ! loop 302
  end do ! loop 304
  !
  !
  !   calculate the reaction-rates for so2-h2o2
  do jk = 1,ilev
    do il = il1,il2
      if (zmlwc(il,jk) > zmin) then
        zlwcl = zmlwc(il,jk) * zrho0(il,jk) * 1.e-06
        zlwcv = zmlwc(il,jk) * zrho0(il,jk) * 1.e-03
        zhp = zhpbase + zso4(il,jk) * 1000./(zmlwc(il,jk) * zmolgs)
        zqtp1 = 1./th(il,jk) - zq298
        zrk = zfarr(zfarr1,zfarr2,zqtp1)/(0.1 + zhp)
        zrke = zrk/(zlwcl * zavo)
        !
        zh_so2 = zfarr(ze2k,ze2h,zqtp1)
        zpfac = zrgas * zlwcv * th(il,jk)
        zp_so2 = zh_so2 * zpfac
        zf_so2(il) = zp_so2/(1. + zp_so2)
        !
        zh_h2o2 = zfarr(zfarr3,zfarr4,zqtp1)
        zp_h2o2 = zh_h2o2 * zpfac
        zf_h2o2 = zp_h2o2/(1. + zp_h2o2)
        !
        zrkh2o2(il,jk) = zrke * zf_so2(il) * zf_h2o2
      else
        zrkh2o2(il,jk) = 0.
      end if
    end do ! loop 306
  end do ! loop 308
  !
  !     * heterogenous chemistry.
  !
  jt = iso2
  !
  !     * initialize.
  !
  do jk = 1,ilev
    do il = il1,il2
      zxtp10(il,jk) = xrow(il,jk,jt) &
                      /(zclf(il,jk) * (sfrc(il,jk,jt) - 1.) + 1.)
      zxtp1c(il,jk) = sfrc(il,jk,jt) * zxtp10(il,jk)
      xcld(il,jk,jt) = zxtp1c(il,jk)
      xclr(il,jk,jt) = zxtp10(il,jk)
    end do
  end do
  !
  do il = il1,il2
    ilwcp(il) = 0
  end do
  kchem = modl_tracers(iso2)%phs > 0 .and. modl_tracers(iso4)%phs > 0
  !
  !     * test if sufficient liquid water is available.
  !
  do jk = 1,ilev
    do il = il1,il2
      if (zmlwc(il,jk) > 0.1e-06 .and. zclf(il,jk) > 0.) then
        kcalc(il,jk) = kchem
        ilwcp(il) = 1
        qmr = qv(il,jk)/(1. - qv(il,jk))
        rgasa = rgas * (1. + qmr/eps1)/(1. + qmr)
        roarow(il,jk) = shj(il,jk) * pressg(il)/(rgasa * th(il,jk))
      else
        kcalc(il,jk)  = .false.
        roarow(il,jk) = 0.0
      end if
    end do
  end do
  if (any(ilwcp == 1)) then
     call oxistr3(il1,     il2,     ilev,    ilg,     ntrac,   kchem,  &
                  nh3row,  nh4row,  hno3row, zzo3,    xrow,    zmlwc,  &
                  zclf,    th,      roarow,  kcalc,   ilwcp,   dqldt,  &
                  ztmst,   zxtp1c,  zso4,    slo3row, slhprow, phlrow, &
                  lev,     lsvchem, zmtof,   zhenry)
  end if
  !
  !    calculate the wet deposition
  !
  !     * so2 wet deposition.
  !
  jt = iso2
  call wetdep4(ilg,il1,il2,ilev, zxtp10, &
               zxtp1c,zmtof,zrho0,zmratep,zmlwc, &
               zfsnow,zfrain,zdep3d,zclf,clrfr,zhenry, &
               clrfs,zfevap,zfsubl,pdclr,pdcld,jt,iso4)
  !
  !     * calculate new tendencies.
  !
  do jk = 1,ilev
    do il = il1,il2
      zxtp1(il) = (1. - zclf(il,jk)) * zxtp10(il,jk) + &
                  zclf(il,jk) * zxtp1c(il,jk)
      zxtp1(il) = zxtp1(il) - zdep3d(il,jk)
      zxte(il,jk,jt) = (zxtp1(il) - xrow(il,jk,jt)) * pqtmst
      xcld(il,jk,jt) = zxtp1c(il,jk) - pdcld(il,jk)
      xclr(il,jk,jt) = zxtp10(il,jk) - pdclr(il,jk)
      if (lsvchem) then
        atmp = max(zdep3d(il,jk) * pcons2 * dshj(il,jk) * pressg(il),0.)
        wdl4(il) = wdl4(il) + atmp
      end if
    end do ! loop 324
  end do ! loop 325
  !
  do jt = 1,ntrac
    if (modl_tracers(jt)%wet /= 0 .and. jt /= iso2) then

      if (jt == iso4) then
        do jk = 1,ilev
          do il = il1,il2
            zxtp1c(il,jk) = zso4(il,jk)
            zxtp10(il,jk) = xrow(il,jk,jt) &
                            /(zclf(il,jk) * (sfrc(il,jk,jt) - 1.) + 1.)
            xcld(il,jk,jt) = zso4(il,jk)
            xclr(il,jk,jt) = zxtp10(il,jk)
            zdep3d(il,jk) = 0.
          end do ! loop 330
        end do ! loop 331
      else
        do jk = 1,ilev
          do il = il1,il2
            zxtp10(il,jk) = xrow(il,jk,jt) &
                            /(zclf(il,jk) * (sfrc(il,jk,jt) - 1.) + 1.)
            zxtp1c(il,jk) = sfrc(il,jk,jt) * zxtp10(il,jk)
            xcld(il,jk,jt) = zxtp1c(il,jk)
            xclr(il,jk,jt) = zxtp10(il,jk)
            zdep3d(il,jk) = 0.
          end do ! loop 332
        end do ! loop 333
      end if
      !
      if (modl_tracers(jt)%wet == 1) then
        do jk = 1,ilev
          do il = il1,il2
            zhenry(il,jk) = asrso4
          end do
        end do ! loop 334
      else
        do jk = 1,ilev
          do il = il1,il2
            zhenry(il,jk) = asrphob
          end do
        end do ! loop 335
      end if
      !
      call wetdep4(ilg,il1,il2,ilev, zxtp10, &
                   zxtp1c,zmtof,zrho0,zmratep,zmlwc, &
                   zfsnow,zfrain,zdep3d,zclf,clrfr,zhenry, &
                   clrfs,zfevap,zfsubl,pdclr,pdcld,jt,iso4)
      !
      !   calculate new tendencies
      do jk = 1,ilev
        do il = il1,il2
          zxtp1(il) = (1. - zclf(il,jk)) * zxtp10(il,jk) + &
                      zclf(il,jk) * zxtp1c(il,jk)
          zxtp1(il) = zxtp1(il) - zdep3d(il,jk)
          zxte(il,jk,jt) = (zxtp1(il) - xrow(il,jk,jt)) * pqtmst
          xcld(il,jk,jt) = zxtp1c(il,jk) - pdcld(il,jk)
          xclr(il,jk,jt) = zxtp10(il,jk) - pdclr(il,jk)
          atmp = max(zdep3d(il,jk) * pcons2 * dshj(il,jk) * pressg(il),0.)
          if (jt == ibco .or. jt == ibcy) depb(il) = depb(il) + atmp
          if (lsvchem) then
            if (jt == iso4) &
                wdl6(il) = wdl6(il) + atmp
            if (jt == idua .or. jt == iduc) &
                wdld(il) = wdld(il) + atmp
            if (jt == ibco .or. jt == ibcy) &
                wdlb(il) = wdlb(il) + atmp
            if (jt == ioco .or. jt == iocy) &
                wdlo(il) = wdlo(il) + atmp
            if (jt == issa .or. jt == issc) &
                wdls(il) = wdls(il) + atmp
          end if
        end do
      end do ! loop 336
      !
    end if
  end do ! loop 354
  !   end of tracer loop
  !
  if (switch_gas_chem) then
  ! Calculate wet deposition for the gas-phase chemical species

    wflxrow(:, 1:2) = 0.0  ! initialize

    do jp = 1, jpwetr
    ! Calculate in-cloud (ZXTP1C) and clear-sky (ZXTP10) concentrations
    ! from the grid-cell average and the cloud/clear-sky ratio (SFRC)
      jt = irnpos(jp)

      do jk = 1, ilev
        do il = il1, il2
          zxtp10(il,jk) = xrow(il,jk,jt) /    &
                        (zclf(il,jk)*(sfrc(il,jk,jt) - 1.0) + 1.0)
          zxtp1c(il,jk) = sfrc(il,jk,jt) * zxtp10(il,jk)
        enddo
      enddo
      xcld(:,:,jt) = zxtp1c(:,:)
      xclr(:,:,jt) = zxtp10(:,:)
     ! zdep3d(:,:)  = 0.0
      zdep3d = 0.0

      ! Calculate the fraction of gas-phase species in cloud water
      !
      ! The standard form of Henry's Law gives H_cp as H_cp = c_aqueous / P,
      ! where c_aqueous has units of mol/L and P (partial pressure of the gas)
      ! is in atmospheres.
      !
      ! Using the ideal gas law (P = RTn/V, with R = 8.2057E-05 atm m^3/K/mol)
      ! Henry's Law can be expressed as: Hcp*R*T = c_aq / (n/V)
      ! where n/V has units of mol/m^3
      !
      ! The amount of the species in cloud water will be c_aq * LWC,
      ! with cloud liquid water (LWC) in units of L/m^3 (or kg-H2O/m^3 air)
      ! and the ratio c_aq/c_gas = Hcp * R * T * LWC
      !
      ! Here we calculate the fraction in the aqueous phase as:
      !                                           1.0
      !     (1 - c_gas/(c_aq+c_gas)) = 1 - -------------------
      !                                    1 + Hcp x R x T x LWC
      !
      ! Note conversion of cloud water (zmlwc) from kg H2O/kg air to kg H2O/m^3
      ! by multiplication by air density:
      ! rho_air (kg/m^3) = PV/RT, with R=287.058 for air
      !
      ! Highly soluble species are treated with the ELSE condition that assumes
      ! almost all of the species is in the aqueous phase.
      if (chnrya(jp) > 0.0) then
        zhenry(:,:) = 0.0
        rgcnst = 8.2057E-05     ! R with units of (atm m^3)/(mol K)
        prx1   = 1.0/287.058    ! 1/R_air J/kg/K for air density calculation
        tinv1  = 1.0/298.0

        do jk = 1, ilev
          do il = il1, il2
            if (zmlwc(il,jk) > zmin) then
              chenry = chnrya(jp) * exp(chnryb(jp) * (1.0/th(il,jk) - tinv1))
              zhenry(il,jk) = 1.0 - 1.0 / (rgcnst * chenry * shj(il,jk)  &
                            * pressg(il) * prx1 * zmlwc(il,jk) + 1.0)
            endif
          enddo
        enddo
      else
        zhenry(:,:) = 0.999
      endif

      ! Set flag for whether below-cloud removal is to be calculated
      ! Currently this is only done for highly soluble species to ensure
      ! that saturation is not a problem.
      ibcwash=1
      if (chnrya(jp) > 0.0) ibcwash = 0

      call wetdep4gasphs (zxtp10, zxtp1c, zmtof, shj, pressg, th, zmratep,  &
                          zmlwc, zfrain, zdep3d, zclf,clrfr, zhenry,zfevap, &
                          pdclr,pdcld, ztmst, ibcwash, ilg,il1,il2,ilev)

      ! Modified approach to passing forward tendencies, reflecting the fact
      ! that ZXTP10 and ZXTP1C are not modified in WETDEP4GASPHS
      do jk = 1, ilev
        do il = il1, il2
          zxte(il,jk,jt) = -1.0 * zdep3d(il,jk) * pqtmst
          xcld(il,jk,jt) = zxtp1c(il,jk) - pdcld(il,jk)
          xclr(il,jk,jt) = zxtp10(il,jk) - pdclr(il,jk)
        enddo
      enddo

      ! Diagnostic wet deposition in units of moles/m^2/sec
      ! => what is the magic number in the denominator?
      if (jt == irnpos(5)) then  ! irnpos(5)=itn5, the tracer index for HNO3
        do il = il1, il2
          atmp = 0.0
          do jk = 1, ilev
            atmp = atmp + zdep3d(il,jk)*dshj(il,jk)
          enddo
          wflxrow(il,1) = wflxrow(il,1) + atmp*pcons2*pressg(il) / mma_kg
        enddo
      endif

      if (jt == irnpos(2)) then  ! irnpos(2)=itn7, the tracer index for HNO4
        do il = il1, il2
          atmp = 0.0
          do jk = 1, ilev
            atmp = atmp + zdep3d(il,jk)*dshj(il,jk)
          enddo
          wflxrow(il,2) = wflxrow(il,2) + atmp*pcons2*pressg(il) / mma_kg
        enddo
      endif

    enddo     ! end loop JP=1,JPWETR over gas-phase species
  endif  ! end conditional on switch_gas_chem
  !
  ! Oxidation with OH
  !
  jt = iso2
  !   day-time chemistry
  do jk = 1,ilev
    do il = il1,il2
      if (cszrow(il) > 0.) then
        zxtp1so2 = xrow(il,jk,jt) + zxte(il,jk,jt) * ztmst
        if (zxtp1so2 <= zmin) then
          zso2 = 0.
        else
          ztk2 = zk2 * (th(il,jk)/300.) ** ( - 3.3)
          zm = zrho0(il,jk) * znamair
          zhil = ztk2 * zm/zk2i
          zexp = log10(zhil)
          zexp = 1./(1. + zexp * zexp)
          ztk23b = ztk2 * zm/(1. + zhil) * zk2f ** zexp
          zso2 = zxtp1so2 * zrhxtoc(il,jk) * zzoh(il,jk) * ztk23b * zdayl(il)
          zso2 = zso2 * zrhctox(il,jk)
          zso2 = min(zso2,zxtp1so2 * pqtmst)
          zxte(il,jk,jt) = zxte(il,jk,jt) - zso2
          zxte(il,jk,iso4) = zxte(il,jk,iso4) + zso2
        end if
        !
        zxtp1dms = xrow(il,jk,idms) + zxte(il,jk,idms) * ztmst
        if (zxtp1dms <= zmin) then
          zdms = 0.
        else
          t = th(il,jk)
          ztk1 = (t * exp( - 234./t) + 8.46e-10 * exp(7230./t) + &
                 2.68e-10 * exp(7810./t))/(1.04e+11 * t + 88.1 * exp(7460./t))
          zdms = zxtp1dms * zrhxtoc(il,jk) * zzoh(il,jk) * ztk1 * zdayl(il)
          zdms = zdms * zrhctox(il,jk)
          zdms = min(zdms,zxtp1dms * pqtmst)
          zxte(il,jk,idms) = zxte(il,jk,idms) - zdms
          zxte(il,jk,jt) = zxte(il,jk,jt) + zdms
        end if
        !
        if (lsvchem) then
          dox4row(il) = dox4row(il) + zso2 * dshj(il,jk) * pressg(il) / grav
          doxdrow(il) = doxdrow(il) + zdms * dshj(il,jk) * pressg(il) / grav
        end if
      end if
    end do ! loop 410
  end do ! loop 412
  !
  !   night-time chemistry
  do jk = 1,ilev
    do il = il1,il2
      if (cszrow(il) <= 0.) then
        zxtp1dms = xrow(il,jk,idms) + zxte(il,jk,idms) * ztmst
        if (zxtp1dms <= zmin) then
          zdms = 0.
        else
          ztk3 = zk3 * exp(520./th(il,jk))
          zdms = zxtp1dms * zrhxtoc(il,jk) * zzno3(il,jk) * ztk3
          zdms = zdms * zrhctox(il,jk)
          zdms = min(zdms,zxtp1dms * pqtmst)
          zxte(il,jk,idms) = zxte(il,jk,idms) - zdms
          zxte(il,jk,jt) = zxte(il,jk,jt) + zdms
          if (lsvchem) then
            noxdrow(il) = noxdrow(il) + zdms * dshj(il,jk) * pressg(il) / grav
          end if
        end if
      end if
    end do ! loop 414
  end do ! loop 416
  !
  if (switch_gas_chem) then
    do jt = 1, ntrac
    ! Revised logic regarding the calculation of cloudy to clear sky ratios.
    ! Update the concentrations:
      do jk = 1, ilev
        do il = il1, il2
          xrow(il,jk,jt) = xrow(il,jk,jt) + zxte(il,jk,jt)*ztmst
          xrow(il,jk,jt) = max(xrow(il,jk,jt),zero)
        enddo
      enddo

      if (modl_tracers(jt)%wet /=0 .and. jt/=iso2 .and. jt/=ihpo) then
        do jk=1,ilev
          do il=il1,il2
            if (xrow(il,jk,jt) > zmin .and. zclf(il,jk) > 1.e-02) then
              if (xclr(il,jk,jt) > zmin ) then
                sfrc(il,jk,jt) = xcld(il,jk,jt) / xclr(il,jk,jt)
                sfrc(il,jk,jt) = min(plarge, max(1.0/plarge,sfrc(il,jk,jt)))
              else
                sfrc(il,jk,jt) = plarge
              endif
            else
                sfrc(il,jk,jt) = 1.0
            endif
          enddo
        enddo
      else
        sfrc(:,:,jt) = 1.0
      endif
    enddo       ! end outer loop 422 over JT=1,NTRAC
    !
    ! Calculate SFRC for gas-phase species that wet deposit because
    ! these species have ITRWET=0
    do jp = 1, jpwetr
      jt = irnpos(jp)
      do jk = 1, ilev
        do il = il1, il2
          if (xrow(il,jk,jt) > zmin .and. zclf(il,jk) > 1.e-02) then
            if (xclr(il,jk,jt) > zmin) then
              sfrc(il,jk,jt) = xcld(il,jk,jt) / xclr(il,jk,jt)
              sfrc(il,jk,jt) = min( plarge, max(1.0/plarge,sfrc(il,jk,jt)) )
            else
              sfrc(il,jk,jt) = plarge
            endif
          else
            sfrc(il,jk,jt) = 1.0
          endif
        enddo
      enddo
    enddo

  else  ! default calculation when switch_gas_chem=F
    do jt = 1, ntrac
      do jk = 1, ilev
        do il = il1, il2

          xrow(il,jk,jt) = xrow(il,jk,jt) + zxte(il,jk,jt) * ztmst
          xrow(il,jk,jt) = max(xrow(il,jk,jt),zero)
          if (modl_tracers(jt)%wet /= 0 .and. xrow(il,jk,jt) > zmin &
              .and. zclf(il,jk) > 1.e-02 .and. jt /= iso2 .and. jt /= ihpo) then
            if (xclr(il,jk,jt) > zmin) then
              sfrc(il,jk,jt) = xcld(il,jk,jt)/xclr(il,jk,jt)
              sfrc(il,jk,jt) = min(plarge,max(1.0/plarge,sfrc(il,jk,jt)))
            else
              sfrc(il,jk,jt) = plarge
            end if
          else
            sfrc(il,jk,jt) = 1.0
          end if
        enddo ! loop 420
      enddo   ! loop 421
    enddo       ! end outer loop 422 over JT=1,NTRAC
  endif     ! end conditional on logical switch_gas_chem
  !
  return
end subroutine xtchemie6
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
