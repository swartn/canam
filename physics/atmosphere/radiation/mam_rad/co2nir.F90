!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine co2nir(hsco2,hr,rmu0, &
                        xvf,co2vf,pcd,icd,k1co2s,k2co2s,wco2s,wco2st, &
                        cco2,lay,ilg,il1,il2)
  !****************************************************************************
  !*                                                                          *
  !*                    subroutine co2nir                                     *
  !*                                                                          *
  !****************************************************************************
  !
  ! to calculate solar heating in the near ir co2 bands. method by
  ! ogibalov and fomichev [asr, 2002] is used.
  !
  !                                    september, 2002, v.i. fomichev
  !              - modified for the ckd scheme, novrmber, 2004, v.i. fomichev
  ! called by mamrad2
  ! calls a18lin
  !
  ! output: hsco2 -- solar heating in the near ir bands in k/s at model grid
  ! ^^^^^^
  ! input:  rmu0 -- cosine of the solar zenith angle
  ! ^^^^^   cco2 -- co2 vmr in the troposphere
  !         xvf  -- model vertical grid in pressure scale height
  !                         (passed through therdat, calculated by thermdat2)
  !         co2vf-- co2 v.m.r. (passed through therdat, calculated by vmrco2,
  !                             which is called from mamrad2)
  !         co2cl-- co2 column amount at xr grid (0-16.5,0.25) (passed through
  !                                 pirco2, determined by dpmco2 subroutine,
  !                                 based on vmr from co2vp array)
  ! tco2ir, qnir -- parameterization coefficients (passed through co2nir,
  !                                   determined in pco2nir subroutine)
  !     pcd, icd -- coefficients and indexes for interpolation from radiation
  !                 to model vertical grid (passed through vfclpr, calculated
  !                 by precl2 which is called after thermdat2)
  ! k1co2s,k2co2s, wco2s,wco2st - indexes and weights to implement the heating
  !                           (passed through radidx, calculated in thermdat2)

  implicit none
  real :: a18lin
  real, intent(in) :: cco2
  real :: co2cl
  real :: co2vp
  real :: g
  real :: heatco2
  integer :: i
  integer :: il
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: k
  integer :: kk
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  real :: qnir
  real :: tco2
  real :: tco2ir

  real, intent(inout), dimension(ilg,lay) :: hsco2 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: rmu0 !< Variable description\f$[units]\f$

  real, intent(in)  , dimension(lay)   :: xvf !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(lay)   :: co2vf !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(3,lay) :: pcd !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(lay)   :: wco2s !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(lay)   :: wco2st !< Variable description\f$[units]\f$
  integer, intent(in) :: k1co2s !< Variable description\f$[units]\f$
  integer, intent(in) :: k2co2s !< Variable description\f$[units]\f$
  integer, intent(in),dimension(lay)   :: icd !< Variable description\f$[units]\f$

  common /pirco2/ co2vp(67), co2cl(67)
  common /co2nirp/ tco2ir(49,10),qnir(49,10)

  ! auxiliary arrays
  real, intent(inout), dimension(ilg,49) :: hr !< Variable description\f$[units]\f$
  real :: tco2o(10) !<
  real :: qco2o(10) !<
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !============================================================================
  ! initialization
  do k=1,lay
    do il = il1,il2
      hsco2(il,k)=0.0
    end do
  end do

  do k=1,49
    do il = il1,il2
      hr(il,k)=1.0
    end do
  end do

  ! ***** heating at radiation vertical grid (x=2-14,0.25):

  do k=1,49

    ! parameterization coefficients for level "k"
    do i=1,10
      tco2o(i)=tco2ir(k,i)
      qco2o(i)=qnir(k,i)
    end do
    ! column co2 amount above level "k"
    tco2=co2cl(k+8)*cco2/3.600e-04

    do il=il1, il2

      heatco2=0.0
      if (rmu0(il) > 0.0) then

        ! co2 amount along the paths
        g=log(tco2*35./sqrt(1224.0*rmu0(il)*rmu0(il)+1.))

        ! solar heating:
        if (g<tco2o(1)) then
          heatco2=qco2o(1)
        else if (g>tco2o(10)) then
          heatco2=0.0
        else
          heatco2=a18lin(g,tco2o,qco2o,1,10)
        end if

      end if      ! rmu0

      hr(il,k)=heatco2

    end do ! loop 11
  end do ! loop 10

  ! ***** heating at model grid

  do k=k1co2s,k2co2s-1
    kk = icd(k)
    do il = il1,il2
      hsco2(il,k) = co2vf(k)*(pcd(1,k)*hr(il,kk)+ &
                      pcd(2,k)*hr(il,kk+1)+pcd(3,k)*hr(il,kk+2))
    end do ! loop 21
  end do ! loop 20
  do k=k2co2s,lay
    do il = il1,il2
      hsco2(il,k) = wco2st(k)*co2vf(k)*hr(il,48)
    end do ! loop 23
  end do ! loop 22

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
