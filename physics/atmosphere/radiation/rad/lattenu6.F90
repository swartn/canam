!> \file
!>\brief Attenuation of thermal radiation between the top of model and top of atmosphere
!!
!! @author Jiangnan Li
!
subroutine lattenu6(atten, ib, ig, o3, q, co2, dp, dip, dt, dt0, &
                    inpt, il1, il2, ilg, lay)
  !
  !     * jun 22,2013 - j.cole.   set the radiative effect of the moon
  !     *                         layer to zero at end (atten=0.),
  !     *                         so that balt=balx.
  !     * may 01,2012 - m.lazare. previous version lattenu5 for gcm16:
  !     *                         - calls attenue5 instead of attenue4.
  !     * feb 09,2009 - j.li.     previous version lattenu4 for gcm15h/i:
  !     *                         - 3d ghg implemented, thus no need
  !     *                           for "trace" common block or
  !     *                           temporary work arrays to hold
  !     *                           mixing ratios of ghg depending on
  !     *                           a passed, specified option.
  !     *                         - calls attenue4 instead of attenue3.
  !     * apr 18,2008 - m.lazare/ previous version lattenu3 for gcm15g:
  !     *               j.li.     - calls attenue3 instead of attenue2.
  !     * may 05,2006 - m.lazare. previous version lattenu2 for gcm15e/f:
  !     *                         - pass integer :: variables "init" and
  !     *                           "nit" instead of actual integer
  !     *                           values, to "attenue" routines.
  !     * original version lattenu by jiangnan li.
  !----------------------------------------------------------------------c
  !     calculation of the attenuation for the downward flux above the   c
  !     model top level. since the temperature at 0.005 mb is unknown we c
  !     assume it is the same as that of model top level                 c
  !                                                                      c
  !     atten: for solar: the attenuation factor for downward flux from  c
  !            toa to the model top level; for longwave: the optical     c
  !            / diffuse factor                                          c
  !     dp:    here dp is only the pressure difference, different from   c
  !            that defined in raddriv. so there is a factor 1.02        c
  !     o3:    o3 mass mixing ratio                                      c
  !     q:     water vapor mass mixing ratio                             c
  !     dip:   interpretation factor for pressure between two            c
  !            neighboring standard input data pressure levels           c
  !     dt:    layer temperature - 250 k                                 c
  !     dt0:   temperature in moon layer - 250 k                         c
  !----------------------------------------------------------------------c
  use ckdlw4, only : cl1co2gh, cl3h2ogh, cl5o3gh, cl7co2gh, cl8h2ogh, cl9h2ogh, ntl
  implicit none
  integer, intent(in) :: ib
  integer, intent(in) :: ig
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  !
  real, intent(inout), dimension(ilg)   :: atten !< Attenuated transmission factor for longwave \f$[1]\f$
  real, intent(in), dimension(ilg)      :: o3 !< O3 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg, lay) :: q !< Water vapor \f$[gram/gram]\f$
  real, intent(in), dimension(ilg, lay) :: co2 !< CO2 \f$[gram/gram]\f$
  real, intent(in), dimension(ilg, lay) :: dp !< Airmass path of a layer \f$[gram/cm^2]\f$
  real, intent(in), dimension(ilg, lay) :: dip !< Interpretation between two neighboring standard input pressure levels \f$[1]\f$
  real, intent(in), dimension(ilg, lay) :: dt !< Temperature profile - 250 K \f$[K]\f$
  real, intent(in), dimension(ilg)      :: dt0 !< Temperature in layer between model top and top of atmosphere - 250 K \f$[K]\f$
  integer, intent(in), dimension(ilg)   :: inpt !< Level number of the selected standard input pressures \f$[1]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  integer :: i
  integer :: isl
  real, dimension(ilg) :: rmu !< Cosine of zenith angle \f$[unitless]\f$
  real, dimension(ilg) :: mmr !< Placeholder for gas concentration (mixing ratio) at model top
  real, dimension(ilg) :: dp0 !< Pressure in layer between model top and top of atmosphere
  real, dimension(ilg) :: tt0 !< Temperature in layer between model top and top of atmosphere - 250K
  real, pointer, dimension(:, :) :: coeff
  !
  !=======================================================================
  !
  !     * set the radiative effect of the moon layer to zero.
  !
  do i = il1, il2
    atten(i) =  0.0
  end do

  return
  !
  nullify(coeff)
  if (ib == 1) then
    isl = 2
    coeff => cl1co2gh(:, :, ig)
    mmr = co2(:, 1)
    !
  else if (ib == 3) then
    isl = 2
    coeff => cl3h2ogh(:, :, ig)
    mmr = q(:, 1)
    !
  else if (ib == 5) then
    isl = 2
    coeff => cl5o3gh(:, :, ig)
    mmr = o3
    !
  else if (ib == 7) then
    isl = 2
    coeff => cl7co2gh(:, :, ig)
    mmr = co2(:, 1)
    !
  else if (ib == 8) then
    isl = 2
    coeff => cl8h2ogh(:, :, ig)
    mmr = q(:, 1)
    !
  else if (ib == 9) then
    isl = 2
    coeff => cl9h2ogh(:, :, ig)
    mmr = q(:, 1)
    !
  end if

  if (associated(coeff)) then
     dp0 = dp(:, 1)
     tt0 = dt(:, 1)
     call attenue5(atten, coeff, mmr, dp0, dip, tt0, dt0, &
                   rmu, inpt, ntl, isl, il1, il2, ilg)
  else
     atten = 0.0
  end if
  !
  return
end subroutine lattenu6
!> \file
!> Calculation attenuation of the longwave flux between the model top level and
!! the top of the atmosphere which is assumed to at 0.005 hPa with an assumed
!! temperature that is the same as that of model top level.
!!\n
!! **Note that currently it is assumed there is no attenuation above the model
!! top.**
