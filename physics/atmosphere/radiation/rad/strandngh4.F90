!> \file
!>\brief Compute the downward solar flux from model top to 1 hPa for optically thick atmospheres
!!
!! @author Jiangnan Li
!
subroutine strandngh4 (trant, itile, &
                       gwgh, atten, taua, tauoma, taucs, &
                       tauomc, cld, rmu, dp, o3, q, co2, ch4, o2, &
                       ib, ig, inpt, dip, dt, lev1, gh, cut, &
                       il1, il2, ilg, lay, lev, ntile)
  !
  !     * jun 02,2015 - m.lazare/ new version for gcm19:
  !     *               j.cole:   - add tiled radiation calculations
  !     *                           (ie "trant")
  !     * feb 09,2009 - j.li.     previous version strandngh4 for gcm15h
  !     *                         through gcm18:
  !     *                         - 3d ghg implemented, thus no need
  !     *                           for "trace" common block or
  !     *                           temporary work arrays to hold
  !     *                           mixing ratios of ghg depending on
  !     *                           a passed, specified option.
  !     *                         - calls tline{1,2}z instead of tline{1,2}y.
  !     * apr 18,2008 - m.lazare/ previous version strandngh3 for gcm15g:
  !     *               l.solheim/- cosmetic change to add threadprivate
  !     *               j.li.       for common block "trace", in support
  !     *                           of "radforce" model option.
  !     *                         - calls tline{1,2}y instead of tline{1,2}x.
  !     *                         - updating o3, adding ch4 and using
  !     *                           kurucz solar function.
  !     * may 05,2006 - m.lazare. previous version strandngh2 for gcm15e/f:
  !     *                         - pass integer :: variables "init" and
  !     *                           "nit" instead of actual integer
  !     *                           values, to "tline_" routines.
  !     * apr 25,2003 - j.li.     previous version strandngh for gcm15d.
  !----------------------------------------------------------------------c
  !     calculation of the downward solar flux under the condition that  c
  !     the extinction coefficient of gas is very large, the scattering  c
  !     effects can be neglected. the cloud optical depth is much smallerc
  !     than the gaseous optical depth, the cloud effect is very small   c
  !     and be treated simply                                            c
  !                                                                      c
  !     hrs:   solar heating rate (k / sec)                              c
  !     tran:  downward flux                                             c
  !    trant:  tiled downward flux                                       c
  !    itile:  flag indicating calculation for particular tile.          c
  !     atten: attenuation factor for downward flux from toa to the      c
  !            model top level                                           c
  !     taucs: cloud optical depth                                       c
  !     cld:   cloud fraction                                            c
  !     rmu:   cos of solar zenith angle                                 c
  !     dp:    air mass path for a model layer (exlained in raddriv).    c
  !     o3:    o3 mass mixing ratio                                      c
  !     q:     water vapor mass mixing ratio                             c
  !     co2:   ch4, o2 are mass mixing                                   c
  !     inpt:  number of the level for the standard input data pressures c
  !     dip:   interpretation factor for pressure between two            c
  !            neighboring standard input data pressure levels           c
  !     dt:    layer temperature - 250 k                                 c
  !----------------------------------------------------------------------c
  use ckdsw4, only : gws1gh, gws2gh, gws3gh, gws4gh, &
                     cs1o2gh3, cs1o3gh, cs2h2ogh, cs2o2gh, &
                     cs3co2gh, cs3h2ogh, cs4ch4gh, cs4co2gh, cs4h2ogh, ntl
  implicit none
  real, intent(in) :: cut
  real, intent(inout) :: gwgh
  integer, intent(in) :: ib
  integer, intent(in) :: ig
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: lev1  !< Vertical level at which pressure becomes greater than 1 hPa \f$[unitless]\f$
  integer, intent(in) :: ntile  !< Number of surface tiles in an atmospheric column \f$[unitless]\f$
  !
  real, intent(in), dimension(ilg) :: atten !< Attenuated downward solar flux from
                                            !! top of atmosphere to model top \f$[W/m^2]\f$
  real, intent(in), dimension(ilg,lay) :: taua !< Gas plus aerosol optical thickness \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: tauoma !< Gaseous plus aerosol optical depth times aerosol
                                                 !! single scattering albedo \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: taucs !< Cloud optical depth \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: tauomc !< Cloud optical depth times cloud single scattering albedo \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: cld !< Cloud fraction \f$[1]\f$
  real, intent(in), dimension(ilg) :: rmu !< 1/(cosine of solar zenith angle) \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: dp !< Airmass path of a layer \f$[gram/cm^2]\f$
  real, intent(in), dimension(ilg,lay) :: o3 !< O3 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: q !< H2O mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: co2 !< CO2 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: ch4 !< CH4 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: o2 !< O2 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: dip !< Interpretation between two neighboring
                                              !! standard input pressure levels \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: dt !< Layer temperature - 250 K \f$[K]\f$
  real, intent(inout), dimension(ilg,ntile,2,lev) :: trant !< Downward solar flux for each tile \f$[W/m^2]\f$
  integer, intent(in), dimension(ilg,ntile) :: itile !< Index of surface tile \f$[1]\f$
  integer, intent(in), dimension(ilg,lay) :: inpt !< Index of standard input pressures for each model layer \f$[1]\f$
  logical, intent(in) :: gh
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * internal work array.
  !
  real, dimension(ilg,lay) :: taug
  real, dimension(ilg,2,lev) :: tran
  !
  integer, external :: mvidx
  real :: absc
  real :: dto3
  real :: dtr1
  integer :: i
  integer :: im
  integer :: init
  integer :: k
  integer :: kp1
  integer :: m
  real :: tau
  real, pointer, dimension(:, :) :: coeff, coeff2
  !
  !=======================================================================
  do i = il1, il2
    tran(i,1,1)           =  atten(i)
    tran(i,2,1)           =  atten(i)
  end do ! loop 10
  !
  if (ib == 1) then
    !
    !----------------------------------------------------------------------c
    !     band1 for uvc (35700 - 50000 cm-1), nongray gaseous absorption  c
    !     of o2  and o3. solar energy 7.5917  w m-2                       c
    !----------------------------------------------------------------------c
    !
    if (ig == 3) then
      do k = 1, lay
        kp1 = k + 1
        do i = il1, il2
          dto3            =  dt(i,k) + 23.13
          tau             = ((cs1o3gh(1,ig) + &
                            dto3 * (cs1o3gh(2,ig) + &
                            dto3 * cs1o3gh(3,ig))) * o3(i,k) + &
                            cs1o2gh3 * o2(i,k)) * dp(i,k) + &
                            taua(i,k)
          dtr1            =  exp( - (tau - tauoma(i,k)) / rmu(i))
          tran(i,1,kp1)   =  tran(i,1,k) * dtr1
          !
          if (cld(i,k) < cut) then
            tran(i,2,kp1) =  tran(i,2,k) * dtr1
          else
            absc          = (1.0 - cld(i,k)) * dtr1 + cld(i,k) * &
                            exp( - (tau + taucs(i,k) - tauomc(i,k)) &
                            / rmu(i))
            tran(i,2,kp1) =  tran(i,2,k) * absc
          end if
        end do ! loop 100
      end do ! loop 105
    else
      do k = 1, lay
        kp1 = k + 1
        do i = il1, il2
          dto3            =  dt(i,k) + 23.13
          tau             = (cs1o3gh(1,ig) + dto3 * (cs1o3gh(2,ig) + &
                            dto3 * cs1o3gh(3,ig))) * o3(i,k) * &
                            dp(i,k) + taua(i,k)
          dtr1            =  exp( - (tau - tauoma(i,k)) / rmu(i))
          tran(i,1,kp1)   =  tran(i,1,k) * dtr1
          !
          if (cld(i,k) < cut) then
            tran(i,2,kp1) =  tran(i,2,k) * dtr1
          else
            absc          = (1.0 - cld(i,k)) * dtr1 + cld(i,k) * &
                            exp( - (tau + taucs(i,k) - tauomc(i,k)) &
                            / rmu(i))
            tran(i,2,kp1) =  tran(i,2,k) * absc
          end if
        end do ! loop 110
      end do ! loop 115
    end if
    gwgh =  gws1gh(ig)
    !
  else if (ib == 2) then
    !
    !----------------------------------------------------------------------c
    !     band (8400 - 14500 cm-1), nongray gaseous absorption of o2      c
    !     and o3. solar energy 8.9036 w m-2                               c
    !----------------------------------------------------------------------c
    !
    if (ig == 1) then
      init = 2
      call tline1z (taug, cs2h2ogh, q, dp, dip, dt, inpt, &
                    lev1, gh, ntl, init, il1, il2, ilg, lay)
    else
      im =  ig - 1
      init = 2
      coeff  => cs2o2gh(:, :, im)
      call tline1z (taug, coeff, o2, dp, dip, dt, inpt, &
                    lev1, gh, ntl, init, il1, il2, ilg, lay)
    end if
    !
    do k = 1, lay
      kp1 = k + 1
      do i = il1, il2
        tau               =  taug(i,k) + taua(i,k)
        dtr1              =  exp( - (tau - tauoma(i,k)) / rmu(i))
        tran(i,1,kp1)     =  tran(i,1,k) * dtr1
        !
        if (cld(i,k) < cut) then
          tran(i,2,kp1)   =  tran(i,2,k) * dtr1
        else
          absc            = (1.0 - cld(i,k)) * dtr1 + cld(i,k) * &
                            exp( - (tau + taucs(i,k) - tauomc(i,k)) &
                            / rmu(i))
          tran(i,2,kp1)   =  tran(i,2,k) * absc
        end if
      end do ! loop 200
    end do ! loop 205
    !
    gwgh =  gws2gh(ig)
    !
  else if (ib == 3) then
    !
    !----------------------------------------------------------------------c
    !     band (4200 - 8400 cm-1), nongray gaseous absorption of h2o and  c
    !     co2. solar energy 7.4453 w m-2                                  c
    !----------------------------------------------------------------------c
    !
    if (ig <= 2) then
      coeff  => cs3h2ogh(:, :, ig)
      coeff2 => cs3co2gh(:, :, ig)
      call tline2z (taug, coeff, coeff2, q, co2, dp, dip, dt, &
                    inpt, lev1, gh, ntl, il1, il2, ilg, lay)

    else
      init = 2
      coeff  => cs3co2gh(:, :, ig)
      call tline1z (taug, coeff, co2, dp, dip, dt, &
                    inpt, lev1, gh, ntl, init, il1, il2, ilg, lay)
    end if
    !
    do k = 1, lay
      kp1 = k + 1
      do i = il1, il2
        tau               =  taug(i,k) + taua(i,k)
        dtr1              =  exp( - (tau - tauoma(i,k)) / rmu(i))
        tran(i,1,kp1)     =  tran(i,1,k) * dtr1
        !
        if (cld(i,k) < cut) then
          tran(i,2,kp1)   =  tran(i,2,k) * dtr1
        else
          absc            = (1.0 - cld(i,k)) * dtr1 + cld(i,k) * &
                            exp( - (tau + taucs(i,k) - tauomc(i,k)) &
                            / rmu(i))
          tran(i,2,kp1)   =  tran(i,2,k) * absc
        end if
      end do ! loop 300
    end do ! loop 305
    !
    gwgh =  gws3gh(ig)
    !
  else if (ib == 4) then
    !
    !----------------------------------------------------------------------c
    !     band (2500 - 4200 cm-1), nongray gaseous absorption of h2o      c
    !     and co2. solar energy 7.0384 w m-2                              c
    !----------------------------------------------------------------------c
    !
    if (ig <= 3) then
      coeff  => cs4h2ogh(:, :, ig)
      coeff2 => cs4co2gh(:, :, ig)
      call tline2z (taug, coeff, coeff2, q, co2, dp, dip, dt, &
                    inpt, lev1, gh, ntl, il1, il2, ilg, lay)
    else if (ig == 6 .or. ig == 8) then
      if (ig == 6)  im = 5
      if (ig == 8)  im = 6
      init = 2
      coeff  => cs4h2ogh(:, :, im)
      call tline1z (taug, coeff, q, dp, dip, dt, inpt, &
                    lev1, gh, ntl, init, il1, il2, ilg, lay)
      !
    else if (ig == 5 .or. ig == 7 .or. ig == 9) then
      if (ig == 5)  im = 4
      if (ig == 7)  im = 5
      if (ig == 9)  im = 6
      init = 2
      coeff  => cs4co2gh(:, :, im)
      call tline1z (taug, coeff, co2, dp, dip, dt, &
                    inpt, lev1, gh, ntl, init, il1, il2, ilg, lay)
    else
      im = 4
      coeff  => cs4h2ogh(:, :, im)
      call tline2z (taug, coeff, cs4ch4gh, q, ch4, dp, dip, dt, &
                    inpt, lev1, gh, ntl, il1, il2, ilg, lay)
    end if
    !
    do k = 1, lay
      kp1 = k + 1
      do i = il1, il2
        tau               =  taug(i,k) + taua(i,k)
        dtr1              =  exp( - (tau - tauoma(i,k)) / rmu(i))
        tran(i,1,kp1)     =  tran(i,1,k) * dtr1
        !
        if (cld(i,k) < cut) then
          tran(i,2,kp1)   =  tran(i,2,k) * dtr1
        else
          absc            = (1.0 - cld(i,k)) * dtr1 + cld(i,k) * &
                            exp( - (tau + taucs(i,k) - tauomc(i,k)) &
                            / rmu(i))
          tran(i,2,kp1)   =  tran(i,2,k) * absc
        end if
      end do ! loop 400
    end do ! loop 405
    !
    gwgh =  gws4gh(ig)
    !
  end if
  do k = 1, lev
    do m = 1, ntile
      do i = il1, il2
        if (itile(i,m) > 0) then
          trant(i,m,1,k) = tran(i,1,k)
          trant(i,m,2,k) = tran(i,2,k)
        end if
      end do ! i
    end do ! m
  end do ! k
  !
  return
end subroutine strandngh4
!> \file
!>  Calculation of the downward solar flux under the condition that the
!! extinction coefficient of gas is very large (GH condition), the
!! scattering effects can be neglected, thus no upward flux. Since
!! the cloud optical depth is much smaller than the gaseous optical depth,
!! the cloud effect is very small and be treated simply by beer's law
