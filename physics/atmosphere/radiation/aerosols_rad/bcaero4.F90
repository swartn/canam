!> \file
!>\brief Calculation of black carbon aerosol optical property for bulk aerosol parameterization
!!
!! @author Jiangnan Li
!
subroutine bcaero4 (exta, exoma, exomga, fa, absa, &
                    exta055, oma055, ga055, &
                    extb, odfb, ssab, absb, extb055, odf055, &
                    ssa055, abs055, bcoload, bcyload, cld, wcdw, &
                    il1, il2, ilg, lay)
  !
  !     * jun 14,2013 - j. li.     new version for gcm17+:
  !     *                          - add semi-direct effect inside cloud
  !     *                            (requires passing and using cld and wcdw).
  !     * apr 23,2012 - j. li & y. peng. previous version bcaero3 for gcm16:
  !     *                                - add optical property at 0.55 um
  !     *                                  and recalculte solar band
  !     *                                  result, in the previous version
  !     *                                  the acc and coa are set opposite.
  !     * feb 09,2009 - m.lazare. previous version bcaero2 for gcm15h/i:
  !     *                         - remove unused code related to rh effect.
  !     * sep/132007 - x. ma/    previous version bcaero for gcm15f:
  !     *               j. li.    - seperate the code for bc and oc
  !     *                           individually.
  !     *                         - modification to match more with
  !     *                           other part of aerosol and simplify
  !     *                           the code.
  !     * june 08,2004 - dominique baeumer: original coding based on:
  !                      d. baeumer, et al. j. geophys, res. 2007 d10207
  !
  !----------------------------------------------------------------------c
  !     calculation of optical properties for bc (50% acc, 50% cm)c
  !     this fraction can be changed if more info is provided !           c
  !                                                                      c
  !     exta:      extinction coefficient                                c
  !     exoma:     extinction coefficient times single scattering albedo c
  !     exomga:    exoma times asymmetry factor                          c
  !     fa:        square of asymmetry factor                            c
  !     absa:      absorption coefficient                                c
  !     bcload:    bc (total) aerosol loading for each layer             c
  !     bcoload:   bco (hydrophobic) aerosol loading for each layer      c
  !     bcyload:   bcy (hydrophylic) aerosol loading for each layer      c
  !                                                                      c
  !     bctldacc:  scaled bcload for accum mode                          c
  !     bctldcoa:  scaled bcload for coarse mode                         c
  !                                                                      c
  !     sphobc:    solar optical data for hydrophobic bc,                c
  !                (nbs,6), index nbs for solar band, 6: 1, 2, 3 for ext,c
  !                single scattering albedo (omega), asymmetry factor (g)c
  !                of accum mode, 4, 5, 6 for coarse mode                c
  !     iphobc:    infrared optical data for hydrophobic bc,             c
  !                (nblm1,2), index nbl for infrared band, 2: 1 for accumc
  !                2 for corase                                          c
  !                                                                      c
  !     fbcacc: fraction of bc in accum mode                             c
  !----------------------------------------------------------------------c
  !
  use psizes_19, only : nbs, nbl
  use phys_consts, only : actfrc
  implicit none
  !
  integer, parameter :: nblm1 = nbl - 1

  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  !
  !     * output arrays.
  !
  real, intent(inout), dimension(ilg,lay,nbs) :: exta !< Extinction coefficient at 0.55 \f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: exoma !< EXTA*single scattering albedo \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: exomga !< EXOMA*asymmetry factor \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: fa !< EXOMGA*asymmetry factor \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbl) :: absa !< Longwave absorption coefficient \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: absb !< Absorption coefficient \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: odfb !< Extinction coefficient per mass for accumulation mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: extb !< Extinction coefficient per mass at 0.55\f$\mu\f$m for accumulation+coarse mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: ssab !< Single scattering albedo \f$[1]\f$
  real, intent(inout), dimension(ilg,lay) :: exta055 !< Extinction coefficient at 0.55 \f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: oma055 !< EXTA055 * single scattering albedo at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: ga055 !< OMA055 * asymmetry factor at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: extb055 !< Extinction coefficient per mass at 0.55\f$\mu\f$m for accumulation+coarse mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: odf055 !< Extinction coefficient per mass at 0.55\f$\mu\f$m for accumulation mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: ssa055 !< Single scattering albedo at 0.55\f$\mu\f$m \f$[1]\f$
  real, intent(inout), dimension(ilg,lay) :: abs055 !< Absorption coefficient at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  !
  !     * input arrays.
  !
  real, intent(in), dimension(ilg,lay) :: bcoload!< Hydrophobic black carbon aerosol loading \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: bcyload!< Hydrophilic black carbon aerosol loading \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: cld !< Cloud fraction \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: wcdw !< Cloud liquid water content \f$[gram/m^3]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * internal work arrays.
  !
  real, dimension(ilg,lay) :: bcload
  real, dimension(ilg,lay) :: bctldacc
  real, dimension(ilg,lay) :: bctldcoa

  real :: abs1
  real :: abs2
  real :: efcld
  real :: exom
  real :: exomg
  real :: exomgg
  real :: ext
  real :: ext0551
  real :: ext0552
  real :: ext1
  real :: ext2
  real :: ga0551
  real :: ga0552
  integer :: i
  integer :: j
  integer :: k
  real :: om0551
  real :: om0552
  real :: ssa1
  real :: ssa2
  real :: tmass
  real :: x
  real :: y
  real :: z
  !
  real, parameter :: fbcacc = 1.0
  real, parameter :: fbccoa = 1.0 - fbcacc
  !  *  hydrophobic data  *
  !
  !  *  corrected data by jiangnan
  !
  real, parameter, dimension(nbs,6) :: sphobc = reshape( &
                        [11.309423, 5.300023, 2.691659, 1.354601, &
                          0.227982, 0.121912, 0.048024, 0.009951, &
                          0.357685, 0.252115, 0.160571, 0.076695, &
                          8.700115, 6.658090, 4.179159, 2.025891, &
                          0.444720, 0.401069, 0.327568, 0.208560, &
                          0.676934, 0.574040, 0.459321, 0.315548], [nbs, 6])
  !
  real, parameter, dimension(nblm1,2) :: iphobc = reshape( &
                [1.03056, .87149, .68762, .48330, .40322, .33417, .23639, .14938, &
                  .87573, .74959, .60427, .43853, .37152, .31304, .22652, .14601], [nblm1, 2])
  !
  real, parameter, dimension(2) :: se055 = [9.4789, 8.4650]
  !
  real, parameter, dimension(2) :: sw055 = [0.2093, 0.4390]
  !
  real, parameter, dimension(2) :: g055 = [0.3348, 0.6577]
  !
  !----------------------------------------------------------------------c
  !     10000. is the unit scaled discussed in other aerosol routines    c
  !     for 0.55 um the accumulation mode using sigma = 2, geometry      c
  !     radius r0 = 0.0118 um (reff = 0.03922), coarse mode sigma = 2,   c
  !     r0 = 0.06017 (reff = 0.2 um), accumulation mode size follows     c
  !     hess etc bul of amer meteor soc 1998                             c
  !----------------------------------------------------------------------c
  !
  do k = 1, lay
    do i = il1, il2
      bcload(i,k)    =  bcoload(i,k) + bcyload(i,k)
      x              =  10000.0 * bcload(i,k)
      !
      !----------------------------------------------------------------------c
      !     actfrc% bc is considered in the internal mixing of cloud,        c
      !     1-actfrc is treated as external mixing inside cloud,             c
      !     1.0 - actfrc * cld = (1.0 - actfrc) * cld + 1.0 - cld            c
      !----------------------------------------------------------------------c
      !
      if (cld(i,k) > 0.01 .and. wcdw(i,k) > 0.001) then
        y            =  10000.0 * bcyload(i,k)
        z            =  10000.0 * bcoload(i,k)
        efcld        =  1.0 - actfrc * cld(i,k)
        bctldacc(i,k) =  efcld * fbcacc * y + fbcacc * z
        bctldcoa(i,k) =  efcld * fbccoa * y + fbccoa * z
      else
        bctldacc(i,k) =  fbcacc * x
        bctldcoa(i,k) =  fbccoa * x
      end if
    end do
  end do ! loop 100
  !
  !----------------------------------------------------------------------c
  !     solar                                                            c
  !     for bc the hydrophylic effect on optical property is too small   c
  !     not accounted for both solar and infrared                        c
  !----------------------------------------------------------------------c
  !
  do j = 1, nbs
    do k = 1, lay
      do i = il1, il2
        !
        ! --- bc accumulation mode ---
        !
        ext            =  bctldacc(i,k) * sphobc(j,1)
        exta(i,k,j)    =  exta(i,k,j) + ext
        exom           =  ext * sphobc(j,2)
        exoma(i,k,j)   =  exoma(i,k,j) + exom
        exomg          =  exom * sphobc(j,3)
        exomga(i,k,j)  =  exomga(i,k,j) + exomg
        exomgg         =  exomg * sphobc(j,3)
        fa(i,k,j)      =  fa(i,k,j) + exomgg
        !
        abs1           =  ext * (1.0 - sphobc(j,2))
        ext1           =  ext
        ssa1           =  exom
        !
        ! --- bc coarse mode ---
        !
        ext            =  bctldcoa(i,k) * sphobc(j,4)
        exta(i,k,j)    =  exta(i,k,j) + ext
        exom           =  ext * sphobc(j,5)
        exoma(i,k,j)   =  exoma(i,k,j) + exom
        exomg          =  exom * sphobc(j,6)
        exomga(i,k,j)  =  exomga(i,k,j) + exomg
        exomgg         =  exomg * sphobc(j,6)
        fa(i,k,j)      =  fa(i,k,j) + exomgg
        !
        abs2           =  ext * (1.0 - sphobc(j,5))
        absb(i,k,j)    =  abs1 + abs2
        ext2           =  ext
        ssa2           =  exom
        !
        tmass          =  10000.0 * bcload(i,k)
        if (tmass > 1.e-15) then
          extb(i,k,j)   = (ext1 + ext2)/ tmass
          odfb(i,k,j)   =  ext1 / bctldacc(i,k)
        else
          extb(i,k,j)   =  0.0
          odfb(i,k,j)   =  0.0
        end if
        !
        if ((ext1 + ext2) > 1.e-20) then
          ssab(i,k,j)   = (ssa1 + ssa2)/(ext1 + ext2)
        else
          ssab(i,k,j)   =  0.0
        end if
        !
      end do
    end do
  end do ! loop 200
  !
  do k = 1, lay
    do i = il1, il2
      ext0551        =  bctldacc(i,k) * se055(1)
      ext0552        =  bctldcoa(i,k) * se055(2)
      exta055(i,k)   =  exta055(i,k) + ext0551 + ext0552
      om0551         =  ext0551 * sw055(1)
      om0552         =  ext0552 * sw055(2)
      oma055(i,k)    =  oma055(i,k) + om0551 + om0552
      ga0551         =  om0551 * g055(1) * g055(1)
      ga0552         =  om0552 * g055(2) * g055(2)
      ga055(i,k)     =  ga055(i,k) + ga0551 + ga0552
      !
      abs055(i,k)    =  ext0551 * (1.0 - sw055(1)) + &
                       ext0552 * (1.0 - sw055(2))
      tmass          =  10000.0 * bcload(i,k)
      if (tmass > 1.e-15) then
        extb055(i,k)  = (ext0551 + ext0552) / tmass
        odf055(i,k)   =  ext0551 / bctldacc(i,k)
      else
        extb055(i,k)  =  0.0
        odf055(i,k)   =  0.0
      end if
      !
      if ((ext0551 + ext0552) > 1.e-20) then
        ssa055(i,k)   = (om0551 + om0552) / (ext0551 + ext0552)
      else
        ssa055(i,k)   =  0.0
      end if
      !
    end do
  end do ! loop 250
  !
  !----------------------------------------------------------------------c
  !     infrared                                                         c
  !----------------------------------------------------------------------c
  !
  do j  = 1, nblm1
    do k  = 1, lay
      do i  = il1, il2
        absa(i,k,j)  =  absa(i,k,j) + &
                       bctldacc(i,k) * iphobc(j,1) + &
                       bctldcoa(i,k) * iphobc(j,2)
      end do
    end do
  end do ! loop 300
  !
  return
end subroutine bcaero4
!> \file
!> Compute optical properties for black carbon assuming that it is 50% hydrophylic
!! and 50% hydrophobic.
!!\n
!! The effect of black carbon embedded in liquid cloud water drops are also computed
!! allowing the inclusion of the semi-indirect effect.
