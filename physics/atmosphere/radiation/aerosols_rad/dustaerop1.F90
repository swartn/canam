!> \file dustaerop.F90
!>\brief Calculate dust aerosol optical properties for PAM aerosol code
!!
!! @author Jiangnan Li
!
subroutine dustaerop1(exta, exoma, exomga, fa, absa, exta055, &
                      exta086, ssa055, aload, re, ve,         &
                      il1, il2, ilg, lay)
  !
  !     Nov   2021 j. li           extension of size range
  !     march 2012 j. li & y.peng. pla version based on the new
  !                                dust refractive index.
  !                                data from acp 2011.
  !
  use rdmod, only : nbs, nbl
  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  !
  real, dimension(ilg,lay,nbs), intent(inout) :: exta  !< Extinction coefficient \f$[m^2/gram]\f$
  real, dimension(ilg,lay,nbs), intent(inout) :: exoma !< EXTA*single scattering albedo \f$[m^2/gram]\f$
  real, dimension(ilg,lay,nbs), intent(inout) :: exomga!< EXOMA*asymetry factor \f$[m^2/gram]\f$
  real, dimension(ilg,lay,nbs), intent(inout) :: fa    !< EXOMGA*asymetry factor \f$[m^2/gram]\f$
  real, dimension(ilg,lay,nbl), intent(inout) :: absa  !< Longwave absorption coefficient \f$[m^2/gram]\f$
  real, dimension(ilg,lay), intent(out)   :: exta055   !< Extinction coefficient at 0.55 \f$[m^2/gram]\f$
  real, dimension(ilg,lay), intent(out)   :: ssa055    !< Single scattering albedo at 0.55 \f$[unitless]\f$
  real, dimension(ilg,lay), intent(out)   :: exta086   !< Extinction coefficient at 0.86 \f$[m^2/gram]\f$
  real, dimension(ilg,lay), intent(in)    :: aload     !< Aerosol loading of dust \f$[gram/gram]\f$
  real, dimension(ilg,lay), intent(inout) :: re        !< Effective radius \f$[micro meter]\f$
  real, dimension(ilg,lay), intent(inout) :: ve        !< Effective variance of dust \f$[unitless]\f$
  !
  real :: cg1
  real :: cg2
  real :: cg3
  real :: exom
  real :: exomg
  real :: ext
  real :: ext1
  real :: ext2
  real :: ext3
  real :: exts
  real :: g
  integer :: i
  integer :: j
  integer :: k
  integer :: l
  integer :: lp1
  real :: om
  real :: om1
  real :: om2
  real :: om3
  real :: r1
  real :: r2
  real :: r3
  real :: sab
  real :: sab1
  real :: sab2
  real :: sab3
  real, dimension(ilg,lay) :: g055      !< asymetry factor \f$[unitless]\f$
  !
  !
  !     calculation of optical properties for dust in continous mode (PLA)
  !     For effective radious, there are 10 standard calculated results,
  !     the linera interpolation is used for any value between two standard
  !     values. For effective variance the 3-point Lagrangian interoplation
  !     method is used
  !     DELTA is the interval of the linear interpolation
  !     FR1, FR2, FR3 are for  3-point Lagrangian interoplation
  !
  !
  real, dimension(ilg,lay) :: delta
  real, dimension(ilg,lay) :: fr1
  real, dimension(ilg,lay) :: fr2
  real, dimension(ilg,lay) :: fr3
  real, dimension(ilg,lay) :: sload
  integer, dimension(ilg,lay) :: re_num
  !
  real, dimension(nbs,10), parameter :: de1 = reshape( &
        [1.7078E+00, 2.9645E-01, 4.4267E-02, 4.7304E-03,  &
         2.1663E+00, 1.9970E+00, 9.0834E-01, 1.5393E-01,  &
         9.4627E-01, 1.2240E+00, 1.1398E+00, 4.1397E-01,  &
         6.2675E-01, 7.0090E-01, 8.8842E-01, 5.6762E-01,  &
         4.7060E-01, 4.9593E-01, 6.2552E-01, 5.9515E-01,  &
         3.7453E-01, 3.9365E-01, 4.5603E-01, 5.4794E-01,  &
         3.1062E-01, 3.2670E-01, 3.5677E-01, 4.7169E-01,  &
         2.6524E-01, 2.7813E-01, 2.9588E-01, 3.9394E-01,  &
         2.3133E-01, 2.4159E-01, 2.5478E-01, 3.2705E-01,  &
         2.0504E-01, 2.1336E-01, 2.2447E-01, 2.7419E-01], [nbs, 10])

  real, dimension(nbs,10), parameter :: de2 = reshape( &
        [2.1813E+00, 6.5447E-01, 1.5135E-01, 1.7596E-02,  &
         1.8530E+00, 1.4894E+00, 8.3795E-01, 2.5022E-01,  &
         1.0268E+00, 1.0612E+00, 8.5331E-01, 4.1099E-01,  &
         6.6984E-01, 7.3779E-01, 7.0841E-01, 4.5710E-01,  &
         4.9108E-01, 5.4347E-01, 5.6940E-01, 4.4559E-01,  &
         3.8701E-01, 4.2369E-01, 4.6165E-01, 4.1159E-01,  &
         3.1952E-01, 3.4544E-01, 3.8171E-01, 3.7174E-01,  &
         2.7239E-01, 2.9136E-01, 3.2242E-01, 3.3314E-01,  &
         2.3772E-01, 2.5217E-01, 2.7786E-01, 2.9844E-01,  &
         2.1124E-01, 2.2267E-01, 2.4375E-01, 2.6830E-01], [nbs, 10])

  real, dimension(nbs,10), parameter :: de3 = reshape( &
        [2.1356E+00, 9.8817E-01, 3.8545E-01, 9.2472E-02,  &
         1.3958E+00, 1.0516E+00, 6.6599E-01, 2.9651E-01,  &
         9.1798E-01, 8.0399E-01, 6.0619E-01, 3.4199E-01,  &
         6.6811E-01, 6.3022E-01, 5.2405E-01, 3.4039E-01,  &
         5.2191E-01, 5.1342E-01, 4.5470E-01, 3.2523E-01,  &
         4.2832E-01, 4.3219E-01, 3.9975E-01, 3.0690E-01,  &
         3.6427E-01, 3.7344E-01, 3.5636E-01, 2.8886E-01,  &
         3.1813E-01, 3.2946E-01, 3.2172E-01, 2.7222E-01,  &
         2.8355E-01, 2.9555E-01, 2.9368E-01, 2.5724E-01,  &
         2.5680E-01, 2.6876E-01, 2.7063E-01, 2.4389E-01], [nbs, 10])
  !
  real, dimension(nbs,10), parameter :: dw1 = reshape( &
        [9.9506E-01, 9.8825E-01, 9.2074E-01, 5.7725E-01,  &
         9.9433E-01, 9.9666E-01, 9.9270E-01, 9.8113E-01,  &
         9.8713E-01, 9.9408E-01, 9.9302E-01, 9.9036E-01,  &
         9.8165E-01, 9.8944E-01, 9.9051E-01, 9.9189E-01,  &
         9.7705E-01, 9.8517E-01, 9.8620E-01, 9.9173E-01,  &
         9.7269E-01, 9.8175E-01, 9.8096E-01, 9.9067E-01,  &
         9.6849E-01, 9.7875E-01, 9.7579E-01, 9.8891E-01,  &
         9.6443E-01, 9.7595E-01, 9.7122E-01, 9.8654E-01,  &
         9.6046E-01, 9.7330E-01, 9.6724E-01, 9.8366E-01,  &
         9.5657E-01, 9.7074E-01, 9.6368E-01, 9.8045E-01], [nbs, 10])
 !
  real, dimension(nbs,10), parameter :: dw2 = reshape( &
        [9.9574E-01, 9.9374E-01, 9.7398E-01, 8.8051E-01,  &
         9.9358E-01, 9.9555E-01, 9.9184E-01, 9.8681E-01,  &
         9.8877E-01, 9.9333E-01, 9.9077E-01, 9.8991E-01,  &
         9.8370E-01, 9.9041E-01, 9.8840E-01, 9.8991E-01,  &
         9.7889E-01, 9.8725E-01, 9.8544E-01, 9.8907E-01,  &
         9.7441E-01, 9.8408E-01, 9.8215E-01, 9.8782E-01,  &
         9.7020E-01, 9.8103E-01, 9.7871E-01, 9.8631E-01,  &
         9.6621E-01, 9.7813E-01, 9.7523E-01, 9.8463E-01,  &
         9.6239E-01, 9.7537E-01, 9.7179E-01, 9.8282E-01,  &
         9.5875E-01, 9.7276E-01, 9.6845E-01, 9.8094E-01], [nbs, 10])
 !
  real, dimension(nbs,10), parameter :: dw3 = reshape( &
        [9.9521E-01, 9.9493E-01, 9.8717E-01, 9.7288E-01,  &
         9.9208E-01, 9.9383E-01, 9.8937E-01, 9.8726E-01,  &
         9.8849E-01, 9.9177E-01, 9.8747E-01, 9.8736E-01,  &
         9.8490E-01, 9.8963E-01, 9.8528E-01, 9.8653E-01,  &
         9.8142E-01, 9.8751E-01, 9.8306E-01, 9.8549E-01,  &
         9.7809E-01, 9.8545E-01, 9.8088E-01, 9.8438E-01,  &
         9.7493E-01, 9.8347E-01, 9.7876E-01, 9.8327E-01,  &
         9.7194E-01, 9.8156E-01, 9.7671E-01, 9.8218E-01,  &
         9.6913E-01, 9.7975E-01, 9.7475E-01, 9.8111E-01,  &
         9.6649E-01, 9.7802E-01, 9.7287E-01, 9.8008E-01], [nbs, 10])
  !
  real, dimension(nbs,10), parameter :: dg1 = reshape( &
        [5.1472E-01, 2.4384E-01, 9.4797E-02, 2.4419E-02,  &
         6.8697E-01, 7.0429E-01, 6.1844E-01, 3.5016E-01,  &
         6.6466E-01, 6.7996E-01, 7.0267E-01, 5.8721E-01,  &
         7.1616E-01, 6.4778E-01, 6.9658E-01, 6.6991E-01,  &
         7.4600E-01, 6.7190E-01, 6.7019E-01, 7.0049E-01,  &
         7.6117E-01, 7.0619E-01, 6.5476E-01, 7.0662E-01,  &
         7.7132E-01, 7.2966E-01, 6.5887E-01, 6.9933E-01,  &
         7.7931E-01, 7.4418E-01, 6.7517E-01, 6.8554E-01,  &
         7.8595E-01, 7.5393E-01, 6.9449E-01, 6.7060E-01,  &
         7.9157E-01, 7.6133E-01, 7.1201E-01, 6.5865E-01], [nbs, 10])

  real, dimension(nbs,10), parameter :: dg2 = reshape( &
        [6.0595E-01, 4.9594E-01, 3.6805E-01, 1.9814E-01,  &
         6.8689E-01, 6.7670E-01, 6.4004E-01, 5.4232E-01,  &
         6.9026E-01, 6.8405E-01, 6.7594E-01, 6.2828E-01,  &
         7.0431E-01, 6.8436E-01, 6.8279E-01, 6.6035E-01,  &
         7.2160E-01, 6.8921E-01, 6.8386E-01, 6.7408E-01,  &
         7.3735E-01, 6.9785E-01, 6.8484E-01, 6.8006E-01,  &
         7.5042E-01, 7.0817E-01, 6.8721E-01, 6.8249E-01,  &
         7.6104E-01, 7.1864E-01, 6.9110E-01, 6.8335E-01,  &
         7.6971E-01, 7.2847E-01, 6.9618E-01, 6.8365E-01,  &
         7.7690E-01, 7.3735E-01, 7.0203E-01, 6.8392E-01], [nbs, 10])

  real, dimension(nbs,10), parameter :: dg3 = reshape( &
        [6.3941E-01, 6.0128E-01, 5.5865E-01, 4.9110E-01,  &
         6.8205E-01, 6.6597E-01, 6.4717E-01, 6.1351E-01,  &
         6.9429E-01, 6.7934E-01, 6.6588E-01, 6.4223E-01,  &
         7.0397E-01, 6.8705E-01, 6.7495E-01, 6.5574E-01,  &
         7.1276E-01, 6.9325E-01, 6.8093E-01, 6.6377E-01,  &
         7.2084E-01, 6.9882E-01, 6.8560E-01, 6.6921E-01,  &
         7.2826E-01, 7.0402E-01, 6.8960E-01, 6.7322E-01,  &
         7.3502E-01, 7.0892E-01, 6.9320E-01, 6.7638E-01,  &
         7.4117E-01, 7.1355E-01, 6.9654E-01, 6.7899E-01,  &
         7.4678E-01, 7.1792E-01, 6.9968E-01, 6.8122E-01], [nbs, 10])
  !
  real, dimension(10), parameter :: de551 = &
        [1.3197e+00, 2.2684e+00, 9.3475e-01, 6.3088e-01, 4.7242e-01, &
        3.7557e-01, 3.1148e-01, 2.6588e-01, 2.3182e-01, 2.0541e-01]
  real, dimension(10), parameter :: de552 = &
        [1.7683E+00, 1.8732E+00, 1.0406E+00, 6.7549E-01, 4.9363E-01, &
         3.8850E-01, 3.2024E-01, 2.7232E-01, 2.3684E-01, 2.0950E-01]
  real, dimension(10), parameter :: de553 = &
        [1.8467E+00, 1.4681E+00, 9.7402E-01, 7.0282E-01, 5.4030E-01, &
         4.3482E-01, 3.6171E-01, 3.0849E-01, 2.6829E-01, 2.3699E-01]
  real, dimension(10), parameter :: dw551 = &
        [9.9608e-01, 9.9633e-01, 9.9078e-01, 9.8670e-01, 9.8337e-01, &
         9.8008e-01, 9.7696e-01, 9.7406e-01, 9.7115e-01, 9.6834e-01]
  real, dimension(10), parameter :: dw552 = &
        [9.9677E-01, 9.9563E-01, 9.9212E-01, 9.8832E-01, 9.8480E-01, &
         9.8139E-01, 9.7816E-01, 9.7517E-01, 9.7223E-01, 9.6943E-01]
  real, dimension(10), parameter :: dw553 = &
        [9.9665E-01, 9.9466E-01, 9.9211E-01, 9.8947E-01, 9.8687E-01, &
         9.8418E-01, 9.8151E-01, 9.7892E-01, 9.7634E-01, 9.7382E-01]
  real, dimension(10), parameter :: dg551 = &
        [4.5019e-01, 6.9259e-01, 6.4883e-01, 7.1370e-01, 7.4380e-01, &
         7.5712e-01, 7.6686e-01, 7.7486e-01, 7.8153e-01, 7.8711e-01]
  real, dimension(10), parameter :: dg552 = &
        [5.8591E-01, 6.8611E-01, 6.8630E-01, 6.9893E-01, 7.1647E-01, &
         7.3239E-01, 7.4575E-01, 7.5653E-01, 7.6531E-01, 7.7249E-01]
  real, dimension(10), parameter :: dg553 = &
        [6.2765E-01, 6.7844E-01, 6.9068E-01, 7.0005E-01, 7.0875E-01, &
         7.1668E-01, 7.2425E-01, 7.3130E-01, 7.3778E-01, 7.4359E-01]
  real, dimension(10), parameter :: de861 = &
        [2.9844e-01, 2.1009e+00, 1.2151e+00, 6.7545e-01, 4.8964e-01, &
         3.9312e-01, 3.2583e-01, 2.7678e-01, 2.4039e-01, 2.1240e-01]
  real, dimension(10), parameter :: de862 = &
        [6.2731E-01, 1.5420E+00, 1.0755E+00, 7.3772E-01, 5.3991E-01, &
         4.1977E-01, 3.4176E-01, 2.8781E-01, 2.4853E-01, 2.1867E-01]
  real, dimension(10), parameter :: de863 = &
        [9.3095E-01, 1.1206E+00, 8.4712E-01, 6.5218E-01, 5.2025E-01, &
         4.2796E-01, 3.6090E-01, 3.1049E-01, 2.7152E-01, 2.4064E-01]
  real, dimension(10), parameter :: re_tab = &
        [0.1, 0.425, 0.75, 1.075, 1.4, 1.725, 2.05, 2.375, 2.7, 3.025]  !
  !
  real, dimension(nbl,10), parameter :: da1 = reshape( &
        [0.1297E-01, 0.1353E-01, 0.2631E-01, 0.3856E-01, 0.4885E-01, &
         0.3383E-01, 0.2337E-01, 0.3206E-01, 0.3749E-01, &
         0.3096E-01, 0.1463E-01, 0.2713E-01, 0.4038E-01, 0.5252E-01, &
         0.3555E-01, 0.2402E-01, 0.3257E-01, 0.3780E-01, &
         0.3262E-01, 0.1600E-01, 0.2812E-01, 0.4357E-01, 0.5997E-01, &
         0.3905E-01, 0.2537E-01, 0.3369E-01, 0.3849E-01, &
         0.3413E-01, 0.1712E-01, 0.2874E-01, 0.4698E-01, 0.6960E-01, &
         0.4358E-01, 0.2721E-01, 0.3535E-01, 0.3952E-01, &
         0.3542E-01, 0.1800E-01, 0.2908E-01, 0.4997E-01, 0.7952E-01, &
         0.4847E-01, 0.2929E-01, 0.3748E-01, 0.4085E-01, &
         0.3650E-01, 0.1859E-01, 0.2923E-01, 0.5216E-01, 0.8755E-01, &
         0.5316E-01, 0.3144E-01, 0.3994E-01, 0.4242E-01, &
         0.3746E-01, 0.1895E-01, 0.2923E-01, 0.5336E-01, 0.9235E-01, &
         0.5706E-01, 0.3353E-01, 0.4260E-01, 0.4417E-01, &
         0.3840E-01, 0.1914E-01, 0.2910E-01, 0.5366E-01, 0.9404E-01, &
         0.5980E-01, 0.3544E-01, 0.4533E-01, 0.4600E-01, &
         0.3936E-01, 0.1923E-01, 0.2886E-01, 0.5331E-01, 0.9346E-01, &
         0.6133E-01, 0.3709E-01, 0.4798E-01, 0.4783E-01, &
         0.4034E-01, 0.1924E-01, 0.2856E-01, 0.5253E-01, 0.9148E-01, &
         0.6188E-01, 0.3839E-01, 0.5040E-01, 0.4957E-01], [nbl, 10])
 !
  real, dimension(nbl,10), parameter :: da2 = reshape( &
        [0.1310E-01, 0.1363E-01, 0.2638E-01, 0.3872E-01, 0.4916E-01, &
         0.3398E-01, 0.2343E-01, 0.3210E-01, 0.3752E-01, &
         0.3159E-01, 0.1520E-01, 0.2749E-01, 0.4197E-01, 0.5671E-01, &
         0.3756E-01, 0.2482E-01, 0.3329E-01, 0.3825E-01, &
         0.3335E-01, 0.1655E-01, 0.2826E-01, 0.4559E-01, 0.6686E-01, &
         0.4279E-01, 0.2710E-01, 0.3560E-01, 0.3971E-01, &
         0.3490E-01, 0.1745E-01, 0.2860E-01, 0.4811E-01, 0.7483E-01, &
         0.4752E-01, 0.2947E-01, 0.3836E-01, 0.4153E-01, &
         0.3632E-01, 0.1802E-01, 0.2864E-01, 0.4949E-01, 0.7975E-01, &
         0.5108E-01, 0.3157E-01, 0.4108E-01, 0.4339E-01, &
         0.3761E-01, 0.1836E-01, 0.2850E-01, 0.4997E-01, 0.8209E-01, &
         0.5345E-01, 0.3329E-01, 0.4349E-01, 0.4508E-01, &
         0.3876E-01, 0.1854E-01, 0.2824E-01, 0.4982E-01, 0.8255E-01, &
         0.5482E-01, 0.3460E-01, 0.4547E-01, 0.4652E-01, &
         0.3973E-01, 0.1861E-01, 0.2791E-01, 0.4924E-01, 0.8172E-01, &
         0.5540E-01, 0.3555E-01, 0.4700E-01, 0.4765E-01, &
         0.4053E-01, 0.1860E-01, 0.2752E-01, 0.4839E-01, 0.8005E-01, &
         0.5540E-01, 0.3619E-01, 0.4809E-01, 0.4847E-01, &
         0.4115E-01, 0.1854E-01, 0.2710E-01, 0.4736E-01, 0.7787E-01, &
         0.5497E-01, 0.3657E-01, 0.4879E-01, 0.4901E-01], [nbl, 10])
  !                 / &
  real, dimension(nbl,10), parameter :: da3 = reshape( &
        [0.1336E-01, 0.1385E-01, 0.2654E-01, 0.3908E-01, 0.4988E-01, &
         0.3431E-01, 0.2355E-01, 0.3220E-01, 0.3758E-01, &
         0.3235E-01, 0.1590E-01, 0.2784E-01, 0.4416E-01, 0.6338E-01, &
         0.4114E-01, 0.2635E-01, 0.3485E-01, 0.3923E-01, &
         0.3419E-01, 0.1703E-01, 0.2820E-01, 0.4683E-01, 0.7170E-01, &
         0.4669E-01, 0.2954E-01, 0.3899E-01, 0.4199E-01, &
         0.3612E-01, 0.1759E-01, 0.2809E-01, 0.4750E-01, 0.7455E-01, &
         0.4952E-01, 0.3168E-01, 0.4196E-01, 0.4426E-01, &
         0.3786E-01, 0.1785E-01, 0.2773E-01, 0.4716E-01, 0.7465E-01, &
         0.5066E-01, 0.3297E-01, 0.4374E-01, 0.4556E-01, &
         0.3892E-01, 0.1793E-01, 0.2727E-01, 0.4631E-01, 0.7339E-01, &
         0.5079E-01, 0.3367E-01, 0.4467E-01, 0.4617E-01, &
         0.3951E-01, 0.1790E-01, 0.2675E-01, 0.4521E-01, 0.7145E-01, &
         0.5033E-01, 0.3396E-01, 0.4503E-01, 0.4630E-01, &
         0.3974E-01, 0.1780E-01, 0.2620E-01, 0.4399E-01, 0.6919E-01, &
         0.4952E-01, 0.3397E-01, 0.4498E-01, 0.4611E-01, &
         0.3973E-01, 0.1765E-01, 0.2564E-01, 0.4272E-01, 0.6680E-01, &
         0.4851E-01, 0.3379E-01, 0.4467E-01, 0.4570E-01, &
         0.3955E-01, 0.1747E-01, 0.2509E-01, 0.4146E-01, 0.6439E-01, &
         0.4739E-01, 0.3348E-01, 0.4417E-01, 0.4514E-01], [nbl, 10])
  !
  !----------------------------------------------------------------------
  !     solar, de55: specific extinction, omas: single scattering albedo,
  !     gas: asymmetry factor, extas: extinction coefficient
  !     10000, because the unit of specific extinction for aerosol
  !     is m^2/gram, while for gas is cm^2/gram, in raddriv we same
  !     dp (air density * layer thickness) is usd for both gas and
  !     aerosol. aload sea salt aerosol loading in unit g (aerosol) / g
  !     (air). tau = aload * specific extinction * air density *
  !     layer thickness
  !
  !     linear interpolation for reff and lagrange interpolation for
  !     veffc, data based on 10 reff points from 0.1 to 3.025 with
  !     interval 0.325, 3 veff points 0.1, 0.5 and 2.0, using Lagrangian
  !     interpolation
  !----------------------------------------------------------------------
  !
  do k = 1, lay
    do i = il1, il2
      re(i,k)         =  max( min (re(i,k), 3.025), 0.1)
      ve(i,k)         =  max( min (ve(i,k), 2.000), 0.1)
      r1              =  ve(i,k) - 0.1
      r2              =  ve(i,k) - 0.5
      r3              =  ve(i,k) - 2.
      fr1(i,k)        =  1.31578946 * r2 * r3
      fr2(i,k)        = -1.66666663 * r1 * r3
      fr3(i,k)        =  0.35087719 * r1 * r2
      !
      re_num(i,k)     =  min(int((10.0 * re(i,k) - 1.0) / 3.25 + 1.0), 10)
      l               =  re_num(i,k)
      delta(i,k)      = (re(i,k) - re_tab(l)) / 0.325
      sload(i,k)      =  10000.0 * aload(i,k)
    end do
  end do
  !
  ! Initialisation
  exta055(:, :) = 1.0e-20
  ssa055(:, :)  = 0.0
  exta086(:, :) = 0.0
  !
  do j = 1, nbs
    do k = 1, lay
      do i = il1, il2
        if (aload(i,k) >= 1.e-12) then
           l             =  re_num(i,k)
           lp1           =  l + 1
           if (delta(i,k) == 0.0) lp1 = l
           ext1          =  de1(j,l) + delta(i,k) * (de1(j,lp1) - de1(j,l))
           ext2          =  de2(j,l) + delta(i,k) * (de2(j,lp1) - de2(j,l))
           ext3          =  de3(j,l) + delta(i,k) * (de3(j,lp1) - de3(j,l))
           !
           om1           =  dw1(j,l) + delta(i,k) * (dw1(j,lp1) - dw1(j,l))
           om2           =  dw2(j,l) + delta(i,k) * (dw2(j,lp1) - dw2(j,l))
           om3           =  dw3(j,l) + delta(i,k) * (dw3(j,lp1) - dw3(j,l))
           !
           cg1           =  dg1(j,l) + delta(i,k) * (dg1(j,lp1) - dg1(j,l))
           cg2           =  dg2(j,l) + delta(i,k) * (dg2(j,lp1) - dg2(j,l))
           cg3           =  dg3(j,l) + delta(i,k) * (dg3(j,lp1) - dg3(j,l))
           !
           ext           =  ext1 * fr1(i,k) + ext2 * fr2(i,k) + ext3 * fr3(i,k)
           om            =  om1 * fr1(i,k) + om2 * fr2(i,k) + om3 * fr3(i,k)
           g             =  cg1 * fr1(i,k) + cg2 * fr2(i,k) + cg3 * fr3(i,k)
           !
           exts          =  ext * sload(i,k)
           exom          =  exts * om
           exomg         =  exom * g
           !
           exta(i,k,j)   =  exta(i,k,j) + exts
           exoma(i,k,j)  =  exoma(i,k,j) + exom
           exomga(i,k,j) =  exomga(i,k,j) + exomg
           fa(i,k,j)     =  fa(i,k,j) + exomg * g
           !
           if (j == 1) then
             !
             !     calculation of 0.55 um
             !
             ext1        =  de551(l) + delta(i,k) * (de551(lp1) - de551(l))
             ext2        =  de552(l) + delta(i,k) * (de552(lp1) - de552(l))
             ext3        =  de553(l) + delta(i,k) * (de553(lp1) - de553(l))
             !
             om1         =  dw551(l) + delta(i,k) * (dw551(lp1) - dw551(l))
             om2         =  dw552(l) + delta(i,k) * (dw552(lp1) - dw552(l))
             om3         =  dw553(l) + delta(i,k) * (dw553(lp1) - dw553(l))
             !
             cg1         =  dg551(l) + delta(i,k) * (dg551(lp1) - dg551(l))
             cg2         =  dg552(l) + delta(i,k) * (dg552(lp1) - dg552(l))
             cg3         =  dg553(l) + delta(i,k) * (dg553(lp1) - dg553(l))
             !
             exta055(i,k)=  ext1 * fr1(i,k) + ext2 * fr2(i,k) + ext3 * fr3(i,k)
             ssa055(i,k) =  om1  * fr1(i,k) + om2  * fr2(i,k) + om3  * fr3(i,k)
             g055(i,k)   =  cg1  * fr1(i,k) + cg2  * fr2(i,k) + cg3  * fr3(i,k)
             !
             !     calculation of 0.865 um
             !     angstrom = lambda1/lambda2 log (aod_2/aod_1), 1: 0.55, 2: 0.865
             !
             ext1        =  de861(l) + delta(i,k) * (de861(lp1) - de861(l))
             ext2        =  de862(l) + delta(i,k) * (de862(lp1) - de862(l))
             ext3        =  de863(l) + delta(i,k) * (de863(lp1) - de863(l))
             !
             exta086(i,k)=  ext1 * fr1(i,k) + ext2 * fr2(i,k) + ext3 * fr3(i,k)
           end if
        end if
      end do
    end do
  end do
  !
  !     infrared, sab: specific absorptance; absa: absorptance
  !
  do j = 1, nbl
    do k = 1, lay
      do i = il1, il2
        if (aload(i,k) < 1.e-12) cycle
        l             =  re_num(i,k)
        lp1           =  l + 1
        if (delta(i,k) == 0.0) lp1 = l
        sab1          =  da1(j,l) + delta(i,k) * (da1(j,lp1) - da1(j,l))
        sab2          =  da2(j,l) + delta(i,k) * (da2(j,lp1) - da2(j,l))
        sab3          =  da3(j,l) + delta(i,k) * (da3(j,lp1) - da3(j,l))
        sab           =  sload(i,k) * ( sab1 * fr1(i,k) + sab2 * fr2(i,k) + &
                         sab3 * fr3(i,k) )
        !
        absa(i,k,j)   =  absa(i,k,j) + sab
      end do
    end do
  end do
  !
  return
end subroutine dustaerop1
!> \file
!> Calculation of optical properties for dust in continous mode (PAM)
!! A library of optical properties are computed for 10 effective radii,
!! which are linerly interpolated to any effective radius between them.
!! For effective variance the 3-point Lagrangian interoplation
!! method is used.
