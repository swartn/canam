!> \file sulfaero5.F90
!>\brief Calculation of black carbon aerosol optical property for bulk aerosol parameterization
!!
!! @author Jiangnan Li
!
subroutine sulfaero5 (exta, exoma, exomga, fa, absa, &
                      exta055, oma055, ga055, &
                      extb, odfb, ssab, absb, extb055,odf055, &
                      ssa055, abs055, rhin, aload, &
                      il1, il2, ilg, lay)
  !
  !     * apr 23,2012 - j.li & y.peng. new version for gcm16:
  !     *                         - add the optical depth calculation
  !     *                           for 0.55 um
  !     * feb 13,2009 - m.lazare. previous version sulfaero4 for gcm15h/i:
  !     *                         rhmax lowered from 0.98 to 0.95.
  !     * dec 05,2007 - m.lazare. previous version sulfaero3 for gcm15g:
  !     *                         - hf,rs2,fr1,fr2,fr3,re,rh now
  !     *                           internal work arrays.
  !     * jun 19/06 -  m.lazare. previous version sulfaero2 for gcm15f:
  !     *                        - cosmetic: use variable instead of
  !     *                          constant in intrinsics such as "max",
  !     *                          so that can compile in 32-bit mode
  !     *                          with real(8).
  !     * may 14,2003 - j.li.    previous version sulfaero for up to
  !     *                        and including gcm15e.
  !
  use psizes_19, only : nbs, nbl
  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  !
  integer, parameter :: nblm1 = nbl - 1
  !
  !     * output arrays.
  !
  real, intent(inout), dimension(ilg,lay,nbs) :: exta !< Extinction coefficient at 0.55 \f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: exoma !< EXTA*single scattering albedo \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: exomga !< EXOMA*asymmetry factor \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: fa !< EXOMGA*asymmetry factor \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: absb !< Absorption coefficient \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: odfb !< Extinction coefficient per mass for accumulation mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: extb !< Extinction coefficient per mass at 0.55\f$\mu\f$m for accumulation+coarse mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: ssab !< Single scattering albedo \f$[1]\f$
  real, intent(inout), dimension(ilg,lay,nbl) :: absa !< Longwave absorption coefficient \f$[m^2/gram]\f$
  !
  real, intent(inout), dimension(ilg,lay) :: exta055 !< Extinction coefficient at 0.55 \f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: oma055 !< EXTA055 * single scattering albedo at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: ga055 !< OMA055 * asymmetry factor at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: extb055 !< Extinction coefficient per mass at 0.55\f$\mu\f$m for accumulation+coarse mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: odf055 !< Extinction coefficient per mass at 0.55\f$\mu\f$m for accumulation mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: ssa055 !< Single scattering albedo at 0.55\f$\mu\f$m \f$[1]\f$
  real, intent(inout), dimension(ilg,lay) :: abs055 !< Absorption coefficient at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  !
  !     * input arrays.
  !
  real, intent(in), dimension(ilg,lay) :: rhin !< Input relative humidity \f$[\%]\f$
  real, intent(in), dimension(ilg,lay) :: aload !< Aerosol loading \f$[gram/gram]\f$
  !==================================================================
  !     calculation of optical properties for sulfate aerosol, h2so4,
  !     (nh4)2so4 and nh4hso4 (li et al 2001 jas p193; li and min 2002
  !     jas p3130)
  !     s1: h2so4, s2: (nh4)2so4, s3: nh4hso4,
  !     9: 1-3 for re = 0.166 um; 4-6 for re = 0.5 um; 7-9 for re = 1.0
  !
  !==================================================================
  !
  real :: cw20166
  real :: cw205
  real :: cw210
  real :: exom
  real :: exomg
  real :: ext
  real :: ext055
  real :: g
  real :: g20166
  real :: g205
  real :: g210
  real :: gg055
  integer :: i
  integer :: j
  integer :: k
  real :: om
  real :: re0166
  real :: re05
  real :: re10
  real :: sa20166
  real :: sa205
  real :: sa210
  real :: se20166
  real :: se205
  real :: se210
  real :: tmass
  !
  !     * internal work arrays.
  !
  real, dimension(ilg,lay) :: hf
  real, dimension(ilg,lay) :: rs2
  real, dimension(ilg,lay) :: fr1
  real, dimension(ilg,lay) :: fr2
  real, dimension(ilg,lay) :: fr3
  real, dimension(ilg,lay) :: re
  real, dimension(ilg,lay) :: rh
  !
  real, dimension(9,nbs) :: se2
  real, dimension(9,nbs) :: cw2
  real, dimension(9,nbs) :: g2
  real, dimension(9,nblm1) :: sa2
  real, dimension(3) :: se055
  real   :: cw055
  real, dimension(2) :: g055
  !
  real, parameter :: rhmax = 0.95
  !
  se2 = reshape([3.94   , 9.46e-1, 7.56e-2, 2.44   , 1.34e-1, 8.80e-2, &
                 1.16   , 1.85e-2, 4.07e-2, 1.64   , 5.55e-1, - 3.05e-2, &
                 2.04   , 3.24e-1, 6.64e-2, 1.24   , 5.11e-2, 4.49e-2, &
                 5.27e-1, 1.38e-1, - 4.33e-2, 1.22   , 3.20e-1, 2.00e-2, &
                 1.07   , 1.40e-1, 3.58e-2, 1.86e-1, 8.73e-2, - 1.75e-2, &
                 4.51e-1, 1.86e-1, - 7.23e-3, 5.97e-1, 1.59e-1, 1.13e-2], [9, nbs])
  !
  cw2 = reshape([6.71e-7, - 4.52e-7, 7.34e-9, 1.36e-6, - 8.83e-7, 9.43e-9, &
                 2.93e-6, - 1.84e-6, 2.51e-8, 2.89e-6, 2.73e-6, - 7.21e-8, &
                 3.65e-6, 1.25e-6, - 5.96e-7, 6.88e-6, 1.09e-6, - 1.48e-6, &
                 1.49e-3, 6.05e-4, 2.99e-5, 9.64e-4, 2.89e-4, - 5.47e-5, &
                 1.38e-3, 7.31e-5, - 1.54e-4, 6.64e-1, 3.33e-2, 1.73e-2, &
                 3.05e-1, 4.78e-2, 7.74e-3, 2.19e-1, 3.67e-2, 3.41e-3], [9, nbs])
  !
  g2 = reshape([.639, .116, - 2.73e-3, .671, .100, - 1.56e-3, .687, .084, - 2.99e-3, &
                .591, .114, - 5.56e-3, .675, .107, - 1.13e-3, .683, .091, - 1.62e-3, &
                .512, .108, - 9.00e-3, .658, .111, - 2.76e-3, .689, .102, - 1.11e-3, &
                .349, .067, - 1.41e-2, .597, .100, - 6.89e-3, .697, .097, - 3.16e-3], [9, nbs])
  !
  sa2 = reshape([1.20e-2,  8.16e-3, - 5.83e-4, 1.68e-2,  1.04e-2, - 6.24e-4, &
                 2.22e-2,  1.09e-2, - 3.54e-4, 3.04e-1, - 1.62e-1,  6.23e-3, &
                 2.74e-1, - 1.23e-1,  6.16e-3, 2.05e-1, - 6.27e-2,  5.43e-3, &
                 3.15e-1, - 1.35e-1,  5.29e-3, 2.94e-1, - 1.05e-1,  5.20e-3, &
                 2.32e-1, - 5.06e-2,  5.07e-3, 3.54e-1, - 2.11e-1,  5.25e-3, &
                 3.22e-1, - 1.78e-1,  4.83e-3, 2.56e-1, - 1.17e-1,  4.45e-3, &
                 2.34e-1,  4.69e-2,  1.34e-2, 3.03e-1, - 1.35e-2,  1.37e-2, &
                 3.25e-1, - 6.14e-2,  1.21e-2, 2.35e-3,  3.21e-2, - 1.34e-3, &
                 1.36e-3,  3.60e-2, - 9.26e-4, 3.30e-3,  3.59e-2, - 2.26e-4, &
                 3.03e-2,  1.37e-1, - 6.75e-3, 2.63e-2,  1.62e-1, - 4.78e-3, &
                 3.06e-2,  1.75e-1, - 7.06e-4, 5.75e-3,  1.01e-1, - 3.85e-3, &
                 7.97e-3,  1.08e-1, - 4.22e-3, 8.59e-3,  1.24e-1, - 2.95e-3], [9, nblm1])
  !
  se055 = [3.44744,  0.88011, 0.38768e-2]
  !
  cw055 = 0.99999
  !
  g055 = [0.61204,  1.6612e-01]
  !
  !----------------------------------------------------------------------c
  !     li et al (2001) can handle s1: h2so4, s2: (nh4)2so4, s3: nh4hso4,c
  !     we only consider 2 here                                          c
  !     rs: ratio of wet to dry sulfate aerosol content, re: effective   c
  !     radius of sulfate aerosol, fr1,2,3 are coefficients for          c
  !     lagrangian interpolation based on results of re = 0.166, 0.5 and c
  !     1 um.                                                            c
  !----------------------------------------------------------------------c
  !
  !----------------------------------------------------------------------c
  !     solar, se: specific extinction, omas: single scattering albedo,  c
  !     gas: asymmetry factor, extas: extinction coefficient             c
  !     10000 in rs2, because the unit of specific extinction for aerosolc
  !     is m^2/gram, while for gas is cm^2/gram, in raddriv we same      c
  !     dp (air density * layer thickness) is used for both gas and      c
  !     aerosol. aload is pure so4 loading in unit g (sulfur) / g        c
  !     (air). tau = aload * specific extinction * air density *         c
  !     layer thickness                                                  c
  !     4.12: convert the mass of sulfur to mass of (nh4)2so4            c
  !           (not for pla mass !! )                                       C
  !----------------------------------------------------------------------c
  !
  do k = 1, lay
    do i = il1, il2
      re(i,k)         =  0.166
      rh(i,k)         =  min (rhin(i,k), rhmax)
      hf(i,k)         =  1.0 / (rh(i,k) - 1.05)
      rs2(i,k)        =  10000.0 * exp( - 0.2373 + 1.135 * rh(i,k) + &
                        0.01048 / ((rh(i,k) - 1.06) * &
                        (rh(i,k) - 1.06)))
      re0166          =  re(i,k) - 0.166
      re05            =  re(i,k) - 0.5
      re10            =  re(i,k) - 1.0
      fr1(i,k)        =  3.5899424 * re05 * re10
      fr2(i,k)        =  - 5.988024 * re0166 * re10
      fr3(i,k)        =  2.3980815 * re0166 * re05
    end do
  end do ! loop 100
  !
  do j = 1, nbs
    do k = 1, lay
      do i = il1, il2
        se20166         =  se2(1,j) + se2(2,j) * rh(i,k) + &
                          se2(3,j) * hf(i,k)
        se205           =  se2(4,j) + se2(5,j) * rh(i,k) + &
                          se2(6,j) * hf(i,k)
        se210           =  se2(7,j) + se2(8,j) * rh(i,k) + &
                          se2(9,j) * hf(i,k)
        ext             =  4.12 * aload(i,k) * rs2(i,k) * &
                          (fr1(i,k) * se20166 + fr2(i,k) * se205 + &
                          fr3(i,k) * se210)
        !
        cw20166         =  cw2(1,j) + cw2(2,j) * rh(i,k) + &
                          cw2(3,j) * hf(i,k)
        cw205           =  cw2(4,j) + cw2(5,j) * rh(i,k) + &
                          cw2(6,j) * hf(i,k)
        cw210           =  cw2(7,j) + cw2(8,j) * rh(i,k) + &
                          cw2(9,j) * hf(i,k)
        om              =  1.0 - fr1(i,k) * cw20166 - fr2(i,k) * cw205 - &
                          fr3(i,k) * cw210
        exom            =  ext * om
        !
        g20166          =  g2(1,j) + g2(2,j) * rh(i,k) + &
                          g2(3,j) * hf(i,k)
        g205            =  g2(4,j) + g2(5,j) * rh(i,k) + &
                          g2(6,j) * hf(i,k)
        g210            =  g2(7,j) + g2(8,j) * rh(i,k) + &
                          g2(9,j) * hf(i,k)
        g               =  fr1(i,k) * g20166 + fr2(i,k) * g205 + &
                          fr3(i,k) * g210
        exomg           =  exom * g
        exta(i,k,j)     =  exta(i,k,j) + ext
        exoma(i,k,j)    =  exoma(i,k,j) + exom
        exomga(i,k,j)   =  exomga(i,k,j) + exomg
        fa(i,k,j)       =  fa(i,k,j) + exomg * g
        !
        absb(i,k,j)     =  ext * (1.0 - om)
        tmass           =  4.12 * 10000.0 * aload(i,k)
        if (tmass > 1.e-15) then
          extb(i,k,j)    =  ext / tmass
        else
          extb(i,k,j)    =  0.0
        end if
        odfb(i,k,j)     =  extb(i,k,j)
        ssab(i,k,j)     =  om
        !
      end do
    end do
  end do ! loop 200
  !
  do k = 1, lay
    do i = il1, il2
      !
      ! * 550 nm, wet approach, same as band cal., vm
      !
      ext055          =  4.12 * aload(i,k) * rs2(i,k) * &
                        (se055(1) + se055(2) * rh(i,k) + &
                        se055(3) * hf(i,k))
      exta055(i,k)    =  exta055(i,k) + ext055
      oma055(i,k)     =  oma055(i,k) + ext055 * cw055
      gg055           =  g055(1) + g055(2) * rh(i,k)
      ga055(i,k)      =  ga055(i,k) + ext055 * cw055 * gg055 ** 2
      !
      abs055(i,k)     =  ext055 * (1.0 - cw055)
      tmass           =  4.12 * 10000.0 * aload(i,k)
      if (tmass > 1.e-15) then
        extb055(i,k)   =  ext055/tmass
      else
        extb055(i,k)   =  0.0
      end if
      odf055(i,k)     =  extb055(i,k)
      ssa055(i,k)     =  cw055
      !
    end do
  end do ! loop 250
  !
  !----------------------------------------------------------------------c
  !     infrared, sa: specific absorptance, absas: absorption            c
  !     coefficient the contribution of band 9 is very small, neglected  c
  !----------------------------------------------------------------------c
  !
  do j = 1, nblm1
    do k = 1, lay
      do i = il1, il2
        sa20166         =  sa2(1,j) + sa2(2,j) * rh(i,k) + &
                          sa2(3,j) * hf(i,k)
        sa205           =  sa2(4,j) + sa2(5,j) * rh(i,k) + &
                          sa2(6,j) * hf(i,k)
        sa210           =  sa2(7,j) + sa2(8,j) * rh(i,k) + &
                          sa2(9,j) * hf(i,k)
        absa(i,k,j)     =  absa(i,k,j) + 4.12 * aload(i,k) * rs2(i,k) * &
                          (fr1(i,k) * sa20166 + fr2(i,k) * sa205 + &
                          fr3(i,k) * sa210)
      end do
    end do
  end do ! loop 400
  !
  do k = 1, lay
    do i = il1, il2
      absa(i,k,9)     =  0.0
    end do
  end do ! loop 600
  !
  return
end subroutine sulfaero5
!> \file
!> Calculation of optical properties for sulfate aerosol, H2SO4,
!! (NH4)2SO4 and NH4HSO4 using the method described in \cite Li2002c for thermal
!! optical properties and in \cite Li2001 for solar optical properties.
