!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine hines_smooth (data,work,dragil,coeff,nsmooth, &
                         il1,il2,lev1,lev2,nlons,nlevs)
  !
  !  smooth a longitude by altitude array in the vertical over a
  !  specified number of levels using a three point smoother.
  !
  !  note: input array data is modified on output !
  !
  !  aug. 3/95 - c. mclandress
  !
  !  modifications:
  !  --------------
  !  feb. 2/96 - c. mclandress (added logical :: flag)
  !

  implicit none

  !  output arguement:
  !  ----------------
  !
  !     * data    = smoothed array (on output).
  !
  !  input arguements:
  !  -----------------
  !
  !     * data    = unsmoothed array of data (on input).
  !     * work    = work array of same dimension as data.
  !     * dragil  = logical :: flag indicating longitudes and levels where
  !     *           calculations to be performed.
  !     * coeff   = smoothing coefficient for a 1:coeff:1 stencil.
  !     *           (e.g., coeff = 2 will result in a smoother which
  !     *           weights the level l gridpoint by two and the two
  !     *           adjecent levels (l+1 and l-1) by one).
  !     * nsmooth = number of times to smooth in vertical.
  !     *           (e.g., nsmooth=1 means smoothed only once,
  !     *           nsmooth=2 means smoothing repeated twice, etc.)
  !     * il1     = first longitudinal index to use (il1 >= 1).
  !     * il2     = last longitudinal index to use (il1 <= il2 <= nlons).
  !     * lev1    = first altitude level to use (lev1 >=1).
  !     * lev2    = last altitude level to use (lev1 < lev2 <= nlevs).
  !     * nlons   = number of longitudes.
  !     * nlevs   = number of vertical levels.
  !
  !  subroutine arguements.
  !  ----------------------
  !
  integer, intent(in)  :: nsmooth !< Variable description\f$[units]\f$
  integer, intent(in)  :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: lev1   !< Variable description\f$[units]\f$
  integer, intent(in)  :: lev2   !< Variable description\f$[units]\f$
  integer, intent(in)  :: nlons !< Variable description\f$[units]\f$
  integer, intent(in) :: nlevs !< Variable description\f$[units]\f$
  ! ccc      logical :: dragil(nlons,nlevs)
  integer, intent(in), dimension(nlons,nlevs) :: dragil !< Variable description\f$[units]\f$
  real, intent(in)    :: coeff !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: data !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: work !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !  internal variables.
  !  -------------------
  !
  integer  :: i
  integer  :: l
  integer  :: ns
  integer  :: lev1p
  integer  :: lev2m
  real    :: sum_wts
  !-----------------------------------------------------------------------
  !
  !  calculate sum of weights.
  !
  sum_wts = coeff + 2.
  !
  lev1p = lev1 + 1
  lev2m = lev2 - 1
  !
  !  smooth nsmooth times
  !
  do ns = 1,nsmooth
    !
    !  copy data into work array.
    !
    do l = lev1,lev2
      do i = il1,il2
        work(i,l) = data(i,l)
      end do
    end do ! loop 20
    !
    !  smooth array work in vertical direction and put into data.
    !
    do l = lev1p,lev2m
      do i = il1,il2
        if (dragil(i,l) == 1) then
          data(i,l) = ( work(i,l + 1) + coeff * work(i,l) + work(i,l - 1) ) &
                      / sum_wts
        end if
      end do
    end do ! loop 30
    !
  end do ! loop 50
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
