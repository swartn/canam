!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine hines_sigma (sigma_t,sigma_alpha,sigsqh_alpha,drag, &
                        naz,lev,il1,il2,nlons,nlevs,nazmth)
  !
  !  this routine calculates the total rms and azimuthal rms horizontal
  !  velocities at a given level on a longitude by altitude grid for
  !  the Hines' Doppler spread GWD parameterization scheme.
  !  note: only 4, 8, 12 or 16 azimuths can be used.
  !
  !  aug. 7/95 - c. mclandress
  !
  !  modifications:
  !  --------------
  !  feb. 2/96 - c. mclandress (added: 12 and 16 azimuths; logical :: flags)
  !

  implicit none

  !  output arguements:
  !  ------------------
  !
  !     * sigma_t     = total rms horizontal wind (m/s).
  !     * sigma_alpha = total rms wind in each azimuth (m/s).
  !
  !  input arguements:
  !  -----------------
  !
  !     * sigsqh_alpha = portion of wind variance from waves having wave
  !     *                normals in the alpha azimuth (m/s).
  !     * drag      = logical :: flag indicating longitudes where calculations
  !     *             to be performed.
  !     * naz       = number of horizontal azimuths used (4, 8, 12 or 16).
  !     * lev       = altitude level to process.
  !     * il1       = first longitudinal index to use (il1 >= 1).
  !     * il2       = last longitudinal index to use (il1 <= il2 <= nlons).
  !     * nlons     = number of longitudes.
  !     * nlevs     = number of vertical levels.
  !     * nazmth    = azimuthal array dimension (nazmth >= naz).
  !
  !  subroutine arguements.
  !  ---------------------
  !
  integer, intent(in)  :: lev   !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in)  :: naz !< Variable description\f$[units]\f$
  integer, intent(in)  :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: nlons !< Variable description\f$[units]\f$
  integer, intent(in)  :: nlevs !< Variable description\f$[units]\f$
  integer, intent(in)  :: nazmth !< Variable description\f$[units]\f$
  ! ccc      logical  :: drag(nlons)
  integer, intent(in), dimension(nlons) :: drag !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: sigma_t !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs,nazmth) :: sigma_alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs,nazmth) :: sigsqh_alpha !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !  internal variables.
  !  -------------------
  !
  integer  :: i
  integer  :: n
  real    :: sum_2468
  real    :: sum_1357
  real    :: sum_26812
  real    :: sum_35911
  real    :: sum_1379
  real    :: sum_461012
  real    :: sum_24810
  real    :: sum_15711
  real    :: sum_281016
  real    :: sum_371115
  real    :: sum_461214
  real    :: sum_13911
  real    :: sum_481216
  real    :: sum_571315
  real    :: sum_241012
  real    :: sum_15913
  real    :: sum_681416
  real    :: sum_351113
  real    :: sum_261014
  real    :: sum_17915
  real    :: c22sq
  real    :: c67sq
  data  c22sq / 0.8535534 /, c67sq / 0.1464466 /
  !-----------------------------------------------------------------------
  !
  !  calculate azimuthal rms velocity for the 4 azimuth case.
  !
  if (naz == 4) then
    do i = il1,il2
      if (drag(i) == 1) then
        sigma_alpha(i,lev,1) = sqrt ( sigsqh_alpha(i,lev,1) &
                               + sigsqh_alpha(i,lev,3) )
        sigma_alpha(i,lev,2) = sqrt ( sigsqh_alpha(i,lev,2) &
                               + sigsqh_alpha(i,lev,4) )
        sigma_alpha(i,lev,3) = sigma_alpha(i,lev,1)
        sigma_alpha(i,lev,4) = sigma_alpha(i,lev,2)
      end if
    end do ! loop 10
  end if
  !
  !  calculate azimuthal rms velocity for the 8 azimuth case.
  !
  if (naz == 8) then
    do i = il1,il2
      if (drag(i) == 1) then
        sum_1357 = sigsqh_alpha(i,lev,1) + sigsqh_alpha(i,lev,3) &
                   + sigsqh_alpha(i,lev,5) + sigsqh_alpha(i,lev,7)
        sum_2468 = sigsqh_alpha(i,lev,2) + sigsqh_alpha(i,lev,4) &
                   + sigsqh_alpha(i,lev,6) + sigsqh_alpha(i,lev,8)
        sigma_alpha(i,lev,1) = sqrt ( sigsqh_alpha(i,lev,1) &
                               + sigsqh_alpha(i,lev,5) + sum_2468/2. )
        sigma_alpha(i,lev,2) = sqrt ( sigsqh_alpha(i,lev,2) &
                               + sigsqh_alpha(i,lev,6) + sum_1357/2. )
        sigma_alpha(i,lev,3) = sqrt ( sigsqh_alpha(i,lev,3) &
                               + sigsqh_alpha(i,lev,7) + sum_2468/2. )
        sigma_alpha(i,lev,4) = sqrt ( sigsqh_alpha(i,lev,4) &
                               + sigsqh_alpha(i,lev,8) + sum_1357/2. )
        sigma_alpha(i,lev,5) = sigma_alpha(i,lev,1)
        sigma_alpha(i,lev,6) = sigma_alpha(i,lev,2)
        sigma_alpha(i,lev,7) = sigma_alpha(i,lev,3)
        sigma_alpha(i,lev,8) = sigma_alpha(i,lev,4)
      end if
    end do ! loop 20
  end if
  !
  !  calculate azimuthal rms velocity for the 12 azimuth case.
  !
  if (naz == 12) then
    do i = il1,il2
      if (drag(i) == 1) then
        sum_26812  = sigsqh_alpha(i,lev, 2) + sigsqh_alpha(i,lev, 6) &
                     + sigsqh_alpha(i,lev, 8) + sigsqh_alpha(i,lev,12)
        sum_35911  = sigsqh_alpha(i,lev, 3) + sigsqh_alpha(i,lev, 5) &
                     + sigsqh_alpha(i,lev, 9) + sigsqh_alpha(i,lev,11)
        sum_1379   = sigsqh_alpha(i,lev, 1) + sigsqh_alpha(i,lev, 3) &
                     + sigsqh_alpha(i,lev, 7) + sigsqh_alpha(i,lev, 9)
        sum_461012 = sigsqh_alpha(i,lev, 4) + sigsqh_alpha(i,lev, 6) &
                     + sigsqh_alpha(i,lev,10) + sigsqh_alpha(i,lev,12)
        sum_24810  = sigsqh_alpha(i,lev, 2) + sigsqh_alpha(i,lev, 4) &
                     + sigsqh_alpha(i,lev, 8) + sigsqh_alpha(i,lev,10)
        sum_15711  = sigsqh_alpha(i,lev, 1) + sigsqh_alpha(i,lev, 5) &
                     + sigsqh_alpha(i,lev, 7) + sigsqh_alpha(i,lev,11)
        sigma_alpha(i,lev,1)  = sqrt ( sigsqh_alpha(i,lev,1) &
                                + sigsqh_alpha(i,lev,7) &
                                + 0.75 * sum_26812 + 0.25 * sum_35911)
        sigma_alpha(i,lev,2)  = sqrt ( sigsqh_alpha(i,lev,2) &
                                + sigsqh_alpha(i,lev,8) &
                                + 0.75 * sum_1379 + 0.25 * sum_461012)
        sigma_alpha(i,lev,3)  = sqrt ( sigsqh_alpha(i,lev,3) &
                                + sigsqh_alpha(i,lev,9) &
                                + 0.75 * sum_24810 + 0.25 * sum_15711)
        sigma_alpha(i,lev,4)  = sqrt ( sigsqh_alpha(i,lev,4) &
                                + sigsqh_alpha(i,lev,10) &
                                + 0.75 * sum_35911 + 0.25 * sum_26812)
        sigma_alpha(i,lev,5)  = sqrt ( sigsqh_alpha(i,lev,5) &
                                + sigsqh_alpha(i,lev,11) &
                                + 0.75 * sum_461012 + 0.25 * sum_1379)
        sigma_alpha(i,lev,6)  = sqrt ( sigsqh_alpha(i,lev,6) &
                                + sigsqh_alpha(i,lev,12) &
                                + 0.75 * sum_15711 + 0.25 * sum_24810)
        sigma_alpha(i,lev,7)  = sigma_alpha(i,lev,1)
        sigma_alpha(i,lev,8)  = sigma_alpha(i,lev,2)
        sigma_alpha(i,lev,9)  = sigma_alpha(i,lev,3)
        sigma_alpha(i,lev,10) = sigma_alpha(i,lev,4)
        sigma_alpha(i,lev,11) = sigma_alpha(i,lev,5)
        sigma_alpha(i,lev,12) = sigma_alpha(i,lev,6)
      end if
    end do ! loop 30
  end if
  !
  !  calculate azimuthal rms velocity for the 16 azimuth case.
  !
  if (naz == 16) then
    do i = il1,il2
      if (drag(i) == 1) then
        sum_281016 = sigsqh_alpha(i,lev, 2) + sigsqh_alpha(i,lev, 8) &
                     + sigsqh_alpha(i,lev,10) + sigsqh_alpha(i,lev,16)
        sum_371115 = sigsqh_alpha(i,lev, 3) + sigsqh_alpha(i,lev, 7) &
                     + sigsqh_alpha(i,lev,11) + sigsqh_alpha(i,lev,15)
        sum_461214 = sigsqh_alpha(i,lev, 4) + sigsqh_alpha(i,lev, 6) &
                     + sigsqh_alpha(i,lev,12) + sigsqh_alpha(i,lev,14)
        sum_13911  = sigsqh_alpha(i,lev, 1) + sigsqh_alpha(i,lev, 3) &
                     + sigsqh_alpha(i,lev, 9) + sigsqh_alpha(i,lev,11)
        sum_481216 = sigsqh_alpha(i,lev, 4) + sigsqh_alpha(i,lev, 8) &
                     + sigsqh_alpha(i,lev,12) + sigsqh_alpha(i,lev,16)
        sum_571315 = sigsqh_alpha(i,lev, 5) + sigsqh_alpha(i,lev, 7) &
                     + sigsqh_alpha(i,lev,13) + sigsqh_alpha(i,lev,15)
        sum_241012 = sigsqh_alpha(i,lev, 2) + sigsqh_alpha(i,lev, 4) &
                     + sigsqh_alpha(i,lev,10) + sigsqh_alpha(i,lev,12)
        sum_15913  = sigsqh_alpha(i,lev, 1) + sigsqh_alpha(i,lev, 5) &
                     + sigsqh_alpha(i,lev, 9) + sigsqh_alpha(i,lev,13)
        sum_681416 = sigsqh_alpha(i,lev, 6) + sigsqh_alpha(i,lev, 8) &
                     + sigsqh_alpha(i,lev,14) + sigsqh_alpha(i,lev,16)
        sum_351113 = sigsqh_alpha(i,lev, 3) + sigsqh_alpha(i,lev, 5) &
                     + sigsqh_alpha(i,lev,11) + sigsqh_alpha(i,lev,13)
        sum_261014 = sigsqh_alpha(i,lev, 2) + sigsqh_alpha(i,lev, 6) &
                     + sigsqh_alpha(i,lev,10) + sigsqh_alpha(i,lev,14)
        sum_17915  = sigsqh_alpha(i,lev, 1) + sigsqh_alpha(i,lev, 7) &
                     + sigsqh_alpha(i,lev, 9) + sigsqh_alpha(i,lev,15)
        sigma_alpha(i,lev,1)  = sqrt ( &
                                sigsqh_alpha(i,lev, 1) + sigsqh_alpha(i,lev, 9) &
                                + c22sq * sum_281016     + 0.5 * sum_371115 &
                                + c67sq * sum_461214)
        sigma_alpha(i,lev,2)  = sqrt ( &
                                sigsqh_alpha(i,lev, 2) + sigsqh_alpha(i,lev,10) &
                                + c22sq * sum_13911      + 0.5 * sum_481216 &
                                + c67sq * sum_571315)
        sigma_alpha(i,lev,3)  = sqrt ( &
                                sigsqh_alpha(i,lev, 3) + sigsqh_alpha(i,lev,11) &
                                + c22sq * sum_241012     + 0.5 * sum_15913 &
                                + c67sq * sum_681416)
        sigma_alpha(i,lev,4)  = sqrt ( &
                                sigsqh_alpha(i,lev, 4) + sigsqh_alpha(i,lev,12) &
                                + c22sq * sum_351113     + 0.5 * sum_261014 &
                                + c67sq * sum_17915)
        sigma_alpha(i,lev,5)  = sqrt ( &
                                sigsqh_alpha(i,lev, 5) + sigsqh_alpha(i,lev,13) &
                                + c22sq * sum_461214     + 0.5 * sum_371115 &
                                + c67sq * sum_281016)
        sigma_alpha(i,lev,6)  = sqrt ( &
                                sigsqh_alpha(i,lev, 6) + sigsqh_alpha(i,lev,14) &
                                + c22sq * sum_571315     + 0.5 * sum_481216 &
                                + c67sq * sum_13911)
        sigma_alpha(i,lev,7)  = sqrt ( &
                                sigsqh_alpha(i,lev, 7) + sigsqh_alpha(i,lev,15) &
                                + c22sq * sum_681416     + 0.5 * sum_15913 &
                                + c67sq * sum_241012)
        sigma_alpha(i,lev,8)  = sqrt ( &
                                sigsqh_alpha(i,lev, 8) + sigsqh_alpha(i,lev,16) &
                                + c22sq * sum_17915      + 0.5 * sum_261014 &
                                + c67sq * sum_351113)
        sigma_alpha(i,lev,9)  = sigma_alpha(i,lev,1)
        sigma_alpha(i,lev,10) = sigma_alpha(i,lev,2)
        sigma_alpha(i,lev,11) = sigma_alpha(i,lev,3)
        sigma_alpha(i,lev,12) = sigma_alpha(i,lev,4)
        sigma_alpha(i,lev,13) = sigma_alpha(i,lev,5)
        sigma_alpha(i,lev,14) = sigma_alpha(i,lev,6)
        sigma_alpha(i,lev,15) = sigma_alpha(i,lev,7)
        sigma_alpha(i,lev,16) = sigma_alpha(i,lev,8)
      end if
    end do ! loop 40
  end if
  !
  !  initialize rms wind.
  !
  do i = il1,il2
    sigma_t(i,lev) = 0.
  end do ! loop 50
  !
  !  calculate total rms wind.
  !
  do n = 1,naz
    do i = il1,il2
      if (drag(i) == 1) then
        sigma_t(i,lev) = sigma_t(i,lev) + sigsqh_alpha(i,lev,n)
      end if
    end do
  end do ! loop 60
  do i = il1,il2
    if (drag(i) == 1) then
      sigma_t(i,lev) = sqrt ( sigma_t(i,lev) )
    end if
  end do ! loop 70
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
