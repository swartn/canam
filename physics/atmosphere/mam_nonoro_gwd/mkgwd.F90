!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine mkgwd(u,pf,ph,tf,th,bvfr,dp,gwkh,rg, &
                 ax,axx,usq,sigma,zkzr,omeg,alpha,beta, &
                 rms,ilev,nhar,levgws,lupp,ilg,il1,il2, &
                 v2,drag,gwkhu,vv2,xx,dz,bvkh,vv)

  ! vectorized version:  a. medvedev (1998)   oct 27
  !
  ! input parameters:
  !     u(ilg,ilev)   - array of background wind
  !    pf(ilg,ilev)   - standard pressure levels in "FULL" points
  !    ph(ilg,ilev)   - standard pressure levels in "HALF" points
  !    tf(ilg,ilev)   - temperature in "FULL" vertical points(throw)
  !    th(ilg,ilev)   - temperature in "HALF" vertical points(tfrow)
  !    bvfr(ilg,ilev) - brunt-vaisala frequency in "FULL" points
  !    gwkh       - horizontal wavenumber
  !    rg         - rgas/grav
  !    nhar       - number of harmonics in the spectrum
  !    levgws     - height level number where gw source is specified
  !    lupp       - upper level for calculation of gwd (=ilev usually)

  ! output data:
  !     usq(ilg,nhar,ilev)- horizontal velocity squared
  !     axilg,(ilev)      - vertical profile of gwd
  !     rms(ilg,ilev)     - rms horizontal wind

  ! working arrays:
  !     sigma(ilg,nhar)  - intrinsic frequencies
  !     zkzr(ilg,nhar,ilev)-vwn
  !     omeg(ilg,nhar,ilev)-frequencies
  !     alpha(ilg,nhar)
  !     beta(nhar,ilev)-damping rates
  !//////////////////////////////////////////////////////////////////////

  implicit none
  real :: a1
  real :: a11
  real :: alpha1
  real :: bet
  real :: critampl
  real, intent(in) :: gwkh
  integer :: i
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: k
  integer :: l
  integer :: l1p
  integer, intent(in) :: levgws  !< Variable description\f$[units]\f$
  integer :: ll
  integer :: llm
  integer :: lm
  integer, intent(in) :: lupp
  integer :: nh
  integer, intent(in) :: nhar
  real, intent(in) :: rg
  real :: sqrt2
  real :: sqrt2p
  real :: sqrtkh
  real :: sqrtp
  real :: vpzero
  real :: x
  real :: xxx

  !      parameter(nhar=15, vpzero=0.0)
  parameter(vpzero = 0.0)
  real, intent(in), dimension(ilg,ilev) :: u !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: pf !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: ph !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: tf !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: th !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: bvfr !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: ax !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: axx !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dp !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: rms !< Variable description\f$[units]\f$

  real, intent(inout), dimension(ilg,nhar,ilev) :: usq !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nhar,ilev) :: zkzr !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nhar,ilev) :: omeg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nhar,ilev) :: beta !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nhar) :: sigma !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nhar) :: alpha !< Variable description\f$[units]\f$

  real, intent(inout), dimension(ilg,nhar) :: v2 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nhar) :: drag !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: gwkhu !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: vv2 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: xx !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: dz   !< Layer thickness for thermodynamic layers \f$[m]\f$
  real, intent(inout), dimension(ilg) :: bvkh !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: vv !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  sqrt2 = 1.4142136
  sqrtp = 1.7724539
  sqrt2p = 2.5066283
  sqrtkh = sqrt2 * gwkh
  critampl = 1.0e-30 * 0.6e-7/real(nhar)
  !
  ! initialize gravity wave drag tendency to zero
  !
  l1p = levgws + 1
  do l = 1,lupp
    do i = il1,il2
      ax(i,l) = vpzero
      axx(i,l) = vpzero
    end do
  end do
  !
  ! compute parameters at the lower boundary
  !
  ll = ilev + 1 - levgws
  do i = il1,il2
    gwkhu(i) = gwkh * u(i,levgws)
    vv2(i) = vpzero
  end do

  do k = 1,nhar
    do i = il1,il2
      sigma(i,k) = omeg(i,k,levgws) + gwkhu(i)
      zkzr(i,k,levgws) = gwkh * bvfr(i,ll)/omeg(i,k,levgws)
      vv2(i) = vv2(i) + usq(i,k,levgws)
      v2(i,k) = vv2(i)
      vv(i) = sqrt(vv2(i))
      alpha(i,k) = omeg(i,k,levgws)/sqrt2/(gwkh * vv(i))
      !     * add this since it appears that beta(i,k,levgws) has not been
      !     * assigned
      beta(i,k,levgws) = 0.
    end do
  end do
  !
  ! step-by-step integration upward
  !
  do l = l1p,lupp
    ll = ilev + 1 - l
    lm = l - 1
    llm = ll + 1
    do i = il1,il2
      gwkhu(i) = gwkh * u(i,l)
      xx(i) = tf(i,ll) * pf(i,lm)/tf(i,llm)/pf(i,l)
      dz(i) = - rg * dp(i,lm) * th(i,ll)/ph(i,l)
      bvkh(i) = bvfr(i,ll) * gwkh
    end do
    !
    do nh = 1,nhar
      do i = il1,il2       !.......... longitude loop begins........
        !
        ! exclude harmonics which have been filtered out by critical levels
        !
        if (usq(i,nh,lm) <= critampl) then
          usq(i,nh,l) = vpzero
          drag(i,nh) = vpzero
        else
          !
          ! Calculate only for harmonics with nonzero amplitude, i.e. which haven't
          ! met critical levels yet
          !
          vv(i) = sqrt(v2(i,nh))
          omeg(i,nh,l) = sigma(i,nh) - gwkhu(i)
          alpha1 = omeg(i,nh,l)/(sqrtkh * vv(i))
          a1 = alpha1 * alpha(i,nh)
          a11 = alpha1 * alpha1
          if ((a1 <= vpzero) .or. (a11 <= 0.5)) then
            usq(i,nh,l) = vpzero
            drag(i,nh) = vpzero
          else
            alpha(i,nh) = alpha1
            beta(i,nh,l) = sqrt2p * exp( - a11) * bvfr(i,ll)/vv(i)
            zkzr(i,nh,l) = bvkh(i)/omeg(i,nh,l)
            xxx = zkzr(i,nh,l)/zkzr(i,nh,lm)
            x = xx(i) * xxx
            bet = 0.5 * (beta(i,nh,l) + beta(i,nh,lm))
            usq(i,nh,l) = x * exp( - bet * dz(i)) * usq(i,nh,lm)
            drag(i,nh) = bet * usq(i,nh,l) * gwkh/zkzr(i,nh,l)
          end if
        end if
      end do             !.......... end of longitute loop........
    end do

    do i = il1,il2
      vv2(i) = vpzero
    end do

    do nh = 1,nhar
      do i = il1,il2
        axx(i,l) = axx(i,l) + drag(i,nh)
        vv2(i) = vv2(i) + usq(i,nh,l)
        v2(i,nh) = vv2(i)
      end do
    end do

    do i = il1,il2
      rms(i,l) = sqrt(v2(i,nhar))
    end do

  end do                   !..........end of altitude loop.........

  do l = 2,lupp - 1
    do i = il1,il2
      ax(i,l) = (axx(i,l - 1) + 2. * axx(i,l) + axx(i,l + 1)) * 0.25
    end do
  end do

  do i = il1,il2
    ax(i,1) = axx(i,1)
    ax(i,lupp) = axx(i,lupp)
  end do

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
