!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine hines_prnt1 (flux_u,flux_v,drag_u,drag_v,vel_u,vel_v, &
                        alt,sigma_t,sigma_alpha,m_alpha, &
                        iu_print,iv_print,nmessg, &
                        ilprt1,ilprt2,levprt1,levprt2, &
                        naz,nlons,nlevs,nazmth)
  !
  !  print out altitude profiles of various quantities from
  !  hines doppler spread gravity wave parameterization scheme.
  !  (note: only for naz = 4, 8 or 12).
  !
  !  aug. 8/95 - c. mclandress
  !
  !  modifications:
  !  --------------
  !  feb. 2/96 - c. mclandress (12 and 16 azimuths)
  !

  implicit none

  !  input arguements:
  !  -----------------
  !
  !     * iu_print = 1 to print out values in east-west direction.
  !     * iv_print = 1 to print out values in north-south direction.
  !     * nmessg   = unit number for printed output.
  !     * ilprt1   = first longitudinal index to print.
  !     * ilprt2   = last longitudinal index to print.
  !     * levprt1  = first altitude level to print.
  !     * levprt2  = last altitude level to print.
  !
  integer, intent(in)  :: naz !< Variable description\f$[units]\f$
  integer, intent(in)  :: ilprt1 !< Variable description\f$[units]\f$
  integer, intent(in)  :: ilprt2 !< Variable description\f$[units]\f$
  integer, intent(in)  :: levprt1   !< Variable description\f$[units]\f$
  integer, intent(in)  :: levprt2   !< Variable description\f$[units]\f$
  integer, intent(in)  :: nlons !< Variable description\f$[units]\f$
  integer, intent(in)  :: nlevs !< Variable description\f$[units]\f$
  integer, intent(in)  :: nazmth !< Variable description\f$[units]\f$
  integer, intent(in)  :: iu_print !< Variable description\f$[units]\f$
  integer, intent(in)  :: iv_print !< Variable description\f$[units]\f$
  integer, intent(in)  :: nmessg !< Variable description\f$[units]\f$!
  real, intent(in), dimension(nlons,nlevs) :: flux_u !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: flux_v !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: drag_u !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: drag_v !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: vel_u !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: vel_v !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: alt !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: sigma_t !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs,nazmth) :: sigma_alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs,nazmth) :: m_alpha !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !  internal variables.
  !  -------------------
  !
  integer  :: n_east
  integer  :: n_west
  integer  :: n_north
  integer  :: n_south
  integer  :: i
  integer  :: l
  !-----------------------------------------------------------------------
  !
  !  azimuthal indices of cardinal directions.
  !
  n_east = 1
  if (naz == 4) then
    n_north = 2
    n_west  = 3
    n_south = 4
  else if (naz == 8) then
    n_north = 3
    n_west  = 5
    n_south = 7
  else if (naz == 12) then
    n_north = 4
    n_west  = 7
    n_south = 10
  else if (naz == 16) then
    n_north = 5
    n_west  = 9
    n_south = 13
  end if
  !
  !  print out values for range of longitudes.
  !
  do i = ilprt1,ilprt2
    !
    !  print east-west wind, sigmas, cutoff wavenumbers, flux and drag.
    !
    if (iu_print == 1) then
      write (nmessg, * )
      write (nmessg,6001) i
      write (nmessg,6005)
6001  format ( 'Hines GW (east - west) at longitude I = ',i3)
6005  format (15x,' U ',2x,'sig_E',2x,'sig_T',3x,'m_E', &
             4x,'m_W',4x,'fluxU',5x,'gwdU')
      do l = levprt1,levprt2
        write (nmessg,6701) alt(i,l)/1.e3, vel_u(i,l), &
                           sigma_alpha(i,l,n_east), sigma_t(i,l), &
                           m_alpha(i,l,n_east) * 1.e3, &
                           m_alpha(i,l,n_west) * 1.e3, &
                           flux_u(i,l) * 1.e5, drag_u(i,l) * 24. * 3600.
      end do ! loop 10
6701  format (' z = ',f7.2,1x,3f7.1,2f7.3,f9.4,f9.3)
    end if
    !
    !  print north-south winds, sigmas, cutoff wavenumbers, flux and drag.
    !
    if (iv_print == 1) then
      write(nmessg, * )
      write(nmessg,6002)
6002  format ( 'Hines GW (north - south) at longitude I = ',i3)
      write(nmessg,6006)
6006  format (15x,' V ',2x,'sig_N',2x,'sig_T',3x,'m_N', &
             4x,'m_S',4x,'fluxV',5x,'gwdV')
      do l = levprt1,levprt2
        write (nmessg,6701) alt(i,l)/1.e3, vel_v(i,l), &
                           sigma_alpha(i,l,n_north), sigma_t(i,l), &
                           m_alpha(i,l,n_north) * 1.e3, &
                           m_alpha(i,l,n_south) * 1.e3, &
                           flux_v(i,l) * 1.e5, drag_v(i,l) * 24. * 3600.
      end do ! loop 20
    end if
    !
  end do ! loop 100
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
