!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

  subroutine classa(fc,     fg,     fcs,    fgs,    alvscn, alircn, &
                    alvsg,  alirg,  alvscs, alircs, alvssn, alirsn, &
                    alvsgc, alirgc, alvssc, alirsc, trvscn, trircn, &
                    trvscs, trircs, fsvf,   fsvfs, &
                    raican, raicns, snocan, snocns, frainc, fsnowc, &
                    fraics, fsnocs, disp,   disps,  zomlnc, zomlcs, &
                    zoelnc, zoelcs, zomlng, zomlns, zoelng, zoelns, &
                    chcap,  chcaps, cmassc, cmascs, cwlcap, cwfcap, &
                    cwlcps, cwfcps, rc,     rcs,    rbcoef, froot, &
                    froots, zplimc, zplimg, zplmcs, zplmgs, zsnow, &
                    wsnow,  alvs,   alir,   htcc,   htcs,   htc, &
                    altg,   alsno,  trsnowc,trsnowg, &
                    wtrc,   wtrs,   wtrg,   cmai,   fsnow, &
                    fcanmx, zoln,   alvsc,  alirc,  paimax, paimin, &
                    cwgtmx, zrtmax, rsmin,  qa50,   vpda,   vpdb, &
                    psiga,  psigb,  paidat, hgtdat, acvdat, acidat, &
                    asvdat, asidat, agvdat, agidat, &
                    algwv,  algwn,  algdv,  algdn, &
                    thliq,  thice,  tbar,   rcan,   sncan,  tcan, &
                    growth, sno,    tsnow,  rhosno, albsno, zblend, &
                    z0oro,  snolim, zplmg0, zplms0, &
                    fcloud, ta,     vpd,    rhoair, coszs, &
                    fsdb, fsfb, refsno, bcsno, &
                    qswinv, radj,   dlon,   rhosni, delz,   delzw, &
                    zbotw,  thpor,  thlmin, psisat, bi,     psiwlt, &
                    hcps,   isand, &
                    fcancmx,ictem,  ictemmod, rmatc, &
                    ailc,   paic,   l2max,  nol2pfts, slaic, &
                    ailcg,  ailcgs, fcanc,  fcancs, &
                    iday,   ilg,    il1,    il2, nbs, &
                    jl,n,   ic,     icp1,   ig,     idisp,  izref, &
                    iwf,    ipai,   ihgt,   ialc,   ials,   ialg, &
                    isnoalb, vdlagat, vdlsgat, switch_gas_chem)

  !     * feb 09/15 - d.verseghy. new version for gcm18 and class 3.6:
  !     *                         - {algwv,algwn,algdv,algdn} are passed
  !     *                           in (originating in classb) and then
  !     *                           passed into gralb, instead of
  !     *                           {algwet,algdry}.
  !     * nov 16/13 - j.cole.     final version for gcm17:
  !     *                         - pass "RHOSNO"in to snoalba to
  !     *                           calculate the proper bc mixing ratio
  !     *                           in snow.
  !     * nov 14/11 - m.lazare.   implement ctem support, primarily
  !     *                         involving additional fields to pass
  !     *                         in/out of new aprep routine. this
  !     *                         includes new input array "PAIC".
  !     * nov 30/06 - d.verseghy. convert radj to regular precision.
  !     * apr 13/06 - d.verseghy. separate ground and snow albedos for
  !     *                         open and canopy-covered areas; keep
  !     *                         fsnow as output array.
  !     * apr 06/06 - d.verseghy. introduce modelling of wsnow.
  !     * mar 14/05 - d.verseghy. rename scan to sncan (reserved name
  !     *                         in f90); change snolim from constant
  !     *                         to variable.
  !     * nov 03/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * dec 05/02 - d.verseghy. new parameters for aprep.
  !     * jul 31/02 - d.verseghy. modifications associated with new
  !     *                         calculation of stomatal resistance.
  !     *                         shortened class3 common block.
  !     * jul 23/02 - d.verseghy. modifications to move addition of air
  !     *                         to canopy mass into aprep; shortened
  !     *                         class4 common block.
  !     * mar 18/02 - d.verseghy. new calls to all subroutines to enable
  !     *                         assignment of user-specified values to
  !     *                         albedos and vegetation properties; new
  !     *                         "CLASS8" common block; move calculation
  !     *                         of "FCLOUD" into class driver.
  !     * sep 19/00 - d.verseghy. pass additional arrays to aprep in common
  !     *                         block class7, for calculation of new
  !     *                         stomatal resistance coefficients used
  !     *                         in tprep.
  !     * apr 12/00 - d.verseghy. rcmin now varies with vegetation type:
  !     *                         pass in background array "RCMINX".
  !     * dec 16/99 - d.verseghy. add "XLEAF" array to class7 common block
  !     *                         and calculation of leaf dimension parameter
  !     *                         "DLEAF" in aprep.
  !     * nov 16/98 - m.lazare.   "DLON" now passed in and used directly
  !     *                         (instead of inferring from "LONSL" and
  !     *                         "ILSL" which used to be passed) to pass
  !     *                         to aprep to calculate growth index. this
  !     *                         is done to make the physics plug compatible
  !     *                         for use with the rcm which does not have
  !     *                         equally-spaced longitudes.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         modifications to allow for variable
  !     *                         soil permeable depth.
  !     * sep 27/96 - d.verseghy. class - version 2.6.
  !     *                         fix bug to calculate ground albedo
  !     *                         under canopies as well as over bare
  !     *                         soil.
  !     * jan 02/96 - d.verseghy. class - version 2.5.
  !     *                         completion of energy balance
  !     *                         diagnostics.
  !     *                         also, pass idisp to subroutine aprep.
  !     * aug 30/95 - d.verseghy. class - version 2.4.
  !     *                         variable surface detention capacity
  !     *                         implemented.
  !     * aug 16/95 - d.verseghy. three new arrays to complete water
  !     *                         balance diagnostics.
  !     * oct 14/94 - d.verseghy. class - version 2.3.
  !     *                         revise calculation of fcloud to
  !     *                         handle cases where incoming solar
  !     *                         radiation is zero at low sun angles.
  !     * nov 24/92 - m.lazare.   class - version 2.1.
  !     *                         modified for multiple latitudes.
  !     * oct 13/92 - d.verseghy/m.lazare. revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. visible and near-ir albedos and
  !     *                         transmissivities for components of
  !     *                         land surface.
  ! ---------------------------------------------------------------------------

  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: iday   !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer, intent(in) :: ic !<
  integer, intent(in) :: icp1 !<
  integer, intent(in) :: ig !<
  integer, intent(in) :: idisp !<
  integer, intent(in) :: izref !<
  integer, intent(in) :: iwf !<
  integer, intent(in) :: ipai !<
  integer, intent(in) :: ihgt !<
  integer, intent(in) :: ialc !<
  integer, intent(in) :: ials !<
  integer, intent(in) :: ialg !<
  integer, intent(in) :: n !<
  integer, intent(in) :: nbs   !< Number of wavelength intervals for solar radiative transfer \f$[unitless]\f$
  integer, intent(in) :: isnoalb !<
  logical, intent(in) :: switch_gas_chem !<
  !
  !     * output arrays.
  !
  real, intent(inout) :: fc    (ilg) !<
  real, intent(inout) :: fg    (ilg) !<
  real, intent(inout) :: fcs   (ilg) !<
  real, intent(inout) :: fgs   (ilg) !<
  real, intent(inout) :: alvscn(ilg) !<
  real, intent(inout) :: alircn(ilg) !<
  real, intent(inout) :: alvsg (ilg) !<
  real, intent(inout) :: alirg (ilg) !<
  real, intent(inout) :: alvscs(ilg) !<
  real, intent(inout) :: alircs(ilg) !<
  real, intent(inout) :: alvssn(ilg) !<
  real, intent(inout) :: alirsn(ilg) !<
  real, intent(inout) :: alvsgc(ilg) !<
  real, intent(inout) :: alirgc(ilg) !<
  real, intent(inout) :: alvssc(ilg) !<
  real, intent(inout) :: alirsc(ilg) !<
  real, intent(inout) :: trvscn(ilg) !<
  real, intent(inout) :: trircn(ilg) !<
  real, intent(inout) :: trvscs(ilg) !<
  real, intent(inout) :: trircs(ilg) !<
  real, intent(inout) :: fsvf  (ilg) !<
  real, intent(inout) :: fsvfs (ilg) !<
  real, intent(inout) :: raican(ilg) !<
  real, intent(inout) :: raicns(ilg) !<
  real, intent(inout) :: snocan(ilg) !<
  real, intent(inout) :: snocns(ilg) !<
  real, intent(inout) :: frainc(ilg) !<
  real, intent(inout) :: fsnowc(ilg) !<
  real, intent(inout) :: fraics(ilg) !<
  real, intent(inout) :: fsnocs(ilg) !<
  real, intent(inout) :: disp  (ilg) !<
  real, intent(inout) :: disps (ilg) !<
  real, intent(inout) :: zomlnc(ilg) !<
  real, intent(inout) :: zomlcs(ilg) !<
  real, intent(inout) :: zoelnc(ilg) !<
  real, intent(inout) :: zoelcs(ilg) !<
  real, intent(inout) :: zomlng(ilg) !<
  real, intent(inout) :: zomlns(ilg) !<
  real, intent(inout) :: zoelng(ilg) !<
  real, intent(inout) :: zoelns(ilg) !<
  real, intent(inout) :: chcap (ilg) !<
  real, intent(inout) :: chcaps(ilg) !<
  real, intent(inout) :: cmassc(ilg) !<
  real, intent(inout) :: cmascs(ilg) !<
  real, intent(inout) :: cwlcap(ilg) !<
  real, intent(inout) :: cwfcap(ilg) !<
  real, intent(inout) :: cwlcps(ilg) !<
  real, intent(inout) :: cwfcps(ilg) !<
  real, intent(inout) :: rc    (ilg) !<
  real, intent(inout) :: rcs   (ilg) !<
  real, intent(inout) :: zplimc(ilg) !<
  real, intent(inout) :: zplimg(ilg) !<
  real, intent(inout) :: zplmcs(ilg) !<
  real, intent(inout) :: zplmgs(ilg) !<
  real, intent(inout) :: rbcoef(ilg) !<
  real, intent(inout) :: trsnowc(ilg) !<
  real, intent(inout) :: zsnow (ilg) !<
  real, intent(inout) :: wsnow (ilg) !<
  real, intent(inout) :: alvs  (ilg) !<
  real, intent(inout) :: alir  (ilg) !<
  real, intent(inout) :: htcc  (ilg) !<
  real, intent(inout) :: htcs  (ilg) !<
  real, intent(inout) :: wtrc  (ilg) !<
  real, intent(inout) :: wtrs  (ilg) !<
  real, intent(inout) :: wtrg  (ilg) !<
  real, intent(inout) :: cmai  (ilg) !<
  real, intent(inout) :: fsnow (ilg) !<
  !
  real, intent(inout) :: trsnowg(ilg,nbs) !<
  real, intent(inout) :: altg(ilg,nbs) !<
  real, intent(inout) :: alsno(ilg,nbs) !<
  !
  real, intent(inout) :: froot (ilg,ig) !<
  real, intent(inout) :: froots(ilg,ig) !<
  real, intent(inout) :: htc   (ilg,ig) !<
!
! Output arrays for gas-phase chemistry
  real, intent(out), dimension(ilg) :: vdlagat, vdlsgat
  !
  !     * input arrays dependent on longitude.
  !
  real, intent(inout) :: fcanmx(ilg,icp1) !<
  real, intent(in) :: zoln  (ilg,icp1) !<
  real, intent(inout) :: alvsc (ilg,icp1) !<
  real, intent(inout) :: alirc (ilg,icp1) !<
  real, intent(inout) :: paimax(ilg,ic) !<
  real, intent(inout) :: paimin(ilg,ic) !<
  real, intent(in) :: cwgtmx(ilg,ic) !<
  real, intent(in) :: zrtmax(ilg,ic) !<
  real, intent(in) :: rsmin (ilg,ic) !<
  real, intent(in) :: qa50  (ilg,ic) !<
  real, intent(in) :: vpda  (ilg,ic) !<
  real, intent(in) :: vpdb  (ilg,ic) !<
  real, intent(in) :: psiga (ilg,ic) !<
  real, intent(in) :: psigb (ilg,ic) !<
  real, intent(inout) :: paidat(ilg,ic) !<
  real, intent(inout) :: hgtdat(ilg,ic) !<
  real, intent(in) :: acvdat(ilg,ic) !<
  real, intent(in) :: acidat(ilg,ic) !<
  real, intent(inout) :: thliq (ilg,ig) !<
  real, intent(inout) :: thice (ilg,ig) !<
  real, intent(inout) :: tbar  (ilg,ig) !<
  !
  real, intent(in) :: asvdat(ilg) !<
  real, intent(in) :: asidat(ilg) !<
  real, intent(in) :: agvdat(ilg) !<
  real, intent(in) :: agidat(ilg) !<
  real, intent(in) :: algwv (ilg) !<
  real, intent(in) :: algwn (ilg) !<
  real, intent(in) :: algdv (ilg) !<
  real, intent(in) :: algdn (ilg) !<
  real, intent(in) :: rhosni(ilg) !<
  real, intent(in) :: z0oro (ilg) !<
  real, intent(inout) :: rcan  (ilg) !<
  real, intent(inout) :: sncan (ilg) !<
  real, intent(inout) :: tcan  (ilg) !<
  real, intent(in) :: growth(ilg) !<
  real, intent(inout) :: sno   (ilg) !<
  real, intent(inout) :: tsnow (ilg) !<
  real, intent(in) :: rhosno(ilg) !<
  real, intent(inout) :: albsno(ilg) !<
  real, intent(in) :: fcloud(ilg) !<
  real, intent(in) :: ta    (ilg) !<
  real, intent(in) :: vpd   (ilg) !<
  real, intent(in) :: rhoair(ilg) !<
  real, intent(in) :: coszs (ilg) !<
  real, intent(in) :: qswinv(ilg) !<
  real, intent(in) :: dlon  (ilg) !<
  real, intent(in) :: zblend(ilg) !<
  real, intent(in) :: snolim(ilg) !<
  real, intent(in) :: zplmg0(ilg) !<
  real, intent(in) :: zplms0(ilg) !<
  real, intent(in) :: radj  (ilg)   !< Latitude \f$[radians]\f$
  real, intent(in) :: refsno(ilg) !<
  real, intent(in) :: bcsno(ilg) !<
  !
  real, intent(in), dimension(ilg,nbs) :: fsdb !<
  real, intent(in), dimension(ilg,nbs) :: fsfb !<
  !
  !    * soil property arrays.
  !
  real, intent(in) :: delzw (ilg,ig) !<
  real, intent(in) :: zbotw (ilg,ig) !<
  real, intent(in) :: thpor (ilg,ig) !<
  real, intent(in) :: thlmin(ilg,ig) !<
  real, intent(in) :: psisat(ilg,ig) !<
  real, intent(in) :: bi    (ilg,ig) !<
  real, intent(in) :: psiwlt(ilg,ig) !<
  real, intent(inout) :: hcps  (ilg,ig) !<
  !
  integer, intent(in), dimension(ilg,ig) :: isand !<
  real, intent(in) :: delz  (ig) !<
  !
  !     * ctem-related fields.

  !     * ailcg  - green lai for use in photosynthesis
  !     * ailcgs - green lai for canopy over snow sub-area
  !     * ailcmin- min. lai for ctem pfts
  !     * ailcmax- max. lai for ctem pfts
  !     * l2max  - maximum number of level 2 ctem pfts
  !     * nol2pfts - number of level 2 ctem pfts
  !     * FCANC  - FRACTION OF CANOPY OVER GROUND FOR CTEM's 9 PFTs
  !     * FCANCS - FRACTION OF CANOPY OVER SNOW FOR CTEM's 9 PFTs
  !     * see bio2str subroutine for definition of other ctem variables
  !
  real, intent(inout) :: fcancmx(ilg,ictem) !<
  real, intent(inout) :: rmatc(ilg,ic,ig) !<
  real, intent(inout) :: ailc  (ilg,ic) !<
  real, intent(inout) :: paic (ilg,ic) !<
  real, intent(inout) :: ailcg (ilg,ictem) !<
  real, intent(inout) :: ailcgs(ilg,ictem) !<
  real, intent(inout) :: fcanc(ilg,ictem) !<
  real, intent(inout) :: fcancs(ilg,ictem) !<
  real, intent(in) :: slaic(ilg,ic) !<

  integer, intent(in) :: ictem !<
  integer, intent(in) :: ictemmod !<
  integer, intent(in) :: l2max !<
  integer, intent(in) :: nol2pfts(ic) !<
  !
  !     * internal work arrays for this and associated subroutines.
  !
  real :: rmat (ilg,ic,ig) !<
  real :: h     (ilg,ic) !<
  real :: hs    (ilg,ic) !<
  real :: pai   (ilg,ic) !<
  real :: pais  (ilg,ic) !<
  real :: fcan  (ilg,ic) !<
  real :: fcans (ilg,ic) !<
  real :: cxteff(ilg,ic) !<
  real :: ail   (ilg,ic) !<
  real :: rcacc (ilg,ic) !<
  real :: rcg   (ilg,ic) !<
  real :: rcv   (ilg,ic) !<
  !
  real :: psignd(ilg) !<
  real :: cwcpav(ilg) !<
  real :: growa (ilg) !<
  real :: grown (ilg) !<
  real :: growb (ilg) !<
  real :: rresid(ilg) !<
  real :: sresid(ilg) !<
  real :: frtot (ilg) !<
  real :: frtots(ilg) !<
  real :: trvs  (ilg) !<
  real :: trir  (ilg) !<
  real :: rct   (ilg) !<
  real :: gc    (ilg) !<
  real :: paican(ilg) !<
  real :: paicns(ilg) !<
  !
  integer :: i !<
  integer :: j !<

  !------------------------------------------------------------------
  !     * calculation of snow depth zsnow and fractional snow cover
  !     * fsnow; initialization of computational arrays.
  !
  do i=il1,il2
    if (sno(i)>0.0) then
      zsnow(i)=sno(i)/rhosno(i)
      if (zsnow(i)>=(snolim(i)-0.00001)) then
        fsnow(i)=1.0
      else
        fsnow(i)=zsnow(i)/snolim(i)
        zsnow(i)=snolim(i)
        wsnow(i)=wsnow(i)/fsnow(i)
      end if
    else
      zsnow(i)=0.0
      fsnow(i)=0.0
    end if
    !
    alvscn(i)=0.0
    alircn(i)=0.0
    alvscs(i)=0.0
    alircs(i)=0.0
    trvscn(i)=0.0
    trircn(i)=0.0
    trvscs(i)=0.0
    trircs(i)=0.0
    alvssn(i)=0.0
    alirsn(i)=0.0
    alvsg (i)=0.0
    alirg (i)=0.0
    alvsgc(i)=0.0
    alirgc(i)=0.0
    alvssc(i)=0.0
    alirsc(i)=0.0
    trsnowc(i)=0.0

    trsnowg(i,1:nbs) = 0.0
    altg(i,1:nbs)    = 0.0
    alsno(i,1:nbs)   = 0.0

  end do ! loop 100
  !
  !     * preparation.
  !
  call aprep (fc,fg,fcs,fgs,paican,paicns,fsvf,fsvfs, &
                  frainc,fsnowc,fraics,fsnocs,raican,raicns,snocan, &
                  snocns,disp,disps,zomlnc,zomlcs,zoelnc,zoelcs, &
                  zomlng,zomlns, zoelng,zoelns,chcap,chcaps,cmassc, &
                  cmascs,cwlcap,cwfcap,cwlcps,cwfcps,rbcoef, &
                  zplimc,zplimg,zplmcs,zplmgs,htcc,htcs,htc, &
                  froot,froots, &
                  wtrc,wtrs,wtrg,cmai,pai,pais,ail,fcan,fcans,psignd, &
                  fcanmx,zoln,paimax,paimin,cwgtmx,zrtmax, &
                  paidat,hgtdat,thliq,thice,tbar,rcan,sncan, &
                  tcan,growth,zsnow,tsnow,fsnow,rhosno,sno,z0oro, &
                  zblend,zplmg0,zplms0, &
                  ta,rhoair,radj,dlon,rhosni,delz,delzw,zbotw, &
                  thpor,thlmin,psisat,bi,psiwlt,hcps,isand, &
                  ilg,il1,il2,jl,ic,icp1,ig,iday,idisp,izref,iwf, &
                  ipai,ihgt,rmat,h,hs,cwcpav,growa,grown,growb, &
                  rresid,sresid,frtot,frtots, &
                  fcancmx,ictem,ictemmod,rmatc, &
                  ailc,paic,ailcg,l2max,nol2pfts, &
                  ailcgs,fcancs,fcanc,slaic)

  !
  !     * bare soil albedos.
  !
  call gralb(alvsg,alirg,alvsgc,alirgc, &
                 algwv,algwn,algdv,algdn, &
                 thliq,fsnow,alvsc(1,5),alirc(1,5), &
                 fcanmx(1,5),agvdat,agidat,fg,isand, &
                 ilg,ig,il1,il2,jl,ialg)


  !     * snow albedos and transmissivity.
  !
  call snoalba(alvssn,alirsn,alvssc,alirsc,albsno, &
                   trsnowc, alsno, trsnowg, fsdb, fsfb, rhosno, &
                   refsno, bcsno,sno,coszs,zsnow,fsnow,asvdat,asidat, &
                   alvsg, alirg, &
                   ilg,ig,il1,il2,jl,ials,nbs,isnoalb)

  !     * canopy albedos and transmissivities, and vegetation
  !     * stomatal resistance.
  !
  call canalb(alvscn,alircn,alvscs,alircs,trvscn,trircn, &
                  trvscs,trircs,rc,rcs, &
                  alvsc,alirc,rsmin,qa50,vpda,vpdb,psiga,psigb, &
                  fc,fcs,fsnow,fsnowc,fsnocs,fcan,fcans,pai,pais, &
                  ail,psignd,fcloud,coszs,qswinv,vpd,ta, &
                  acvdat,acidat,alvsgc,alirgc,alvssc,alirsc, &
                  ilg,il1,il2,jl,ic,icp1,ig,ialc, &
                  cxteff,trvs,trir,rcacc,rcg,rcv,rct,gc)
  !
  !     * effective whole-surface visible and near-ir albedos.
  !
  do i=il1,il2
    alvs(i)=fc(i)*alvscn(i)+fg(i)*alvsg(i)+fcs(i)*alvscs(i)+ &
             fgs(i)*alvssn(i)
    alir(i)=fc(i)*alircn(i)+fg(i)*alirg(i)+fcs(i)*alircs(i)+ &
             fgs(i)*alirsn(i)
  end do ! loop 500
  !
  if (isnoalb == 0) then
    do i = il1, il2
      altg(i,1) = alvs(i)
      altg(i,2) = alir(i)
      altg(i,3) = alir(i)
      altg(i,4) = alir(i)
    end do ! i
  else if (isnoalb == 1) then
    do i = il1, il2
      altg(i,1) = fc(i)*alvscn(i)+fg(i)*alvsg(i)+fcs(i)*alvscs(i)+ &
                        fgs(i)*alsno(i,1)
      altg(i,2) = fc(i)*alircn(i)+fg(i)*alirg(i)+fcs(i)*alircs(i)+ &
                        fgs(i)*alsno(i,2)
      altg(i,3) = fc(i)*alircn(i)+fg(i)*alirg(i)+fcs(i)*alircs(i)+ &
                        fgs(i)*alsno(i,3)
      altg(i,4) = fc(i)*alircn(i)+fg(i)*alirg(i)+fcs(i)*alircs(i)+ &
                        fgs(i)*alsno(i,4)
    end do ! i
  end if ! isnoalb

  if (switch_gas_chem) then
! Collect canopy LAI (leaf area index) for dry deposition:
    vdlagat(il1:il2) = paican(il1:il2)
    vdlsgat(il1:il2) = paicns(il1:il2)
  endif

  return
end subroutine classa
