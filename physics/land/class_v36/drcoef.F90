subroutine drcoef(cdm,z0_momentum,z0_heat,cdh,rib,cflux,qg,qa,zomin,zohin, &
                        crib,tvirtg,tvirta,va,fi,iter, &
                        ilg,il1,il2) 
  !
  !     * nov 04/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * sep 10/02 - k.abdella.  bugfix in calculation of "OLS" (2 places).
  !     * may 22/02 - n.mcfarlane.use the asymptotic value for cdh
  !     *                         generally to limit fluxes and prevent
  !     *                         overshoot. (commmented out for
  !     *                         off-line runs.)
  !     * apr 11/01 - m.lazare.   shortened "CLASS2" common block.
  !     * oct 26/99 - e. chan.    comment out artificial damping of
  !     *                         turbulent fluxes for stand-alone testing.
  !     * jul 18/97 - m. lazare,  class 2.7. pass in additional work arrays
  !     *             d.verseghy. zomin and zohin to use internally,
  !     *                         so that input zomin and zohin do not
  !     *                         change when passed back to the
  !     *                         iteration (bug from previous
  !     *                         versions). previous zom and zoh
  !     *                         become work arrays.
  !     * mar 10/97 - m. lazare.  pass in qg and qa and use to only
  !     *                         damp turbulent fluxes using cdhmod
  !     *                         under stable conditions if moisture
  !     *                         flux is upwards. also, better
  !     *                         definition of surface layer top
  !     *                         from k.abdella.
  !     * may 21/96 - k. abdella. modification for free-convective
  !     *                         limit on unstable side added.
  !     * jan 10/96 - k. abdella. correct error in au1 (unstable
  !     *                         side) and put in prandtl number
  !     *                         range (0.74->1)/
  !     *                         "CDHMOD" used on both stable and
  !     *                         unstable side, to limit fluxes
  !     *                         over long timestep.
  !     * m. lazare - feb 14/95.  use variables "ZOLN" and "ZMLN"
  !     *                         on unstable side, for optimization.
  !     *                         this is previous version "DRCOEFX".
  !     * k. abdella/m. lazare. - nov 30/94.
  !
  use phys_consts, only : grav, vkc, rmos1, rmos2
  ! get logical tke switch from phys_options structure
  use agcm_types_mod, only: phys_options
  !
  implicit none

  !     * calculates drag coefficients and related variables for class.

  !     * output fields are:
  !     *    cdm    : stability-dependent drag coefficient for momentum.
  !     *    cdh    : stability-dependent drag coefficient for heat.
  !     *    rib    : bulk richardson number.
  !     *    cflux  : cd * mod(v), bounded by free-convective limit.

  !     * input fields are:
  !     *    zomin/: roughness heights for momentum/heat normalized by
  !     *    zohin   reference height.
  !     *    crib   : -rgas*slthkef/(va**2), where
  !     *             slthkef=-log(max(sgj(ilev),shj(ilev)))
  !     *    tvirtg : "SURFACE" virtual temperature.
  !     *    tvirta : lowest level virtual temperature.
  !     *    va     : amplitude of lowest level wind.
  !     *    fi     : fraction of surface type being studied.
  !     *    qg     : saturation specific humidity at ground temperature.
  !     *    qa     : lowest level specific humidity.
  !     *    iter   : index array indicating if point is undergoing
  !     *             further iteration or not.

  !     *    zom/  : work arrays used for scaling zomin/zohin
  !     *    zoh     on stable side, as part of calculation.
  !     * integer :: constants.

  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$

  !     * output arrays.

  real, intent(inout) :: cdm    (ilg) !<
  real, intent(inout) :: z0_momentum    (ilg) !<
  real, intent(inout) :: z0_heat    (ilg) !<
  real, intent(inout) :: cdh    (ilg) !<
  real, intent(inout) :: rib    (ilg) !<
  real, intent(inout) :: cflux  (ilg) !<

  !     * input arrays.

  real, intent(in) :: zomin  (ilg) !<
  real, intent(in) :: zohin  (ilg) !<
  real, intent(in) :: crib   (ilg) !<
  real, intent(in) :: tvirtg (ilg) !<
  real, intent(in) :: tvirta (ilg) !<
  real, intent(in) :: va     (ilg) !<
  real, intent(in) :: fi     (ilg) !<
  real, intent(in) :: qg     (ilg) !<
  real, intent(in) :: qa     (ilg) !<

  integer, intent(in), dimension(ilg) :: iter !<

  !     * work arrays.

  integer :: jl !<
  integer :: i !<
  real :: zom    (ilg) !<
  real :: zoh    (ilg) !<

  !     * temporary variables.

  real :: aa !<
  real :: aa1 !<
  real :: beta !<
  real :: pr !<
  real :: zlev !<
  real :: zs !<
  real :: zoln !<
  real :: zmln !<
  real :: cpr !<
  real :: zi !<
  real :: olsf !<
  real :: olfact !<
  real :: zl !<
  real :: zmol !<
  real :: zhol !<
  real :: xm !<
  real :: xh !<
  real :: bh1 !<
  real :: bh2 !<
  real :: bh !<
  real :: wb !<
  real :: wstar !<
  real :: rib0 !<
  real :: wspeed !<
  real :: au1 !<
  real :: ols !<
  real :: psim1 !<
  real :: psim0 !<
  real :: psih1 !<
  real :: psih0 !<
  real :: ustar !<
  real :: tstar !<
  real :: wts !<
  real :: as1 !<
  real :: as2 !<
  real :: as3 !<
  real :: climit !<

  !     * additional temporary variables for tke.
  real :: etae,chie,phime,phifac1,phifac2,phihe,ffe
  real :: a1,a2,dffe,chif

  !-------------------------------------------------------------

  aa=9.5285714
  aa1=14.285714
  beta=1.2
  pr = 1.
  !

      if (phys_options%use_tke) then
       aa=2.381
       do i=il1,il2
        if(fi(i).gt.0. .and. iter(i).eq.1)                         then
          rib(i)=crib(i)*(tvirtg(i)-tvirta(i))
          zlev=-crib(i)*tvirta(i)*(va(i)**2)/grav
          if(rib(i).ge.0.0) then
            zs=max(10.,5.*max(zomin(i)*zlev, zohin(i)*zlev))
            zs=zlev*(1.+rib(i))/(1.+(zlev/zs)*rib(i))
            zom(i)=zomin(i)*zlev/zs
            zoh(i)=zohin(i)*zlev/zs
            rib(i)=rib(i)*zs/zlev
          else
            zom(i)=zomin(i)
            zoh(i)=zohin(i)
          endif
          zoln=log(zoh(i))
          zmln=log(zom(i))
          if(rib(i).lt.0.0) then
            cpr=min(max(zoln/zmln,0.74),1.0)
            zi=1000.0
            olsf=beta**3*zi*vkc**2/zmln**3
            olfact=1.7*(log(1.+zom(i)/zoh(i)))**0.5+0.9
            olsf=olsf*olfact
            zl = -crib(i)*tvirta(i)*(va(i)**2)/grav
            zmol=zom(i)*zl/olsf
            zhol=zoh(i)*zl/olsf
            xm=(1.00-rmos1*zmol)**(0.250)
            xh=(1.00-rmos2*zhol)**0.25
            bh1=-log(-2.41*zmol)+log(((1.+xm)/2.)**2*(1.+xm**2)/2.)
            bh1=bh1-2.*atan(xm)+atan(1.)*2.
            bh1=bh1**1.5
            bh2=-log(-0.25*zhol)+2.*log(((1.00+xh**2)/2.00))
            bh=vkc**3.*beta**1.5/(bh1*(bh2)**1.5)
            wb=sqrt(grav*(tvirtg(i)-tvirta(i))*zi/tvirtg(i))
            wstar=bh**(0.333333)*wb
            rib0=rib(i)

            wspeed=sqrt(va(i)**2+(beta*wstar)**2)
            rib(i)=rib0*va(i)**2/wspeed**2
            au1=1.+5.0*(zoln-zmln)*rib(i)*(zoh(i)/zom(i))**0.25
            ols=-rib(i)*zmln**2/(cpr*zoln)*(1.0+au1/(1.0-rib(i)/(zom(i)*zoh(i))**0.25))
            psim1=log(((1.00+(1.00-rmos1*ols)**0.250)/2.00)**2* &
                 (1.0+(1.00-rmos1*ols)**0.5)/2.0)-2.0*atan(    &
                 (1.00-rmos1*ols)**0.250)+atan(1.00)*2.00
            psim0=log(((1.00+(1.00-rmos1*ols*zom(i))**0.250)/2.00)**2 &
                 *(1.0+(1.00-rmos1*ols*zom(i))**0.5)/2.0)-2.0* &
                 atan((1.00-rmos1*ols*zom(i))**0.250)+atan(1.00)*2.0
            psih1=log(((1.00+(1.00-rmos2*ols)**0.50)/2.00)**2)
            psih0=log(((1.00+(1.00-rmos2*ols*zoh(i))**0.50)/2.00)**2)

            ustar=vkc/(-zmln-psim1+psim0)
            tstar=vkc/(-zoln-psih1+psih0)
            cdh(i)=ustar*tstar/pr
            wts=cdh(i)*wspeed*(tvirtg(i)-tvirta(i))
            wstar=(grav*zi/tvirtg(i)*wts)**(0.333333)

            wspeed=sqrt(va(i)**2+(beta*wstar)**2)
            rib(i)=rib0*va(i)**2/wspeed**2
            au1=1.+5.0*(zoln-zmln)*rib(i)*(zoh(i)/zom(i))**0.25
            ols=-rib(i)*zmln**2/(cpr*zoln)*(1.0+au1/(1.0-rib(i)/(zom(i)*zoh(i))**0.25))
            psim1=log(((1.00+(1.00-rmos1*ols)**0.250)/2.00)**2* &
                 (1.0+(1.00-rmos1*ols)**0.5)/2.0)-2.0*atan( &
                 (1.00-rmos1*ols)**0.250)+atan(1.00)*2.00
            psim0=log(((1.00+(1.00-rmos1*ols*zom(i))**0.250)/2.00)**2 &
                 *(1.0+(1.00-rmos1*ols*zom(i))**0.5)/2.0)-2.0* &
                 atan((1.00-rmos1*ols*zom(i))**0.250)+atan(1.00)*2.0
            psih1=log(((1.00+(1.00-rmos2*ols)**0.50)/2.00)**2)
            psih0=log(((1.00+(1.00-rmos2*ols*zoh(i))**0.50)/2.00)**2)

          else

            wspeed=va(i)
            as1=10.0*zmln*(zom(i)-1.0)
            as2=5.00/(2.0-8.53*rib(i)*exp(-3.35*rib(i))+0.05*rib(i)**2)
!<<<
            as2=as2*pr*sqrt(-zmln)/2.
            as3=27./(8.*pr*pr)
!>>>
            etae=4.*rib(i)
            chie=etae*((zmln**2)/(-pr*zoln)+as3*etae)
!           phime=-zmln+chie*(1.-zom(i))
            phime=-zmln+chie*(1.-zom(i)) ! 1201dv1
            phifac1=(1.+2.*chie/3.)**0.5
            phifac2=(1.+2.*chie*zoh(i)/3.)**0.5
            phihe=-zoln+phifac1**3-phifac2**3
            ffe=chie*phihe/phime**2-etae
            a1=phifac1-zoh(i)*phifac2
            a2=(a1/phihe-2.*(1-zom(i))/phime)
            dffe=(phihe/phime**2)*(1.+chie*a2)
            chif=chie-ffe/dffe
! ** note that in the formulae below chif = 4z(ilev)/l where l is the monin-obukov length. **
! ** that is, ols=chif/4., where ols is the the ratio of the lowest model level height (z1) to the monin-obukov length.
            psim1=-chif
            psim0=-chif*zom(i)
            psih1=-(1.+2.*chif/3.)**1.5 +1.
            psih0=-(1.+3.*chif*zoh(i)/3.)**1.5 +1.
          endif

          ustar=vkc/(-zmln-psim1+psim0)
          tstar=vkc/(-zoln-psih1+psih0)

          cdm(i)=ustar**2.0
          cdh(i)=ustar*tstar/pr
!
          z0_momentum(i)=exp(zmln)*zlev
          z0_heat(i)=exp(zoln)*zlev
!         * calculate cd*mod(v) under free-convective limit.
!
          if(tvirtg(i).gt.tvirta(i))    then
            climit=1.9e-3*(tvirtg(i)-tvirta(i))**0.333333
          else
            climit=0.
          endif
          cflux(i)=max(cdh(i)*wspeed,climit)
        endif
      enddo
    endif
!
    if (.not. phys_options%use_tke) then
      aa=9.5285714
      do i=il1,il2
        if (fi(i)>0. .and. iter(i)==1) then
          rib(i)=crib(i)*(tvirtg(i)-tvirta(i))
          zlev=-crib(i)*tvirta(i)*(va(i)**2)/grav
          if (rib(i)>=0.0) then
            zs=max(10.,5.*max(zomin(i)*zlev, zohin(i)*zlev))
            zs=zlev*(1.+rib(i))/(1.+(zlev/zs)*rib(i))
            zom(i)=zomin(i)*zlev/zs
            zoh(i)=zohin(i)*zlev/zs
            rib(i)=rib(i)*zs/zlev
          else
            zom(i)=zomin(i)
            zoh(i)=zohin(i)
          end if
          zoln=log(zoh(i))
          zmln=log(zom(i))
          if (rib(i)<0.0) then
            cpr=max(zoln/zmln,0.74)
            cpr=min(cpr,1.0)
            zi=1000.0
            olsf=beta**3*zi*vkc**2/zmln**3
            olfact=1.7*(log(1.+zom(i)/zoh(i)))**0.5+0.9
            olsf=olsf*olfact
            zl = -crib(i)*tvirta(i)*(va(i)**2)/grav
            zmol=zom(i)*zl/olsf
            zhol=zoh(i)*zl/olsf
            xm=(1.00-15.0*zmol)**(0.250)
            xh=(1.00-9.0*zhol)**0.25
            bh1=-log(-2.41*zmol)+log(((1.+xm)/2.)**2*(1.+xm**2)/2.)
            bh1=bh1-2.*atan(xm)+atan(1.)*2.
            bh1=bh1**1.5
            bh2=-log(-0.25*zhol)+2.*log(((1.00+xh**2)/2.00))
            bh=vkc**3.*beta**1.5/(bh1*(bh2)**1.5)
            wb=sqrt(grav*(tvirtg(i)-tvirta(i))*zi/tvirtg(i))
            wstar=bh**(0.333333)*wb
            rib0=rib(i)

            wspeed=sqrt(va(i)**2+(beta*wstar)**2)
            rib(i)=rib0*va(i)**2/wspeed**2
            au1=1.+5.0*(zoln-zmln)*rib(i)*(zoh(i)/zom(i))**0.25
            ols=-rib(i)*zmln**2/(cpr*zoln)*(1.0+au1/ &
                (1.0-rib(i)/(zom(i)*zoh(i))**0.25))
            psim1=log(((1.00+(1.00-15.0*ols)**0.250)/2.00)**2* &
                 (1.0+(1.00-15.0*ols)**0.5)/2.0)-2.0*atan( &
                 (1.00-15.0*ols)**0.250)+atan(1.00)*2.00
            psim0=log(((1.00+(1.00-15.0*ols*zom(i))**0.250)/2.00)**2 &
                 *(1.0+(1.00-15.0*ols*zom(i))**0.5)/2.0)-2.0* &
                 atan((1.00-15.0*ols*zom(i))**0.250)+atan(1.00)*2.0
            psih1=log(((1.00+(1.00-9.0*ols)**0.50)/2.00)**2)
            psih0=log(((1.00+(1.00-9.0*ols*zoh(i))**0.50)/2.00)**2)

            ustar=vkc/(-zmln-psim1+psim0)
            tstar=vkc/(-zoln-psih1+psih0)
            cdh(i)=ustar*tstar/pr
            wts=cdh(i)*wspeed*(tvirtg(i)-tvirta(i))
            wstar=(grav*zi/tvirtg(i)*wts)**(0.333333)

            wspeed=sqrt(va(i)**2+(beta*wstar)**2)
            rib(i)=rib0*va(i)**2/wspeed**2
            au1=1.+5.0*(zoln-zmln)*rib(i)*(zoh(i)/zom(i))**0.25
            ols=-rib(i)*zmln**2/(cpr*zoln)*(1.0+au1/ &
                (1.0-rib(i)/(zom(i)*zoh(i))**0.25))
            psim1=log(((1.00+(1.00-15.0*ols)**0.250)/2.00)**2* &
                 (1.0+(1.00-15.0*ols)**0.5)/2.0)-2.0*atan( &
                 (1.00-15.0*ols)**0.250)+atan(1.00)*2.00
            psim0=log(((1.00+(1.00-15.0*ols*zom(i))**0.250)/2.00)**2 &
                 *(1.0+(1.00-15.0*ols*zom(i))**0.5)/2.0)-2.0* &
                 atan((1.00-15.0*ols*zom(i))**0.250)+atan(1.00)*2.0
            psih1=log(((1.00+(1.00-9.0*ols)**0.50)/2.00)**2)
            psih0=log(((1.00+(1.00-9.0*ols*zoh(i))**0.50)/2.00)**2)

          else

            wspeed=va(i)
            as1=10.0*zmln*(zom(i)-1.0)
            as2=5.00/(2.0-8.53*rib(i)*exp(-3.35*rib(i))+0.05*rib(i)**2)
            !< <<
            as2=as2*pr*sqrt(-zmln)/2.
            as3=27./(8.*pr*pr)
            !> >>
            ols=rib(i)*(zmln**2+as3*as1*(rib(i)**2+as2*rib(i))) &
               /(as1*rib(i)-pr*zoln)
            psim1=-0.667*(ols-aa1)*exp(-0.35*ols)-aa-ols
            psim0=-0.667*(ols*zom(i)-aa1)*exp(-0.35*ols*zom(i)) &
                 -aa-ols*zom(i)
            psih1=-(1.0+2.0*ols/3.0)**1.5-0.667*(ols-aa1) &
                 *exp(-0.35*ols)-aa+1.0
            psih0=-(1.0+2.0*ols*zoh(i)/3.0)**1.5-0.667*(ols*zoh(i)-aa1) &
                 *exp(-0.35*ols*zoh(i))-aa+1.0

          end if

          ustar=vkc/(-zmln-psim1+psim0)
          tstar=vkc/(-zoln-psih1+psih0)

          cdm(i)=ustar**2.0
          cdh(i)=ustar*tstar/pr
          
          z0_momentum(i)=exp(zmln)*zlev
          z0_heat(i)=exp(zoln)*zlev
          !         * calculate cd*mod(v) under free-convective limit.
          !
          if (tvirtg(i)>tvirta(i)) then
            climit=1.9e-3*(tvirtg(i)-tvirta(i))**0.333333
          else
            climit=0.
          end if
          cflux(i)=max(cdh(i)*wspeed,climit)
      end if
    end do ! loop 100
  endif
  !-------------------------------------------------------------
  return
end subroutine drcoef
