subroutine  balcar (gleafmas, stemmass, rootmass, bleafmas, &
                           litrmass, soilcmas, ntchlveg, ntchsveg, &
                           ntchrveg, tltrleaf, tltrstem, tltrroot, &
                           glcaemls, blcaemls, stcaemls, rtcaemls, &
                           ltrcemls, ltresveg, scresveg, humtrsvg, &
                           pglfmass, pblfmass, pstemass, protmass, &
                           plitmass, psocmass,   deltat, vgbiomas, &
                           pvgbioms, gavgltms, pgavltms, gavgscms, &
                           pgavscms, galtcels, &
                           npp,  autores, hetrores,      gpp, &
                           nep,   litres,   socres, dstcemls, &
                           nbp, litrfall, humiftrs, &
                           icc,      ilg,      il1,      il2)
  !     -----------------------------------------------------------------
  !
  !               canadian terrestrial ecosystem model (ctem) v1.0
  !                          carbon balance subroutine
  !
  !     sk: may  2018 - revise tolerances for 32-bit version.
  !     27  may 2003  - this subroutine checks if the various c fluxes
  !     v. arora        between the different pools balance properly to
  !                     make sure that conservation of mass is achieved
  !                     with in a specified tolerance.
  !     inputs
  !
  !                 unless mentioned all pools are in kg c/m2
  !
  !                 pools (after being updated)
  !
  !     stemmass  - stem mass for each of the 9 ctem pfts
  !     rootmass  - root mass for each of the 9 ctem pfts
  !     gleafmas  - green leaf mass for each of the 9 ctem pfts
  !     bleafmas  - brown leaf mass for each of the 9 ctem pfts
  !     litrmass  - litter mass over the 9 pfts and the bare fraction
  !                 of the grid cell
  !     soilcmas  - soil carbon mass over the 9 pfts and the bare fraction
  !                 of the grid cell
  !
  !                 grid averaged pools
  !     vgbiomas  - vegetation biomass
  !     gavgltms  - litter mass
  !     gavgscms  - soil carbon mass
  !
  !                 pools (before being updated)

  !                 variable explanation same as above.
  !     pglfmass  - previous green leaf mass
  !     pblfmass  - previous brown leaf mass
  !     pstemass  - previous stem mass
  !     protmass  - previous root mass
  !     plitmass  - previous litter mass
  !     psocmass  - previous soil c mass
  !
  !                 grid average pools
  !     pvgbioms  - previous vegetation biomass
  !     pgavltms  - previous litter mass
  !     pgavscms  - previous soil c mass
  !
  !                 unless mentioned all fluxes are in units of
  !                 u-mol co2/m2.sec
  !
  !                 fluxes for each pft
  !
  !     ntchlveg  - net change in leaf biomass
  !     ntchsveg  - net change in stem biomass
  !     ntchrveg  - net change in root biomass
  !                 the net change is the difference between allocation
  !                 and autotrophic respiratory fluxes
  !
  !     tltrleaf  - total leaf litter falling rate
  !     tltrstem  - total stem litter falling rate
  !     tltrroot  - total root litter falling rate
  !
  !                 carbon emission losses mainly due to fire
  !     glcaemls  - green leaf carbon emission losses
  !     blcaemls  - brown leaf carbon emission losses
  !     stcaemls  - stem carbon emission losses
  !     rtcaemls  - root carbon emission losses
  !     ltrcemls  - litter carbon emission losses
  !
  !     ltresveg  - litter respiration for each pft + bare fraction
  !     scresveg  - soil c respiration for each pft + bare fraction
  !     humtrsvg  - humification for each pft + bare fraction
  !
  !                 grid averaged fluxes
  !
  !     npp       - net primary productivity
  !     autores   - autotrophic respiration
  !     hetrores  - heterotrophic respiration
  !     gpp       - gross primary productivity
  !     nep       - net primary productivity
  !     litres    - litter respiration
  !     socres    - soil carbon respiration
  !     dstcemls  - carbon emission losses due to disturbance, mainly fire
  !     galtcels  - carbon emission losses from litter
  !     nbp       - net biome productivity
  !     litrfall  - combined (leaves, stem, and root) total litter fall rate
  !     humiftrs  - humification
  !
  !                 other variables
  !
  !     DELTAT    - CTEM's TIME STEP
  !     icc       - no. of ctem plant function types, currently 8
  !     ilg       - no. of grid cells in latitude circle
  !     il1,il2   - il1=1, il2=ilg
  !
  implicit none
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: icc !<
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer :: i !<
  integer :: j !<
  integer :: k !<
  !
  real, intent(in) :: stemmass(ilg,icc) !<
  real, intent(in) :: rootmass(ilg,icc) !<
  real, intent(in) :: gleafmas(ilg,icc) !<
  real, intent(in) :: bleafmas(ilg,icc) !<
  real, intent(in) :: litrmass(ilg,icc+1) !<
  real, intent(in) :: soilcmas(ilg,icc+1) !<
  real, intent(in) :: ntchlveg(ilg,icc) !<
  real, intent(in) :: ntchsveg(ilg,icc) !<
  real, intent(in) :: ntchrveg(ilg,icc) !<
  real, intent(in) :: tltrleaf(ilg,icc) !<
  real, intent(in) :: tltrstem(ilg,icc) !<
  real, intent(in) :: tltrroot(ilg,icc) !<
  real, intent(in) :: glcaemls(ilg,icc) !<
  real, intent(in) :: blcaemls(ilg,icc) !<
  real, intent(in) :: stcaemls(ilg,icc) !<
  real, intent(in) :: rtcaemls(ilg,icc) !<
  real, intent(in) :: ltrcemls(ilg,icc) !<
  real, intent(in) :: ltresveg(ilg,icc+1) !<
  real, intent(in) :: scresveg(ilg,icc+1) !<
  real, intent(in) :: humtrsvg(ilg,icc+1) !<
  real, intent(in) :: pglfmass(ilg,icc) !<
  real, intent(in) :: pblfmass(ilg,icc) !<
  real, intent(in) :: pstemass(ilg,icc) !<
  real, intent(in) :: protmass(ilg,icc) !<
  real, intent(in) :: plitmass(ilg,icc+1) !<
  real, intent(in) :: psocmass(ilg,icc+1) !<
  real, intent(in) :: npp(ilg) !<
  real, intent(in) :: vgbiomas(ilg) !<
  real, intent(in) :: pvgbioms(ilg) !<
  real, intent(in) :: gavgltms(ilg) !<
  real, intent(in) :: pgavltms(ilg) !<
  real, intent(in) :: gavgscms(ilg) !<
  real, intent(in) :: pgavscms(ilg) !<
  real, intent(in) :: autores(ilg) !<
  real, intent(in) :: hetrores(ilg) !<
  real, intent(in) :: gpp(ilg) !<
  real, intent(in) :: nep(ilg) !<
  real, intent(in) :: litres(ilg) !<
  real, intent(in) :: socres(ilg) !<
  real, intent(in) :: dstcemls(ilg) !<
  real, intent(in) :: nbp(ilg) !<
  real, intent(in) :: litrfall(ilg) !<
  real, intent(in) :: humiftrs(ilg) !<
  real :: zero !<
  real :: tolrance !<
  real, intent(in) :: deltat   !< Timestep for CTEM \f$[days]\f$
  real, intent(in) :: galtcels(ilg) !<
  !
  real             :: diff1 !<
  real             :: diff2 !<
  !
  !
  !     -----------------------------------------------------------------
  !     constants
  !
  !     our tolerance for balancing c budget in kg c/m2 in one day
  !     data tolrance/0.00001/    ! 64-bit this is 1/100th of a gram of c
  data tolrance/0.0001/     ! 32-bit this is 1/10th of a gram of c
  !     -----------------------------------------------------------------
  !
  if (icc/=9)                            call xit('BALCAR',-1)
  !
  !     to check c budget we go through each pool for each vegetation
  !     type.
  !
  !     green and brown leaves
  !
  do j = 1, icc
    do i = il1, il2
      diff1=(gleafmas(i,j)+bleafmas(i,j)- pglfmass(i,j)- &
      pblfmass(i,j))
      diff2=(ntchlveg(i,j)- tltrleaf(i,j)- glcaemls(i,j)- &
      blcaemls(i,j))*(deltat/963.62)
      if ((abs(diff1-diff2))>tolrance) then
        write(6,2000)i,j,abs(diff1-diff2),tolrance
2000    format('AT (I)= (',i3,'), PFT=',i2,', ',f12.6,' IS GREATER &
  than our tolerance of ',F12.6,' for leaves')
        call xit('BALCAR',-2)
      end if
    end do ! loop 110
  end do ! loop 100
  !
  !     stem
  !
  do j = 1, icc
    do i = il1, il2
      diff1=stemmass(i,j) - pstemass(i,j)
      diff2=(ntchsveg(i,j)- tltrstem(i,j)- &
      stcaemls(i,j))*(deltat/963.62)
      if ((abs(diff1-diff2))>tolrance) then
        write(6,2001)i,j,abs(diff1-diff2),tolrance
2001    format('AT (I)= (',i3,'), PFT=',i2,', ',f12.6,' IS GREATER &
 than our tolerance of ',F12.6,' for stem')
        call xit('BALCAR',-3)
      end if
    end do ! loop 160
  end do ! loop 150
  !
  !     root
  !
  do j = 1, icc
    do i = il1, il2
      diff1=rootmass(i,j) - protmass(i,j)
      diff2=(ntchrveg(i,j)- tltrroot(i,j)- &
      rtcaemls(i,j))*(deltat/963.62)
      if ((abs(diff1-diff2))>tolrance) then
        write(6,2002)i,j,abs(diff1-diff2),tolrance
2002    format('AT (I)= (',i3,'), PFT=',i2,', ',f12.6,' IS GREATER &
 than our tolerance of ',F12.6,' for root')
        call xit('BALCAR',-4)
      end if
    end do ! loop 210
  end do ! loop 200
  !
  !     litter over all pfts
  !
  do j = 1, icc
    do i = il1, il2
      diff1=litrmass(i,j) - plitmass(i,j)
      diff2=( tltrleaf(i,j)+tltrstem(i,j)+tltrroot(i,j)- &
       ltresveg(i,j)-humtrsvg(i,j)-ltrcemls(i,j))*(deltat/963.62)
      if ((abs(diff1-diff2))>tolrance) then
        write(6,2003)i,j,abs(diff1-diff2),tolrance
2003    format('AT (I)= (',i3,'), PFT=',i2,', ',f12.6,' IS GREATER &
 than our tolerance of ',F12.6,' for litter')
        call xit('BALCAR',-5)
      end if
    end do ! loop 260
  end do ! loop 250
  !
  !     litter over the bare fraction
  !
  do j = icc+1, icc+1
    do i = il1, il2
      diff1=litrmass(i,j) - plitmass(i,j)
      diff2=( -ltresveg(i,j)-humtrsvg(i,j))* &
           (deltat/963.62)
      if ((abs(diff1-diff2))>tolrance) then
        write(6,2003)i,j,abs(diff1-diff2),tolrance
        call xit('BALCAR',-6)
      end if
    end do ! loop 280
  end do ! loop 270
  !
  !
  !     soil carbon
  !
  do j = 1, icc+1
    do i = il1, il2
      diff1=soilcmas(i,j) - psocmass(i,j)
      diff2=( humtrsvg(i,j)-scresveg(i,j) )*(deltat/963.62)
      if ((abs(diff1-diff2))>tolrance) then
        write(6,2004)i,j,abs(diff1-diff2),tolrance
2004    format('AT (I)= (',i3,'), PFT=',i2,', ',f12.6,' IS GREATER &
 than our tolerance of ',F12.6,' for soil c')
        call xit('BALCAR',-7)
      end if
    end do ! loop 310
  end do ! loop 300
  !
  !     -----------------------------------------------------------------
  !
  !     grid averaged fluxes must also balance
  !
  !     vegetation biomass
  !
  do i = il1, il2
    diff1=vgbiomas(i)-pvgbioms(i)
    diff2=(gpp(i)-autores(i)-litrfall(i)- &
    dstcemls(i) )*(deltat/963.62)
    if ((abs(diff1-diff2))>tolrance) then
      write(6,3001)'VGBIOMAS(',i,')=',vgbiomas(i)
      write(6,3001)'PVGBIOMS(',i,')=',pvgbioms(i)
      write(6,3001)'     GPP(',i,')=',gpp(i)
      write(6,3001)' AUTORES(',i,')=',autores(i)
      write(6,3001)'LITRFALL(',i,')=',litrfall(i)
      write(6,3001)'DSTCEMLS(',i,')=',dstcemls(i)
3001  format(a9,i2,a2,f14.9)
      write(6,2005)i,abs(diff1-diff2),tolrance
2005  format('AT (I)= (',i3,'),',f12.6,' IS GREATER &
 than our tolerance of ',F12.6,' for vegetation biomass')
      call xit('BALCAR',-8)
    end if
  end do ! loop 350
  !
  !     litter
  !
  do i = il1, il2
    diff1=gavgltms(i)-pgavltms(i)
    diff2=(litrfall(i)-litres(i)-humiftrs(i)-galtcels(i))* &
    (deltat/963.62)
    if ((abs(diff1-diff2))>tolrance) then
      write(6,3001)'PGAVLTMS(',i,')=',pgavltms(i)
      write(6,3001)'GAVGLTMS(',i,')=',gavgltms(i)
      write(6,3001)'LITRFALL(',i,')=',litrfall(i)
      write(6,3001)'  LITRES(',i,')=',litres(i)
      write(6,3001)'HUMIFTRS(',i,')=',humiftrs(i)
      write(6,3001)'GALTCELS(',i,')=',galtcels(i)
      write(6,2006)i,abs(diff1-diff2),tolrance
2006  format('AT (I)= (',i3,'),',f12.6,' IS GREATER &
 than our tolerance of ',F12.6,' for litter mass')
      call xit('BALCAR',-9)
    end if
  end do ! loop 380
  !
  !     soil carbon
  !
  do i = il1, il2
    diff1=gavgscms(i)-pgavscms(i)
    diff2=(humiftrs(i)-socres(i))*(deltat/963.62)
    if ((abs(diff1-diff2))>tolrance) then
      write(6,3001)'PGAVSCMS(',i,')=',pgavscms(i)
      write(6,3001)'GAVGSCMS(',i,')=',gavgscms(i)
      write(6,3001)'HUMIFTRS(',i,')=',humiftrs(i)
      write(6,3001)'  SOCRES(',i,')=',socres(i)
      write(6,2007)i,abs(diff1-diff2),tolrance
2007  format('AT (I)= (',i3,'),',f12.6,' IS GREATER &
 than our tolerance of ',F12.6,' for soil c mass')
      call xit('BALCAR',-10)
    end if
  end do ! loop 390
  !
  return
end

