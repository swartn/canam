# CanAM

CanAM is the atmospheric component of [CanESM](https://gitlab.com/cccma/canesm).

## License

CanAM / CanESM are distributed under the [Open Government License - Canada version 2.0](https://open.canada.ca/en/open-government-licence-canada).

## Support disclaimer

This code is made available on an as-is basis. lt has been tested only on the computing facilities
within Environment and Climate Change Canada (ECCC). There is no guarantee that it will run on
other platforms or if it does, that it will run correctly. No support of any kind will be made available
to help users to run the model on their own system. README documents linked below describe the development process.